#ifndef FLORA_INTERSECTION_H
#define FLORA_INTERSECTION_H

#include <flora_renderer.h>

#include <algorithm>

/*! \file
 * The header file of flora_intersection library.
 * This library provides means to define bounding figures for
 * objects and manipulate them.
 */

class BoundingSphere;

/*!
 * A bounding figure.
 * Every figure can be converted to sphere.
 */
class BoundingFigure
{
public:
    virtual ~BoundingFigure() = default;

    /*!
     * Convert to sphere.
     * @return A sphere which covers a source figure.
     */
    virtual BoundingSphere convertToSphere() const = 0;
};

/*!
 * A sphere.
 */
class BoundingSphere : public BoundingFigure
{
public:
    BoundingSphere();
    BoundingSphere(frVector4 position, double radius);

    /*! Return coordinates of a sphere.
     * @return Position vector. */
    frVector4 getPosition();
    /*! Return radius of a sphere.
     * @return Radius. */
    double getRadius();

    BoundingSphere convertToSphere() const override;

    /*!
     * Return sphere transformed by matrix.
     * @param matrix A transformation matrix.
     * @return A transformed sphere.
     */
    BoundingSphere getTransformed(frMatrix4x4 matrix) const;

    /*!
     * Calculate the sphere that covers the model consisting of verticles.
     * @tparam T Type of verticles to be used.
     * @param vertices An array of verticles.
     * @param count Number of verticles. Must be more than zero.
     * @return A sphere that cover the 3d model.
     */
    template<typename T>
    BoundingSphere calculateBoundingSphere(T* vertices, size_t count);
private:
    frVector4 position;
    double radius;
};

/*!
 * Calculates the maximum distance from verticle to point where
 * verticle is a member of verticles.
 * @tparam T Type of verticles to be used.
 * @param point Point in question.
 * @param vertices An array of verticles.
 * @param count Number of verticles. Must be more than zero.
 * @return Maximum distance from point to a furthest verticle.
 */
template<typename T>
double calculateMaxDistanceFromPoint(frVector4 point, T* vertices, size_t count)
{
    double distance = 0;

    for(size_t i = 0; i < count; i++)
    {
        distance = std::max(distance,
                            frVector3Distance(vertices[i]->position, point));
    }

    return distance;
}

template<typename T>
BoundingSphere BoundingSphere::calculateBoundingSphere(T* vertices,
                                                       size_t count)
{
    BoundingSphere result(FR_VEC3_ZERO, 0);

    for(size_t i = 0; i < count; i++)
        result.position = FR_VEC3_ADD(result.position, vertices[i].position);

    double multiplier = 1 / (double)count;

    result.position.x *= multiplier;
    result.position.y *= multiplier;
    result.position.z *= multiplier;

    result.radius = calculateMaxDistanceFromPoint(result.position,
                                                  vertices,
                                                  count);

    return result;
}

/*!
 * Axis-aligned bounding box.
 * left and right correspond to x-axis, bottom and top correspond
 * to y-axis, near and far correspond to z-axis.
 * near is less than far.
 *
 * A set of methods getXYZCorner() is defined as following:
 * Such a method returns a corner which is defined by coordinates
 * taken from values stored with respect to axes and closeness to positive
 * and negative infinities. So, the getPNPCorner() returns
 * the corner with the biggest x coordinate, smallest y coordinate
 * and biggest z coordinate, i. e. right, bottom, far.
 */
class BoundingAABox : public BoundingFigure
{
public:
    const double SIDE_SIZE_PADDING = FR_TINY;

    BoundingAABox() = default;
    BoundingAABox(double left, double right,
                  double bottom, double top,
                  double near, double far);
    BoundingAABox(BoundingSphere);
    BoundingAABox(const BoundingAABox&) = default;

    BoundingAABox& operator=(const BoundingAABox&);
    BoundingAABox& operator=(BoundingAABox&&) noexcept;

    double left();
    double right();
    double bottom();
    double top();
    double near();
    double far();

    frVector4 getNNNCorner() const; /*!< See class description for details, */
    frVector4 getNNPCorner() const; /*!< See class description for details, */
    frVector4 getNPNCorner() const; /*!< See class description for details, */
    frVector4 getNPPCorner() const; /*!< See class description for details, */
    frVector4 getPNNCorner() const; /*!< See class description for details, */
    frVector4 getPNPCorner() const; /*!< See class description for details, */
    frVector4 getPPNCorner() const; /*!< See class description for details, */
    frVector4 getPPPCorner() const; /*!< See class description for details, */

    /*!
     * Return center of the box.
     * @return Position vector of the center of the box.
     */
    frVector4 getCenter() const;

    BoundingSphere convertToSphere() const override;

    BoundingAABox getTransformed(frMatrix4x4 matrix) const;

    /*!
     * Expand the box such that it covers other box.
     * @param other Another box.
     */
    void expandBy(BoundingAABox other);
    /*!
     * Expand the box such that it covers the sphere.
     * @param sphere Sphere in question.
     */
    void expandBy(BoundingSphere sphere);

    /*!
     * Expand the box by a FR_TINY.
     */
    void expandTiny();

    /*!
     * Calculate the box that covers the model consisting of verticles.
     * @tparam T Type of verticles to be used.
     * @param vertices An array of verticles.
     * @param count Number of verticles. Must be more than zero.
     * @return A sphere that cover the 3d model.
     */
    template<typename T>
    static BoundingAABox computeAABox(T* vertices, size_t count);
private:
    double leftv;
    double rightv;
    double bottomv;
    double topv;
    double nearv;
    double farv;
};

template<typename T>
BoundingAABox BoundingAABox::computeAABox(T* vertices, size_t count)
{
    double left   = vertices[0].position.x,
           right  = vertices[0].position.x,
           bottom = vertices[0].position.y,
           top    = vertices[0].position.y,
           near   = vertices[0].position.z,
           far    = vertices[0].position.z;

    for(size_t i = 0; i < count; i++)
    {
        frVector4 position = vertices[i].position;

        left    = std::min(left,   position.x);
        right   = std::max(right,  position.x);
        bottom  = std::min(bottom, position.y);
        top     = std::max(top,    position.y);
        near    = std::min(near,   position.z);
        far     = std::max(far,    position.z);
    }

    return BoundingAABox(left - FR_TINY, right + FR_TINY,
                         bottom - FR_TINY, top + FR_TINY,
                         near - FR_TINY, far + FR_TINY);
}

#endif
