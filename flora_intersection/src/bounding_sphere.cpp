#include <flora_intersection.h>

BoundingSphere::BoundingSphere()
    : BoundingSphere(FR_VEC3_ZERO, 0)
{ }

BoundingSphere::BoundingSphere(frVector4 position, double radius)
    : position(position), radius(radius)
{ }

frVector4 BoundingSphere::getPosition()
{
    return position;
}

double BoundingSphere::getRadius()
{
    return radius;
}

BoundingSphere BoundingSphere::convertToSphere() const
{
    return *this;
}

BoundingSphere BoundingSphere::getTransformed(frMatrix4x4 matrix) const
{
    frVector4 newPos = frMatrixApply(&matrix, position);
    frVector4 radiusPoint = frMatrixApply(&matrix,
                                          FR_VEC3_ADD(position,
                                                      FR_VEC3(radius, 0, 0)));

    return BoundingSphere(newPos, frVector3Distance(newPos, radiusPoint));
}
