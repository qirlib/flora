#include <flora_intersection.h>

BoundingAABox::BoundingAABox(double left, double right,
                             double bottom, double top,
                             double near, double far)
{
    if(left == right)
    {
        left -= SIDE_SIZE_PADDING;
        right += SIDE_SIZE_PADDING;
    }

    if(bottom == top)
    {
        bottom -= SIDE_SIZE_PADDING;
        top += SIDE_SIZE_PADDING;
    }

    if(near == far)
    {
        near -= SIDE_SIZE_PADDING;
        far += SIDE_SIZE_PADDING;
    }

    this->leftv = left; this->rightv = right;
    this->bottomv = bottom; this->topv = top;
    this->nearv = near; this->farv = far;
}

BoundingAABox::BoundingAABox(BoundingSphere sphere)
{
    frVector4 pos = sphere.getPosition();
    double radius = sphere.getRadius();

    if(radius == 0)
        radius = SIDE_SIZE_PADDING;

    leftv = pos.x - radius;
    rightv = pos.x + radius;
    bottomv = pos.y - radius;
    topv = pos.y + radius;
    nearv = pos.z - radius;
    farv = pos.z + radius;
}

BoundingAABox& BoundingAABox::operator=(const BoundingAABox& other)
{
    leftv = other.leftv;
    rightv = other.rightv;
    bottomv = other.bottomv;
    topv = other.topv;
    nearv = other.nearv;
    farv = other.farv;

    return *this;
}

BoundingAABox& BoundingAABox::operator=(BoundingAABox&& other) noexcept
{
    leftv = other.leftv;
    rightv = other.rightv;
    bottomv = other.bottomv;
    topv = other.topv;
    nearv = other.nearv;
    farv = other.farv;

    return *this;
}

double BoundingAABox::left()
{
    return leftv;
}

double BoundingAABox::right()
{
    return rightv;
}

double BoundingAABox::bottom()
{
    return bottomv;
}

double BoundingAABox::top()
{
    return topv;
}

double BoundingAABox::near()
{
    return nearv;
}

double BoundingAABox::far()
{
    return farv;
}

frVector4 BoundingAABox::getNNNCorner() const
{
    return FR_VEC3(leftv, bottomv, nearv);
}

frVector4 BoundingAABox::getNNPCorner() const
{
    return FR_VEC3(leftv, bottomv, farv);
}

frVector4 BoundingAABox::getNPNCorner() const
{
    return FR_VEC3(leftv, topv, nearv);
}

frVector4 BoundingAABox::getNPPCorner() const
{
    return FR_VEC3(leftv, topv, farv);
}

frVector4 BoundingAABox::getPNNCorner() const
{
    return FR_VEC3(rightv, bottomv, nearv);
}

frVector4 BoundingAABox::getPNPCorner() const
{
    return FR_VEC3(rightv, bottomv, farv);
}

frVector4 BoundingAABox::getPPNCorner() const
{
    return FR_VEC3(rightv, topv, nearv);
}

frVector4 BoundingAABox::getPPPCorner() const
{ 
    return FR_VEC3(rightv, topv, farv);
}

frVector4 BoundingAABox::getCenter() const
{
    return FR_VEC3((rightv + leftv) / 2,
                   (topv + bottomv) / 2,
                   (farv + nearv) / 2);
}

BoundingSphere BoundingAABox::convertToSphere() const
{
    return BoundingSphere(getCenter(),
                          frVector3Distance(getPPPCorner(), getCenter()));
}

BoundingAABox BoundingAABox::getTransformed(frMatrix4x4 matrix) const
{
    frVector4 nnn = frMatrixApply(&matrix, getNNNCorner());
    frVector4 nnp = frMatrixApply(&matrix, getNNPCorner());
    frVector4 npn = frMatrixApply(&matrix, getNPNCorner());
    frVector4 npp = frMatrixApply(&matrix, getNPPCorner());
    frVector4 pnn = frMatrixApply(&matrix, getPNNCorner());
    frVector4 pnp = frMatrixApply(&matrix, getPNPCorner());
    frVector4 ppn = frMatrixApply(&matrix, getPPNCorner());
    frVector4 ppp = frMatrixApply(&matrix, getPPPCorner());

    double left = std::min({ nnn.x, nnp.x, npn.x, npp.x,
                             pnn.x, pnp.x, ppn.x, ppp.x });
    double right = std::max({ nnn.x, nnp.x, npn.x, npp.x,
                              pnn.x, pnp.x, ppn.x, ppp.x });
    double bottom = std::min({ nnn.y, nnp.y, npn.y, npp.y,
                               pnn.y, pnp.y, ppn.y, ppp.y });
    double top = std::max({ nnn.y, nnp.y, npn.y, npp.y,
                            pnn.y, pnp.y, ppn.y, ppp.y });
    double near = std::min({ nnn.z, nnp.z, npn.z, npp.z,
                             pnn.z, pnp.z, ppn.z, ppp.z });
    double far = std::max({ nnn.z, nnp.z, npn.z, npp.z,
                            pnn.z, pnp.z, ppn.z, ppp.z });

    return BoundingAABox(left, right, bottom, top, near, far);
}

void BoundingAABox::expandBy(BoundingAABox other)
{
    leftv = std::min(leftv, other.leftv);
    rightv = std::max(rightv, other.rightv);
    bottomv = std::min(bottomv, other.bottomv);
    topv = std::max(topv, other.topv);
    nearv = std::min(nearv, other.nearv);
    farv = std::max(farv, other.farv);
}

void BoundingAABox::expandBy(BoundingSphere sphere)
{
    frVector4 pos = sphere.getPosition();
    double radius = sphere.getRadius();

    leftv = std::min(leftv, pos.x - radius);
    rightv = std::max(rightv, pos.x + radius);
    bottomv = std::min(bottomv, pos.y - radius);
    topv = std::max(topv, pos.y + radius);
    nearv = std::min(nearv, pos.z - radius);
    farv = std::max(farv, pos.z + radius);
}

void BoundingAABox::expandTiny()
{
    leftv -= FR_TINY;
    rightv += FR_TINY;
    bottomv -= FR_TINY;
    topv += FR_TINY;
    nearv -= FR_TINY;
    farv += FR_TINY;
}
