#include <flora_renderer.h>

frColor frRgb(frColorComponent, frColorComponent, frColorComponent);

void frColorPrint(frColor color)
{
    printf("(%u; %u; %u)\n",
           frColorR(color),
           frColorG(color),
           frColorB(color));
    
    fflush(stdout);
}
