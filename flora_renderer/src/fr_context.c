#include <flora_renderer.h>

bool frContextInit(frContext* context,
                   size_t maxExpectedWidth,
                   size_t maxExpectedHeight,
                   frContextInitFlags flags)
{
    if(!context || maxExpectedWidth == 0 || maxExpectedHeight == 0)
        return false;

    frBuffer2d frame = frBuffer2dGetInvalid();
    frBuffer2d zBuffer = frBuffer2dGetInvalid();

    bool result = true;

    context->colorBufferEnabled = false;
    context->zBufferEnabled = false;

    if((flags & FR_CONTEXT_INIT_DISABLE_COLOR_BUFFER) == 0)
    {
        result = frBuffer2dCreate(&frame,
                                  maxExpectedWidth,
                                  maxExpectedHeight,
                                  maxExpectedWidth *
                                      maxExpectedHeight,
                                  sizeof(uint32_t));

        context->colorBufferEnabled = true;
    }

    if(!result)
        return false;

    if((flags & FR_CONTEXT_INIT_DISABLE_Z_BUFFER) == 0)
    {
        result = frBuffer2dCreate(&zBuffer,
                                  maxExpectedWidth,
                                  maxExpectedHeight,
                                  maxExpectedWidth *
                                      maxExpectedHeight,
                                  sizeof(uint32_t));

        context->zBufferEnabled = true;
    }

    if(!result)
        return false;

    context->frame = frame;
    context->zBuffer = zBuffer;

    context->fragmentShader = NULL;
    context->uniformFsData = NULL;

    context->cullingMode = frCullingModeBack;
    context->clippingMode = frClippingModeNear;

    context->maxExpectedWidth = maxExpectedWidth;
    context->maxExpectedHeight = maxExpectedHeight;

    context->wireframeMode = false;

    return true;
}

void frContextDestroy(frContext* context)
{
    if(!context)
        return;

    frBuffer2dDestroy(&context->frame);
    frBuffer2dDestroy(&context->zBuffer);
}

bool frStartNewFrame(frContext* context, size_t width, size_t height)
{
    if(!context || width == 0 || height == 0)
        return false;

    bool result = true;

    context->width = width;
    context->height = height;

    if(context->colorBufferEnabled)
        result &= frBuffer2dResize(&context->frame, width, height);

    if(context->zBufferEnabled)
        result &= frBuffer2dResize(&context->zBuffer, width, height);

    if(!result)
        return false;

    uint32_t color = (uint32_t)0xFF << 24;
    uint32_t zBufferValue = 0xFFFFFFFF;

    if(context->colorBufferEnabled)
        frBuffer2dFill(&context->frame, &color);

    if(context->zBufferEnabled)
        frBuffer2dFill(&context->zBuffer, &zBufferValue);

    return true;
}
