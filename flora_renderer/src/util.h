#ifndef FLORAINC_FLORA_RENDERER_SRC_UTIL_H
#define FLORAINC_FLORA_RENDERER_SRC_UTIL_H

/*! Swap x and y of type type. */
#define FR_SWAP(x, y, type) { type tmp = (x); (x) = (y); (y) = tmp; }

#define FR_MIN(x, y) (((x) < (y)) ? (x) : (y))
#define FR_MIN3(x, y, z) (((x) < (y)) ? (((x) < (z)) ? (x) : (z)) : \
                                        (((y) < (z)) ? (y) : (z)))

#define FR_MAX(x, y) (((x) > (y)) ? (x) : (y))
#define FR_MAX3(x, y, z) (((x) > (y)) ? (((x) > (z)) ? (x) : (z)) : \
                                        (((y) > (z)) ? (y) : (z)))

#define FR_CLAMP_VALUE(x, low, high) \
    { if((x) < (low)) x = low; else if((x) > (high)) x = high; }

#endif
