#include <flora_renderer.h>

/*! \file
 * There are a POINT_INSIDE_X (POINT_OUTSIDE_X) macroses.
 * They test on which side of plane X point v lies.
 * POINT_INSIDE_X() returns true if point lies on the same side as
 * viewing volume. POINT_OUTSIDE_X() returns true if point lies
 * on a different side than the viewing volume.
 *
 * There are a lot of X_PLANE_FORMULA_NOM [DENOM] macroses.
 * They represent nominators (denominators) of formulas
 * for calculating intersection of plane and line in homogeneous
 * coordinates. v0 and v1 stand for beginning and end of the line,
 * X defines the plane against which the line is clipped.
 *
 * Each clipping algorithm first uses Cohen-Sutherland clipping codes
 * to determine if point, line of triangle should be trivially accepted
 * or rejected.
 *
 * Triangle clipping is done by creating a polygon.
 * Polygon is defined by a series
 * of verticles of type FanVertex. Every FanVertex has a position and
 * barycentric coordinates calculated relative to the base triangle.
 * Polygon is then divided
 * into triangles using fan-like pattern. Then each triangle is
 * rendered using drawClippedTriangle().
 */

#include "util.h"

/*!
 * Convert vector from clip coordinates to normalized coordinates.
 * @param vector Vector in question.
 * @return vector in normalized coordinates.
 */
FR_CF static inline frVector4 convertToNormalizedCoordinates(frVector4 vector)
{
    double wInversed = 1.0 / vector.w;

    vector.x *= wInversed;
    vector.y *= wInversed;
    vector.z *= wInversed;

    return vector;
}

/*!
 * Convert vector from normalized coordinates to viewport coordinates.
 * Function also caches reverse of biased z.
 *
 * @param vector Vector in question.
 * @param width Width of the viewport.
 * @param height Height of the viewport.
 * @return Vector with x and y in viewport coordinates and cached w.
 */
FR_CF static inline frVector4 convertToViewportCoordinates(frVector4 vector,
                                                           size_t width,
                                                           size_t height)
{
    vector.x = (vector.x + 1) * (width - 1) / 2.0;
    vector.y = (1 - vector.y) * (height - 1) / 2.0;

    vector.w = 1 / (vector.z + 2);

    return vector;
}

/*!
 * A shortcut function for converting from clip coordinates to
 * viewport coordinates.
 * @param vector Vector in question.
 * @param width Width of the viewport.
 * @param height Height of the viewport.
 * @return Vector with x and y in viewport coordinates and cached w.
 */
static inline frVector4
    convertFromClippingToViewportCoordinates(frVector4 vector,
                                             size_t width,
                                             size_t height)
{
    return
        convertToViewportCoordinates(convertToNormalizedCoordinates(vector),
                                     width, height);
}

/*!
 * Convert normalized z-coordinate into 32-bit integer value.
 * @param z Z-coordinate in normalized coordinates.
 * @return 32-bit integer normalized z-coordinate representation.
 */
FR_CF static inline uint32_t getUintZValue(double z)
{
    return (uint32_t)round((z + 1) * ((double)UINT32_MAX / 2.0));
}

/*!
 * Test whenether point is visible, update z-Buffer if so.
 * @param buffer Buffer in question.
 * @param zValue 32-bit z value.
 * @return True if point passed z-Buffer test, false otherwise.
 */
static bool zBufferTest(frBuffer2d* buffer,
                        size_t x,
                        size_t y,
                        uint32_t zValue)
{
    if(zValue <= FR_BUFP_EL(buffer, y, x, uint32_t))
    {
        FR_BUFP_EL(buffer, y, x, uint32_t) = zValue;
        return true;
    }

    return false;
}

/*! See the file documentation for details */
#define POINT_INSIDE_LEFT(v)   ((v).x > -(v).w)
#define POINT_INSIDE_RIGHT(v)  ((v).x < (v).w)
#define POINT_INSIDE_DOWN(v)   ((v).y > -(v).w)
#define POINT_INSIDE_UP(v)     ((v).y < (v).w)
#define POINT_INSIDE_NEAR(v)   ((v).z > -(v).w)
#define POINT_INSIDE_FAR(v)    ((v).z < (v).w)

/*! See the file documentation for details */
#define POINT_OUTSIDE_LEFT(v)   ((v).x < -(v).w)
#define POINT_OUTSIDE_RIGHT(v)  ((v).x > (v).w)
#define POINT_OUTSIDE_DOWN(v)   ((v).y < -(v).w)
#define POINT_OUTSIDE_UP(v)     ((v).y > (v).w)
#define POINT_OUTSIDE_NEAR(v)   ((v).z < -(v).w)
#define POINT_OUTSIDE_FAR(v)    ((v).z > (v).w)

/*!
 * Get Cohen–Sutherland clipping codes for point v.
 * @param v Point to be tested.
 * @return Cohen-Sutherland clipping codes.
 */
FR_CF static inline uint8_t getClippingCodes(frVector4 v)
{
    return (POINT_OUTSIDE_RIGHT(v) << 0) |
           (POINT_OUTSIDE_UP(v)    << 1) |
           (POINT_OUTSIDE_FAR(v)   << 2) |
           (POINT_OUTSIDE_LEFT(v)  << 3) |
           (POINT_OUTSIDE_DOWN(v)  << 4) |
           (POINT_OUTSIDE_NEAR(v)  << 5);
}

/*!
 * Test if Cohen–Sutherland clipping codes of a point allow it to be
 * trivially accepted.
 * @param clippingCodes Cohen–Sutherland clipping codes.
 * @return True if point can be trivially accepted, false otherwise.
 */
FR_CF static inline bool triviallyAcceptPoint(uint8_t clippingCodes)
{
    return clippingCodes == 0;
}

/*!
 * Draw point.
 * @param context Context in question.
 * @param v0Ptr Pointer to a verticle.
 * @param v0 Position of the verticle.
 */
static inline void drawPoint(frContext* context, void* v0Ptr, frVector4 v0)
{
    const size_t width = context->width;
    const size_t height = context->height;

    if(!triviallyAcceptPoint(getClippingCodes(v0)) || v0.w == 0)
        return;

    v0 = convertFromClippingToViewportCoordinates(v0, width, height);

    size_t x = (size_t)round(v0.x);
    size_t y = (size_t)round(v0.y);

    uint32_t v0zUint32 = getUintZValue(v0.z);

    if(context->zBufferEnabled &&
       !zBufferTest(&context->zBuffer, x, y, v0zUint32))
    {
        return;
    }
    
    if(!context->colorBufferEnabled)
        return;

    frColor color = context->fragmentShader(context->uniformFsData,
                                            v0Ptr, NULL, NULL,
                                            1, 0, 0);

    frSetPixel(&context->frame, x, y, color);
}

/*!
 * Determine if line should be trivially rejected.
 * @param cc1 Clipping codes for the start of the line.
 * @param cc2 Clipping codes for the end of the line.
 * @return True if line should be trivially rejected, false otherwise.
 */
static inline bool triviallyRejectLine(uint8_t cc1, uint8_t cc2)
{
    return (cc1 & cc2) != 0;
}

/*!
 * Return point on a line defined in clip coordinates
 * using vector equation (using vector as direction
 * and a as parameter).
 * @param origin Start of the line.
 * @param vector Direction vector.
 * @param a Parameter.
 * @return A point on the line.
 */
static inline frVector4 parametericLinePoint(frVector4 origin,
                                             frVector4 vector,
                                             double a)
{
    return FR_VEC4(origin.x + vector.x * a,
                   origin.y + vector.y * a,
                   origin.z + vector.z * a,
                   origin.w + vector.w * a);
}

/*! See the file documentation for details */
#define LEFT_PLANE_FORMULA_NOM(v0, v1) (v0.x + v0.w)
#define LEFT_PLANE_FORMULA_DENOM(v0, v1) (v0.x + v0.w - v1.x - v1.w)
#define RIGHT_PLANE_FORMULA_NOM(v0, v1) (v0.w - v0.x)
#define RIGHT_PLANE_FORMULA_DENOM(v0, v1) (v0.w - v1.w - v0.x + v1.x)
#define DOWN_PLANE_FORMULA_NOM(v0, v1) (v0.w + v0.y)
#define DOWN_PLANE_FORMULA_DENOM(v0, v1) (v0.w + v0.y - v1.w - v1.y)
#define UP_PLANE_FORMULA_NOM(v0, v1) (v0.w - v0.y)
#define UP_PLANE_FORMULA_DENOM(v0, v1) (v0.w - v1.w - v0.y + v1.y)
#define NEAR_PLANE_FORMULA_NOM(v0, v1) (v0.w + v0.z)
#define NEAR_PLANE_FORMULA_DENOM(v0, v1) (v0.w + v0.z - v1.w - v1.z)
#define FAR_PLANE_FORMULA_NOM(v0, v1) (v0.w - v0.z)
#define FAR_PLANE_FORMULA_DENOM(v0, v1) (v0.w - v1.w - v0.z + v1.z)

/*! Expands to code which clips line against one of 6 intersection planes.
 * Parameters ins1-4 should be set to adjacent intersection planes.
 * Used in clipLine(). Makes sense in context.
 * @param planeiq Plane in question.
 */
#define CLIP_AGAINST_PLANE(planeiq, ins1, ins2, ins3, ins4) \
{ \
    double implcapden = planeiq##_PLANE_FORMULA_DENOM(v0, v1); \
    if((implcapden) != 0) \
    { \
        double a = planeiq##_PLANE_FORMULA_NOM(v0, v1) / (implcapden); \
        if(a >= 0 && a <= 1) \
        { \
            frVector4 point = parametericLinePoint(v0, direction, a); \
            if(POINT_INSIDE_##ins1(point) && \
               POINT_INSIDE_##ins2(point) && \
               POINT_INSIDE_##ins3(point) && \
               POINT_INSIDE_##ins4(point)) \
            { \
                clipped += 1; \
                if(POINT_OUTSIDE_##planeiq(v0)) \
                    t0 = fmax(t0, a); \
                else \
                    t1 = fmin(t1, a); \
            } \
        } \
    } \
}

/*!
 * Clip line against viewing volume.
 * @param v0Ptr The beginning of the line. After execution value referenced
 * is set to the new beginning of the line.
 * @param v1Ptr The end of the line. After execution value referenced
 * is set to new end of the line.
 * @param t0Ptr Scalar in the vector equation representing beginning of
 * the line. After execution value
 * referenced is set to new beginning of the line.
 * @param t1Ptr Scalar in the vector equation representing end of the line.
 * After execution value referenced is set to new end of the line.
 * @return True if line is visible and should be drawn, false otherwise.
 */
static inline bool clipLine(frVector4* v0Ptr, frVector4* v1Ptr,
                            double* t0Ptr, double* t1Ptr)
{
    frVector4 v0 = *v0Ptr;
    frVector4 v1 = *v1Ptr;

    uint8_t v0ClippingCodes = getClippingCodes(v0);
    uint8_t v1ClippingCodes = getClippingCodes(v1);

    /* number of valid points */
    int clipped = -2 + triviallyAcceptPoint(v0ClippingCodes)
                     + triviallyAcceptPoint(v1ClippingCodes);

    /* trivially accept */
    if(clipped == 0)
        return true;

    if(triviallyRejectLine(v0ClippingCodes, v1ClippingCodes))
        return false;

    frVector4 direction = FR_VEC4_SUB(v1, v0);

    double t0 = 0, t1 = 1;

    CLIP_AGAINST_PLANE(LEFT, DOWN, UP, NEAR, FAR)
    CLIP_AGAINST_PLANE(RIGHT, DOWN, UP, NEAR, FAR)
    CLIP_AGAINST_PLANE(DOWN, LEFT, RIGHT, NEAR, FAR)
    CLIP_AGAINST_PLANE(UP, LEFT, RIGHT, NEAR, FAR)
    CLIP_AGAINST_PLANE(NEAR, LEFT, RIGHT, DOWN, UP)
    CLIP_AGAINST_PLANE(FAR, LEFT, RIGHT, DOWN, UP)

    if(t0 > t1 || clipped < 0)
        return false;

    *t0Ptr = t0;
    *t1Ptr = t1;

    *v0Ptr = parametericLinePoint(v0, direction, t0);
    *v1Ptr = parametericLinePoint(v0, direction, t1);

    return true;
}

/*!
 * Return value representing sign of argument.
 * @param value Number to be examined.
 * @return 1 if value > 0, -1 if value < 0, 0 otherwise.
 */
static inline double sign(double value)
{
    return value > 0 ? 1 : (value < 0 ? -1 : 0);
}

/*!
 * Draw a fragment of a line defined by vertices v0Ptr and v1Ptr.
 * @param context Context.
 * @param v0Ptr Pointer to vertex 0.
 * @param v1Ptr Pointer to vertex 1.
 * @param v0 Position of the beginning of the fragment.
 * @param v1 Position of the end of the fragment.
 * @param t0 Scalar from vector equation representing beginning of the fragment.
 * @param t1 Scalar from vector equation representing end of the fragment.
 */
static inline void drawClippedLine(frContext* context,
                                   void* v0Ptr, void* v1Ptr,
                                   frVector4 v0, frVector4 v1,
                                   double t0, double t1)
{
    const bool colorBufferEnabled = context->colorBufferEnabled;
    const bool zBufferNotEnabled = !context->zBufferEnabled;

    frBuffer2d* const zBuffer = &context->zBuffer;

    const frFsFunc fragmentShader = context->fragmentShader;

    v0.x = round(v0.x);
    v0.y = round(v0.y);

    v1.x = round(v1.x);
    v1.y = round(v1.y);

    uint32_t v0zUint32 = getUintZValue(v0.z);
    uint32_t v1zUint32 = getUintZValue(v1.z);

    frColor color;

    const double t0Rev = 1 - t0;
    const double t1Rev = 1 - t1;

    if(v0.x == v1.x && v0.y == v1.y)
    {
        if(v0zUint32 > v1zUint32)
        {
            FR_SWAP(v0, v1, frVector4)
            FR_SWAP(v0Ptr, v1Ptr, void*)
            FR_SWAP(v0zUint32, v1zUint32, uint32_t)
        }

        if(context->zBufferEnabled &&
           !zBufferTest(zBuffer, v0.x, v0.y, v0zUint32))
        {
            return;
        }

        if(!context->colorBufferEnabled)
            return;

        color = fragmentShader(context->uniformFsData,
                               v0Ptr, v1Ptr, NULL,
                               t0Rev, t0, 0);

        frSetPixel(&context->frame, v0.x, v0.y, color);
    }

    int x = (int)v0.x, y = (int)v0.y;

    int dx = (int)fabs(v1.x - v0.x), dy = (int)fabs(v1.y - v0.y);
    int sx = (int)sign(v1.x - v0.x), sy = (int)sign(v1.y - v0.y);

    int exchange = dx <= dy;
    if(exchange)
    FR_SWAP(dx, dy, int)

    int error = (dy << 1) - dx;
    int i = 0;

    double b = 1, bStep = -1 / (double)dx;
    double z, i0, i1;

    if(exchange)
    {
        while(i <= dx)
        {
            z = 1 / (b * v0.w + (1 - b) * v1.w);

            if(zBufferNotEnabled || zBufferTest(zBuffer,
                                                x,
                                                y,
                                                getUintZValue(z - 2)))
            {
                if(colorBufferEnabled)
                {
                    i0 = z * (v0.w * b * t0Rev + v1.w * (1 - b) * t1Rev);
                    i1 = z * (v0.w * b * t0 + v1.w * (1 - b) * t1);

                    color = fragmentShader(context->uniformFsData,
                                           v0Ptr, v1Ptr, NULL,
                                           i0, i1, 0);

                    frSetPixel(&context->frame, x, y, color);
                }
            }

            if(error > 0)
            {
                x += sx; error -= dx << 1;
            }

            y += sy;
            error += dy << 1;

            b += bStep;

            i++;
        }
    }
    else
    {
        while(i <= dx)
        {
            z = 1 / (b * v0.w + (1 - b) * v1.w);

            if(zBufferNotEnabled || zBufferTest(zBuffer,
                                                x,
                                                y,
                                                getUintZValue(z - 2)))
            {
                if(colorBufferEnabled)
                {
                    i0 = z * (v0.w * b * t0Rev + v1.w * (1 - b) * t1Rev);
                    i1 = z * (v0.w * b * t0 + v1.w * (1 - b) * t1);

                    color = fragmentShader(context->uniformFsData,
                                           v0Ptr, v1Ptr, NULL,
                                           i0, i1, 0);

                    frSetPixel(&context->frame, x, y, color);
                }
            }

            if(error > 0)
            {
                y += sy; error -= dx << 1;
            }

            x += sx;
            error += dy << 1;

            b += bStep;

            i++;
        }
    }
}

/*!
 * Draw line defined by vertices v0Ptr and v1Ptr.
 * @param context Context.
 * @param v0Ptr Vertex 0.
 * @param v1Ptr Vertex 1.
 * @param v0 Position of vertex 0.
 * @param v1 Position of vertex 1.
 */
static inline void drawLine(frContext* context,
                            void* v0Ptr, void* v1Ptr,
                            frVector4 v0, frVector4 v1)
{
    const size_t width = context->width;
    const size_t height = context->height;

    double t0 = 0, t1 = 1;
    
    if(!clipLine(&v0, &v1, &t0, &t1))
        return;

    v0 = convertFromClippingToViewportCoordinates(v0, width, height);
    v1 = convertFromClippingToViewportCoordinates(v1, width, height);

    drawClippedLine(context, v0Ptr, v1Ptr, v0, v1, t0, t1);
}

/*!
 * Returns scalar value of cross product of c - a and c - b.
 * @return Length of cross product of c - a and c - b.
 */
static inline double pseudoCross(const frVector4 a,
                                 const frVector4 b,
                                 const frVector4 c)
{
    return (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x);
}

/**
 * Determines if triangle should be trivially rejected.
 * @param cc0 Cohen–Sutherland clipping codes of verticle 0.
 * @param cc1 Cohen–Sutherland clipping codes of verticle 1.
 * @param cc2 Cohen–Sutherland clipping codes of verticle 2.
 * @return True if triangle should be trivially rejected, false otherwise.
 */
static inline bool triviallyRejectTriangle(uint8_t cc0,
                                           uint8_t cc1,
                                           uint8_t cc2)
{
    return (cc0 & cc1 & cc2) != 0;
}

/*!
 * Determine if triangle should be culled.
 * @param v0 Position of vertex 0 in viewport coordinates.
 * @param v1 Position of vertex 1 in viewport coordinates.
 * @param v2 Position of vertex 2 in viewport coordinates.
 * @param cullingMode Current culling mode.
 * @return True if triangle can be discarded, false otherwise.
 */
static inline bool cullTriangle(frVector4 v0,
                                frVector4 v1,
                                frVector4 v2,
                                frCullingMode cullingMode)
{
    double area = pseudoCross(v0, v1, v2);

    if(cullingMode == frCullingModeNone)
        return area != 0;

    return (area < 0) ^ cullingMode;
}

#define FAN_VERTICLES_MAX_COUNT (6 + 2)

/*!
 * Verticle of a subtriangle.
 * See file documentation for details.
 */
typedef struct
{
    frVector4 v; /*!< Position. */

    double i0; /*!< Barycentric coordinate i0 of v, */
    double i1; /*!< Barycentric coordinate i1 of v. */
    double i2; /*!< Barycentric coordinate i2 of v. */
} FanVertex;

/*! Verticle which state is said to be invalid */
#define INVALID_FAN_VERTEX (FanVertex){ FR_VEC4_INVALID, -1, -1, -1 }

/*! Check if verticle vert is valid. */
#define VALID_FAN_VERTEX(vert) ((vert).i0 != -1)

/*! Interpolate barycentric coordinates to get new ones. Used in clipTriangle.
 * Makes sense in context. */
#define CALCULATE_BARYCENTRIC \
src[i - 1].i0 * (1 - a) + src[i].i0 * a, \
src[i - 1].i1 * (1 - a) + src[i].i1 * a, \
src[i - 1].i2 * (1 - a) + src[i].i2 * a

/*! Used in clipTriangle. Makes sense in context. */
#define CLIP_TRIANGLE_LINE_AGAINST_PLANE(plane) \
{ \
    i = 1; j = 0; \
    while(VALID_FAN_VERTEX(src[i])) \
    { \
        direction = FR_VEC4_SUB(src[i].v, src[i - 1].v); \
        denominator = plane##_PLANE_FORMULA_DENOM(src[i - 1].v, src[i].v); \
        if(denominator != 0) \
        { \
            a = plane##_PLANE_FORMULA_NOM(src[i - 1].v, src[i].v) \
                / denominator; \
            if(a >= 0 && a <= 1) \
            { \
                point = parametericLinePoint(src[i - 1].v, direction, a); \
                dest[j++] = (FanVertex) { point, CALCULATE_BARYCENTRIC }; \
            } \
        } \
        if(POINT_INSIDE_##plane(src[i].v)) dest[j++] = src[i]; \
        i++; \
    } \
    dest[j++] = dest[0]; dest[j] = INVALID_FAN_VERTEX; \
    FR_SWAP(src, dest, FanVertex*) \
}

/*!
 * Clip triangle against viewing volume.
 * @param v0Original Position of triangle vertex 0.
 * @param v1Original Position of triangle vertex 1.
 * @param v2Original Position of triangle vertex 2.
 * @param fanVerticlesArray1 Algorithm temporary storage 1.
 * @param fanVerticlesArray2 Algorithm temporary storage 2.
 * @param clippingMode Clipping mode currently used.
 * @return Pointer to array containing FanVerticles which can
 * be used for drawing clipped triangle.
 */
static inline FanVertex* clipTriangle(frVector4 v0Original,
                                      frVector4 v1Original,
                                      frVector4 v2Original,
                                      FanVertex* fanVerticlesArray1,
                                      FanVertex* fanVerticlesArray2,
                                      frClippingMode clippingMode)
{
    fanVerticlesArray1[0] = (FanVertex){ v0Original, 1, 0, 0 };
    fanVerticlesArray1[1] = (FanVertex){ v1Original, 0, 1, 0 };
    fanVerticlesArray1[2] = (FanVertex){ v2Original, 0, 0, 1 };
    fanVerticlesArray1[3] = (FanVertex){ v0Original, 1, 0, 0 };
    fanVerticlesArray1[4] = INVALID_FAN_VERTEX;

    FanVertex* src = fanVerticlesArray1;
    FanVertex* dest = fanVerticlesArray2;

    frVector4 point, direction;

    double a;
    int i, j;
    double denominator;

    switch(clippingMode)
    {
    case frClippingModeAll:
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(LEFT)
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(RIGHT)
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(DOWN)
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(UP)
    case frClippingModeNearFar:
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(FAR)
    case frClippingModeNear:
        CLIP_TRIANGLE_LINE_AGAINST_PLANE(NEAR)
    }

    return src;
}

/*!
 * Draw fully visible triangle defined by v0Ptr, v1Ptr and v2Ptr.
 * @param context Context.
 * @param v0Ptr Pointer to vertex 0.
 * @param v1Ptr Pointer to vertex 1.
 * @param v2Ptr Pointer to vertex 2.
 * @param v0 Position of vertex 0.
 * @param v1 Position of vertex 1.
 * @param v2 Position of vertex 2.
 */
static inline void drawTrivialTriangle(frContext* context,
                                       void* v0Ptr, void* v1Ptr, void* v2Ptr,
                                       frVector4 v0,
                                       frVector4 v1,
                                       frVector4 v2)
{
    frBuffer2d* frame = &context->frame;
    frBuffer2d* zBuffer = &context->zBuffer;
    frFsFunc fragmentShader = context->fragmentShader;
    void* uniformFsData = context->uniformFsData;

    const bool colorBufferEnabled = context->colorBufferEnabled;
    const bool zBufferNotEnabled = !context->zBufferEnabled;

    int minX = (int)round(FR_MIN3(v0.x, v1.x, v2.x));
    int minY = (int)round(FR_MIN3(v0.y, v1.y, v2.y));

    int maxX = (int)round(FR_MAX3(v0.x, v1.x, v2.x));
    int maxY = (int)round(FR_MAX3(v0.y, v1.y, v2.y));

    frVector4 p;
    frColor color;

    double i0, i1, i2;
    double areaInv = 1 / pseudoCross(v0, v1, v2);

    double z;

    uint32_t pointZUint32;

    if(areaInv < 0)
    {
        areaInv *= -1;
        FR_SWAP(v0, v2, frVector4)
        FR_SWAP(v0Ptr, v2Ptr, void*)
    }

    for(int y = minY; y <= maxY; y++)
    {
        for(int x = minX; x <= maxX; x++)
        {
            p = FR_VEC3(x + 0.5, y + 0.5, 0);

            i0 = pseudoCross(v1, v2, p);
            i1 = pseudoCross(v2, v0, p);
            i2 = pseudoCross(v0, v1, p);

            if(i0 >= 0 && i1 >= 0 && i2 >= 0)
            {
                i0 *= areaInv; i1 *= areaInv; i2 *= areaInv;
                z = 1 / (i0 * v0.w + i1 * v1.w + i2 * v2.w);

                pointZUint32 = getUintZValue(z - 2);

                if(zBufferNotEnabled || zBufferTest(zBuffer,
                                                    x, y,
                                                    pointZUint32))
                {
                    if(colorBufferEnabled)
                    {
                        i0 *= z * v0.w;
                        i1 *= z * v1.w;
                        i2 *= z * v2.w;

                        color = fragmentShader(uniformFsData,
                                               v0Ptr, v1Ptr, v2Ptr,
                                               i0, i1, i2);

                        frSetPixel(frame, x, y, color);
                    }
                }
            }
        }
    }
}

/*!
 * Draw a subtriangle of a triangle defined by v0Ptr, v1Ptr, v2Ptr.
 * @param context Context.
 * @param width Width of the viewport.
 * @param height Height of the viewport.
 * @param v0Ptr Pointer to vertex 0.
 * @param v1Ptr Pointer to vertex 1.
 * @param v2Ptr Pointer to vertex 2.
 * @param v0 FanVerticle 0 of subtriangle.
 * @param v1 FanVerticle 1 of subtriangle.
 * @param v2 FanVerticle 2 of subtriangle.
 */
static inline void drawClippedTriangle(frContext* context,
                                       size_t width, size_t height,
                                       void* v0Ptr, void* v1Ptr, void* v2Ptr,
                                       FanVertex v0,
                                       FanVertex v1,
                                       FanVertex v2)
{
    frBuffer2d* frame = &context->frame;
    frBuffer2d* zBuffer = &context->zBuffer;
    frFsFunc fragmentShader = context->fragmentShader;
    void* uniformFsData = context->uniformFsData;

    const bool colorBufferEnabled = context->colorBufferEnabled;
    const bool zBufferNotEnabled = !context->zBufferEnabled;

    int minX = (int)round(FR_MIN3(v0.v.x, v1.v.x, v2.v.x));
    int minY = (int)round(FR_MIN3(v0.v.y, v1.v.y, v2.v.y));

    int maxX = (int)round(FR_MAX3(v0.v.x, v1.v.x, v2.v.x));
    int maxY = (int)round(FR_MAX3(v0.v.y, v1.v.y, v2.v.y));

    FR_CLAMP_VALUE(minX, 0, (int)width - 1)
    FR_CLAMP_VALUE(maxX, 0, (int)width - 1)

    FR_CLAMP_VALUE(minY, 0, (int)height - 1)
    FR_CLAMP_VALUE(maxY, 0, (int)height - 1)

    frVector4 p;
    frColor color;

    double i0, i1, i2, k0, k1, k2;
    double areaInv = 1 / pseudoCross(v0.v, v1.v, v2.v);

    double z;
    uint32_t pointZUint32;

    bool v0v2swapped = areaInv < 0;
    if(v0v2swapped)
    {
        areaInv *= -1;
        FR_SWAP(v0, v2, FanVertex)
        FR_SWAP(v0Ptr, v2Ptr, void*)
    }

    for(int y = minY; y <= maxY; y++)
    {
        for(int x = minX; x <= maxX; x++)
        {
            p = FR_VEC3(x + 0.5, y + 0.5, 0);

            i0 = pseudoCross(v1.v, v2.v, p);
            i1 = pseudoCross(v2.v, v0.v, p);
            i2 = pseudoCross(v0.v, v1.v, p);

            if(i0 >= 0 && i1 >= 0 && i2 >= 0)
            {
                i0 *= areaInv; i1 *= areaInv; i2 *= areaInv;
                z = 1 / (i0 * v0.v.w + i1 * v1.v.w + i2 * v2.v.w);

                pointZUint32 = getUintZValue(z - 2);

                if(zBufferNotEnabled || zBufferTest(zBuffer,
                                                    x, y,
                                                    pointZUint32))
                {
                    if(colorBufferEnabled)
                    {
                        k0 = z * (i0 * (v0.v.w * v0.i0) +
                                  i1 * (v1.v.w * v1.i0) +
                                  i2 * (v2.v.w * v2.i0));

                        k1 = z * (i0 * (v0.v.w * v0.i1) +
                                  i1 * (v1.v.w * v1.i1) +
                                  i2 * (v2.v.w * v2.i1));

                        k2 = z * (i0 * (v0.v.w * v0.i2) +
                                  i1 * (v1.v.w * v1.i2) +
                                  i2 * (v2.v.w * v2.i2));

                        if(v0v2swapped)
                            FR_SWAP(k0, k2, double)

                        color = fragmentShader(uniformFsData,
                                               v0Ptr, v1Ptr, v2Ptr,
                                                   k0, k1, k2);

                        frSetPixel(frame, x, y, color);
                    }
                }
            }
        }
    }
}

/*!
 * Possibly clip and draw triangle defined by v0Ptr, v1Ptr, v2Ptr.
 * @param context Context.
 * @param v0Ptr Vertex 0.
 * @param v1Ptr Vertex 1.
 * @param v2Ptr Vertex 2.
 * @param v0 Position of vertex 0.
 * @param v1 Position of vertex 1.
 * @param v2 Position of vertex 2.
 */
static inline void drawTriangle(frContext* context,
                                void* v0Ptr, void* v1Ptr, void* v2Ptr,
                                frVector4 v0, frVector4 v1, frVector4 v2)
{
    if(v0.w == 0 || v1.w == 0 || v2.w == 0)
        return;

    uint8_t v0ClippingCodes = getClippingCodes(v0);
    uint8_t v1ClippingCodes = getClippingCodes(v1);
    uint8_t v2ClippingCodes = getClippingCodes(v2);

    if(triviallyRejectTriangle(v0ClippingCodes,
                               v1ClippingCodes,
                               v2ClippingCodes))
    {
        return;
    }

    if(!cullTriangle(convertToNormalizedCoordinates(v0),
                     convertToNormalizedCoordinates(v1),
                     convertToNormalizedCoordinates(v2),
                     context->cullingMode))
    {
        return;
    }

    size_t width = context->width;
    size_t height = context->height;

    if(triviallyAcceptPoint(v0ClippingCodes) &&
       triviallyAcceptPoint(v1ClippingCodes) &&
       triviallyAcceptPoint(v2ClippingCodes))
    {
        if(!context->wireframeMode)
        {
            v0 = convertFromClippingToViewportCoordinates(v0, width, height);
            v1 = convertFromClippingToViewportCoordinates(v1, width, height);
            v2 = convertFromClippingToViewportCoordinates(v2, width, height);

            drawTrivialTriangle(context, v0Ptr, v1Ptr, v2Ptr, v0, v1, v2);
        }
        else
        {
            drawLine(context, NULL, NULL, v0, v1);
            drawLine(context, NULL, NULL, v1, v2);
            drawLine(context, NULL, NULL, v2, v0);
        }

        return;
    }

    FanVertex fanVerticles1[FAN_VERTICLES_MAX_COUNT];
    FanVertex fanVerticles2[FAN_VERTICLES_MAX_COUNT];

    FanVertex* result;
    if(!(result = clipTriangle(v0, v1, v2,
                               fanVerticles1, fanVerticles2,
                               context->clippingMode)))
    {
        return;
    }

    FanVertex f0, f1, f2;

    int i = 2;
    f0 = result[0];
    f1 = result[1];

    if(!context->wireframeMode)
    {
        f0.v = convertFromClippingToViewportCoordinates(f0.v, width, height);
        f1.v = convertFromClippingToViewportCoordinates(f1.v, width, height);

        while(VALID_FAN_VERTEX(result[i + 1]))
        {
            f2 = result[i];

            f2.v = convertFromClippingToViewportCoordinates(f2.v,
                                                            width,
                                                            height);

            drawClippedTriangle(context, width, height,
                                v0Ptr, v1Ptr, v2Ptr, f0, f1, f2);

            f1 = f2;

            i++;
        }
    }
    else
    {
        drawLine(context, NULL, NULL, f0.v, f1.v);

        while(VALID_FAN_VERTEX(result[i + 1]))
        {
            f2 = result[i];

            drawLine(context, NULL, NULL, f0.v, f2.v);
            drawLine(context, NULL, NULL, f1.v, f2.v);

            f1 = f2;

            i++;
        }
    }
}

/*!
 * Return white color.
 * Shader used when drawing wireframe.
 * @return FR_WHITE
 */
static frColor wireframeFragmentShader(
    FR_UNUSED void* c,
    FR_UNUSED void* d,  FR_UNUSED void* e,  FR_UNUSED void* f,
    FR_UNUSED double g, FR_UNUSED double h, FR_UNUSED double i)
{
    return FR_WHITE;
}

void frDraw(frContext* context,
            frVsFunc vertexShader,
            frFsFunc fragmentShader,
            void* uniformVsData,
            void* uniformFsData,
            frPrimitiveType type,
            const void* vertices,
            size_t vertexSize,
            size_t verticesCount)
{
    if(!context || !vertices || vertexSize == 0 || verticesCount == 0)
        return;

    context->fragmentShader = fragmentShader;
    context->uniformFsData = uniformFsData;

    void* v0Ptr;
    void* v1Ptr;
    void* v2Ptr;

    frVector4 v0, v1, v2;

    switch(type)
    {
    case frPrimitiveTypeTriangles:
        if(context->wireframeMode)
            context->fragmentShader = wireframeFragmentShader;

        for(size_t i = 0; i < verticesCount; i += 3)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, i);
            v1Ptr = FR_ARR_EL(vertices, vertexSize, i + 1);
            v2Ptr = FR_ARR_EL(vertices, vertexSize, i + 2);

            v0 = vertexShader(v0Ptr, uniformVsData);
            v1 = vertexShader(v1Ptr, uniformVsData);
            v2 = vertexShader(v2Ptr, uniformVsData);

            drawTriangle(context, v0Ptr, v1Ptr, v2Ptr, v0, v1, v2);
        }

        break;
    case frPrimitiveTypeTrianglesStrip:
        if(verticesCount < 3)
            return;

        if(context->wireframeMode)
            context->fragmentShader = wireframeFragmentShader;

        bool flip = true;

        v0Ptr = FR_ARR_EL(vertices, vertexSize, 0);
        v1Ptr = FR_ARR_EL(vertices, vertexSize, 1);
        v2Ptr = FR_ARR_EL(vertices, vertexSize, 2);

        v0 = vertexShader(v0Ptr, uniformVsData);
        v1 = vertexShader(v1Ptr, uniformVsData);
        v2 = vertexShader(v2Ptr, uniformVsData);

        drawTriangle(context,
                     v0Ptr, v1Ptr, v2Ptr,
                     v0, v1, v2);

        for(size_t i = 3; i < verticesCount; i += 1)
        {
            if(flip)
            {
                v0Ptr = v2Ptr;
                v2Ptr = FR_ARR_EL(vertices, vertexSize, i);

                v0 = v2;
                v2 = vertexShader(v2Ptr, uniformVsData);
            }
            else
            {
                v1Ptr = v2Ptr;
                v2Ptr = FR_ARR_EL(vertices, vertexSize, i);

                v1 = v2;
                v2 = vertexShader(v2Ptr, uniformVsData);
            }
 
            drawTriangle(context,
                         v0Ptr, v1Ptr, v2Ptr,
                         v0, v1, v2);

            flip = !flip;
        }

        break;
    case frPrimitiveTypeTrianglesFan:
        if(verticesCount < 3)
            return;

        if(context->wireframeMode)
            context->fragmentShader = wireframeFragmentShader;

        v0Ptr = FR_ARR_EL(vertices, vertexSize, 0);
        v1Ptr = FR_ARR_EL(vertices, vertexSize, 1);
        v2Ptr = FR_ARR_EL(vertices, vertexSize, 2);

        v0 = vertexShader(v0Ptr, uniformVsData);
        v1 = vertexShader(v1Ptr, uniformVsData);
        v2 = vertexShader(v2Ptr, uniformVsData);

        drawTriangle(context,
                     v0Ptr, v1Ptr, v2Ptr,
                     v0, v1, v2);

        for(size_t i = 3; i < verticesCount; i += 1)
        {
            v1 = v2;
            v1Ptr = v2Ptr;

            v2Ptr = FR_ARR_EL(vertices, vertexSize, i);
            v2 = vertexShader(v2Ptr, uniformVsData);
 
            drawTriangle(context,
                         v0Ptr, v1Ptr, v2Ptr,
                         v0, v1, v2);
        }

        break;
    case frPrimitiveTypePoints:
        for(size_t i = 0; i < verticesCount; i++)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, i);

            v0 = vertexShader(v0Ptr, uniformVsData);

            drawPoint(context, v0Ptr, v0);
        }

        break;
    case frPrimitiveTypeLines:
        for(size_t i = 0; i < verticesCount; i += 2)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, i);
            v1Ptr = FR_ARR_EL(vertices, vertexSize, i + 1);

            v0 = vertexShader(v0Ptr, uniformVsData);
            v1 = vertexShader(v1Ptr, uniformVsData);

            drawLine(context, v0Ptr, v1Ptr, v0, v1);
        }

        break;
    default:
        break;
    }
}

void frDrawWithIndicies(frContext* context,
                        frVsFunc vertexShader,
                        frFsFunc fragmentShader,
                        void* uniformVsData,
                        void* uniformFsData,
                        frPrimitiveType type,
                        const void* vertices,
                        size_t vertexSize,
                        const unsigned int* indicies,
                        size_t indiciesCount)
{
    if(!context || !vertices ||
       vertexSize == 0 || !indicies || indiciesCount == 0)
    {
        return;
    }

    context->fragmentShader = fragmentShader;
    context->uniformFsData = uniformFsData;

    void* v0Ptr;
    void* v1Ptr;
    void* v2Ptr;

    frVector4 v0, v1, v2;

    switch(type)
    {
    case frPrimitiveTypeTriangles:
        if(context->wireframeMode)
            context->fragmentShader = wireframeFragmentShader;

        for(size_t i = 0; i < indiciesCount; i += 3)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i]);
            v1Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i + 1]);
            v2Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i + 2]);

            v0 = vertexShader(v0Ptr, uniformVsData);
            v1 = vertexShader(v1Ptr, uniformVsData);
            v2 = vertexShader(v2Ptr, uniformVsData);

            drawTriangle(context,
                         v0Ptr, v1Ptr, v2Ptr,
                         v0, v1, v2);
        }

        break;
    case frPrimitiveTypeTrianglesStrip:
        if(indiciesCount < 3)
            return;

        if(context->wireframeMode)
            context->fragmentShader = wireframeFragmentShader;

        bool flip = true;

        v0Ptr = FR_ARR_EL(vertices, vertexSize, indicies[0]);
        v1Ptr = FR_ARR_EL(vertices, vertexSize, indicies[1]);
        v2Ptr = FR_ARR_EL(vertices, vertexSize, indicies[2]);

        v0 = vertexShader(v0Ptr, uniformVsData);
        v1 = vertexShader(v1Ptr, uniformVsData);
        v2 = vertexShader(v2Ptr, uniformVsData);

        drawTriangle(context,
                     v0Ptr, v1Ptr, v2Ptr,
                     v0, v1, v2);

        for(size_t i = 3; i < indiciesCount; i++)
        {
            if(flip)
            {
                v0Ptr = v2Ptr;
                v2Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i]);

                v0 = v2;
                v2 = vertexShader(v2Ptr, uniformVsData);
            }
            else
            {
                v1Ptr = v2Ptr;
                v2Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i]);

                v1 = v2;
                v2 = vertexShader(v2Ptr, uniformVsData);
            }

            drawTriangle(context,
                         v0Ptr, v1Ptr, v2Ptr,
                         v0, v1, v2);

            flip = !flip;
        }

        break;
    case frPrimitiveTypePoints:
        for(size_t i = 0; i < indiciesCount; i++)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i]);

            v0 = vertexShader(v0Ptr, uniformVsData);

            drawPoint(context, v0Ptr, v0);
        }

        break;
    case frPrimitiveTypeLines:
        for(size_t i = 0; i < indiciesCount; i += 2)
        {
            v0Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i]);
            v1Ptr = FR_ARR_EL(vertices, vertexSize, indicies[i + 1]);

            v0 = vertexShader(v0Ptr, uniformVsData);
            v1 = vertexShader(v1Ptr, uniformVsData);

            drawLine(context, v0Ptr, v1Ptr, v0, v1);
        }

        break;
    default:
        break;
    }
}
