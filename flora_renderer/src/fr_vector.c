#include <flora_renderer.h>

double frVector2Length(double x, double y)
{
    return hypot(x, y);
}

frVector4 frVector3Create(double x, double y, double z)
{
    return (frVector4){ x, y, z, 1 };
}

frVector4 frVector3Add(frVector4 a, frVector4 b)
{
    return (frVector4){ a.x + b.x, a.y + b.y, a.z + b.z, 1 };
}

frVector4 frVector3Sub(frVector4 a, frVector4 b)
{
    return (frVector4){ a.x - b.x, a.y - b.y, a.z - b.z, 1 };
}

frVector4 frVector3Mul(frVector4 a, double b)
{
    return (frVector4){ a.x * b, a.y * b, a.z * b, 1 };
}

double frVector3Dot(frVector4 a, frVector4 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

frVector4 frVector3Cross(frVector4 a, frVector4 b)
{
    return (frVector4){ a.y * b.z - a.z * b.y,
                        a.z * b.x - a.x * b.z,
                        a.x * b.y - a.y * b.x,
                        1 };
}

double frVector3Length(frVector4 vector)
{
    return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}

double frVector3Distance(frVector4 a, frVector4 b)
{
    return frVector3Length(frVector3Sub(a, b));
}

frVector4 frVector3Normalize(frVector4 vector)
{
    double length = frVector3Length(vector);

    if(length == 0)
        return FR_VEC4_INVALID;

    length = 1 / length;

    return FR_VEC3(vector.x * length,
                   vector.y * length,
                   vector.z * length);
}

frVector4 frVector3DirectionByAngle(double yRot, double zRot)
{
    return FR_VEC3(cos(zRot) * cos(yRot), sin(zRot), cos(zRot) * sin(yRot));
}

bool frVector3Equal(frVector4 vector1, frVector4 vector2)
{
    return vector1.x == vector2.x &&
           vector1.y == vector2.y &&
           vector1.z == vector2.z;
}

frVector4 frVector3GetNormal2(frVector4 center, frVector4 a, frVector4 b)
{
    frVector4 dir1 = frVector3Sub(a, center);
    frVector4 dir2 = frVector3Sub(b, center);

    return frVector3Normalize(frVector3Cross(dir1, dir2));
}

frVector4 frVector3GetNormal3(frVector4 center,
                              frVector4 a, frVector4 b, frVector4 c)
{
    frVector4 dir1 = frVector3Sub(a, center);
    frVector4 dir2 = frVector3Sub(b, center);
    frVector4 dir3 = frVector3Sub(c, center);

    frVector4 cross1 = frVector3Cross(dir1, dir2);
    frVector4 cross2 = frVector3Cross(dir2, dir3);

    return frVector3Normalize(FR_MIXEQ_VEC3(cross1, cross2));
}

frVector4 frVector3GetNormal4Looped(frVector4 center,
                                    frVector4 a, frVector4 b,
                                    frVector4 c, frVector4 d)
{
    frVector4 dir1 = frVector3Sub(a, center);
    frVector4 dir2 = frVector3Sub(b, center);
    frVector4 dir3 = frVector3Sub(c, center);
    frVector4 dir4 = frVector3Sub(d, center);

    frVector4 cross1 = frVector3Cross(dir1, dir2);
    frVector4 cross2 = frVector3Cross(dir2, dir3);
    frVector4 cross3 = frVector3Cross(dir3, dir4);
    frVector4 cross4 = frVector3Cross(dir4, dir1);

    return frVector3Normalize(FR_MIX4EQ_VEC3(cross1, cross2, cross3, cross4));
}

frVector4 frVector4Create(double x, double y, double z, double w)
{
    return (frVector4){ x, y, z, w };
}

frVector4 frVector4Sub(frVector4 v1, frVector4 v2)
{
    return (frVector4){ v1.x - v2.x,
                        v1.y - v2.y,
                        v1.z - v2.z,
                        v1.w - v2.w };
}

void frVectorPrint(frVector4 v)
{
    printf("(%lf, %lf, %lf, %lf)\n", v.x, v.y, v.z, v.w);
    fflush(stdout);
}
