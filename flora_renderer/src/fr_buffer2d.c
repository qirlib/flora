#include <flora_renderer.h>

#include <string.h>
#include <tgmath.h>

#ifdef __unix__
#include <unistd.h>
#endif

bool frBuffer2dCreate(frBuffer2d* buffer,
                      size_t width,
                      size_t height,
                      size_t elementsToReserve,
                      size_t elementSize)
{ 
    if(!buffer)
        return false;

    elementsToReserve = fmax(width * height, elementsToReserve);

    size_t memorySize = elementsToReserve * elementSize;

#if _POSIX_C_SOURCE >= 200112L
    long pageSize = sysconf(_SC_PAGESIZE);

    if(posix_memalign(&buffer->data, pageSize, memorySize) != 0)
        buffer->data = malloc(memorySize);
#else
    buffer->data = malloc(memorySize);
#endif

    if(!buffer->data)
        return false;

    buffer->width = width;
    buffer->height = height;

    buffer->elementSize = elementSize;
    buffer->elementsToReserve = elementsToReserve;

    return true;
}

bool frBuffer2dResize(frBuffer2d* buffer, size_t width, size_t height)
{
    if(!buffer)
        return false;

    if(buffer->width == width && buffer->height == height)
        return true;

    if(width == 0 || height == 0)
        return false;

    if(width * height > buffer->elementsToReserve)
    {
        size_t spaceRequired = width * height * buffer->elementSize;

        unsigned char* newData = realloc(buffer->data, spaceRequired);

        if(newData == NULL)
            return false;

        buffer->data = newData;
        buffer->elementsToReserve = width * height;
    }

    buffer->width = width;
    buffer->height = height;

    return true;
}

void frBuffer2dFill(frBuffer2d* buffer, void* data)
{
    if(!buffer || !buffer->data || !data) return;

    size_t elementSize = buffer->elementSize;
    unsigned char* last = (unsigned char*)buffer->data +
                                          buffer->width * buffer->height *
                                              elementSize;

    for(unsigned char* ptr = buffer->data; ptr < last; ptr += elementSize)
        memcpy(ptr, data, elementSize);
}

void frBuffer2dClear(frBuffer2d* buffer)
{
    if(!buffer || !buffer->data) return;

    memset(buffer->data,
           0,
           buffer->elementSize * buffer->width * buffer->height);
}

void frBuffer2dDestroy(frBuffer2d* buffer)
{
    if(!buffer) return;

    free(buffer->data);
    buffer->data = NULL;

    buffer->width = buffer->height =
        buffer->elementSize = buffer->elementsToReserve = 0;
}

frBuffer2d frBuffer2dGetInvalid()
{
    return (frBuffer2d){ NULL, 0, 0, 0, 0 };
}

void frSetPixel(frBuffer2d* renderbuffer, size_t x, size_t y, uint32_t color);

bool frReformatDepthBuffer(frBuffer2d* zBuffer)
{
    if(!zBuffer)
        return false;

    uint32_t* last = &FR_BUFP_EL(zBuffer,
                                 zBuffer->height - 1,
                                 zBuffer->width,
                                 uint32_t);

    frColorComponent component;
    for(uint32_t* ptr = zBuffer->data; ptr < last; ptr++)
    {
        component = round(255.0 * log2(UINT32_MAX - *ptr) / log2(UINT32_MAX));

        *ptr = frRgb(component, component, component);
    }

    return true;
}
