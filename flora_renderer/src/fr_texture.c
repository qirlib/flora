#include <flora_renderer.h>

frTexture frGetInvalidTexture()
{
    return (frTexture){ NULL, (size_t)-1, (size_t)-1 };
}

frTexture frBufferToTexture(frBuffer2d* buffer)
{
    return (frTexture){ buffer->data, buffer->width, buffer->height };
}

frColor frAccessTexture(frTexture texture,
                        double u,
                        double v)
{
    size_t x = ((size_t) round(u * (texture.width - 1))) % texture.width;
    size_t y = ((size_t) round(v * (texture.height - 1))) % texture.height;

    return FR_TEX_EL(texture, x, y);
}

frColor frAccessTextureInterpolated(frTexture tex, double u, double v)
{
    double x = fmod(u * (tex.width - 1), tex.width);
    double y = fmod(v * (tex.height - 1), tex.height);

    size_t x0 = (size_t)trunc(x);
    size_t y0 = (size_t)trunc(y);

    int xAdd = x0 + 1 != tex.width ? 1 : -(int)x0;
    int yAdd = y0 + 1 != tex.height ? 1 : -(int)y0;

    double i1 = x - x0, i0 = 1 - i1;

    frColor color0 = FR_MIX_COLORS(FR_TEX_EL(tex, x0, y0),
                                   FR_TEX_EL(tex, x0 + xAdd, y0));
    frColor color1 = FR_MIX_COLORS(FR_TEX_EL(tex, x0, y0 + yAdd),
                                   FR_TEX_EL(tex, x0 + xAdd, y0 + yAdd));

    i1 = y - y0, i0 = 1 - i1;

    return FR_MIX_COLORS(color0, color1);
}
