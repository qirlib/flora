 #include <flora_renderer.h>

frMatrix4x4 frCreateMatrix(double i0, double i1, double i2, double i3,
                           double i4, double i5, double i6, double i7,
                           double i8, double i9, double i10, double i11,
                           double i12, double i13, double i14, double i15)
{
    return (frMatrix4x4){ { i0, i1, i2, i3,
                            i4, i5, i6, i7,
                            i8, i9, i10, i11,
                            i12, i13, i14, i15 } };
}

frMatrix4x4 frMatrixIdentity()
{
    return (frMatrix4x4){ { 1, 0, 0, 0,
                            0, 1, 0, 0,
                            0, 0, 1, 0,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateTranslation(double x, double y, double z)
{
    return (frMatrix4x4){ { 1, 0, 0, x,
                            0, 1, 0, y,
                            0, 0, 1, z,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateTranslationVec(frVector4 vec)
{
    return frMatrixCreateTranslation(vec.x, vec.y, vec.z);
}

frMatrix4x4 frMatrixCreateRotationX(double angle)
{
    return (frMatrix4x4){ { 1, 0, 0, 0,
                            0, cos(angle), -sin(angle), 0,
                            0, sin(angle), cos(angle), 0,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateRotationY(double angle)
{
    return (frMatrix4x4){ { cos(angle), 0, sin(angle), 0,
                            0, 1, 0, 0,
                            -sin(angle), 0, cos(angle), 0,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateRotationZ(double angle)
{
    return (frMatrix4x4){ { cos(angle), -sin(angle), 0, 0,
                            sin(angle), cos(angle), 0, 0,
                            0, 0, 1, 0,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateScale(double x, double y, double z)
{
    return (frMatrix4x4){ { x, 0, 0, 0,
                            0, y, 0, 0,
                            0, 0, z, 0,
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateFrustum(double left, double right,
                                  double bottom, double top,
                                  double near, double far)
{
    return (frMatrix4x4){ { 2 * near / (right - left),
                            0,
                            (right + left) / (right - left),
                            0,

                            0,
                            2 * near / (top - bottom),
                            (top + bottom) / (top - bottom),
                            0,

                            0,
                            0,
                            -(far + near) / (far - near),
                            -2 * far * near / (far - near),

                            0, 0, -1, 0  } };
}

frMatrix4x4 frMatrixCreateOrtho(double left, double right,
                                double bottom, double top,
                                double near, double far)
{
    return (frMatrix4x4){ { 2 / (right - left),
                            0,
                            0,
                            -(right + left) / (right - left),
                           
                            0,
                            2 / (top - bottom),
                            0,
                            -(top + bottom) / (top - bottom),

                            0,
                            0,
                            -2 / (far - near),
                            -(far + near) / (far - near),

                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixCreateOrthoCorrected(double fov,
                                         double aspectRatio,
                                         double near,
                                         double far)
{
    double top = (far + near) / 2 * tan(FR_PI / 360 * fov);
    double right = top * aspectRatio;

    return frMatrixCreateOrtho(-right, right, -top, top, near, far);
}

frMatrix4x4 frMatrixCreatePerspective(double fov,
                                      double aspectRatio,
                                      double near,
                                      double far)
{
    double top = near * tan(FR_PI / 360.0 * fov);
    double right = top * aspectRatio;

    return frMatrixCreateFrustum(-right, right, -top, top, near, far);  
}

frMatrix4x4 frMatrixCreateLookAt(frVector4 eye,
                                 frVector4 target,
                                 frVector4 up)
{
    frVector4 x, y, z;
    z = frVector3Normalize(
            FR_VEC3(eye.x - target.x, eye.y - target.y, eye.z - target.z));

    y = up;

    x = frVector3Normalize(frVector3Cross(y, z));
    y = frVector3Normalize(frVector3Cross(z, x));

    return (frMatrix4x4){ { x.x, x.y, x.z, -frVector3Dot(x, eye),
                            y.x, y.y, y.z, -frVector3Dot(y, eye),
                            z.x, z.y, z.z, -frVector3Dot(z, eye),
                            0, 0, 0, 1 } };
}

frMatrix4x4 frMatrixMultiply(const frMatrix4x4 src1, const frMatrix4x4 src2)
{
    frMatrix4x4 result;

    double sum;

    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {            
            sum = 0;

            for(int k = 0; k < 4; k++)
            {
                sum += FR_MDATA_EL(src1.data, i, k) *
                       FR_MDATA_EL(src2.data, k, j);
            }

            FR_MDATA_EL(result.data, i, j) = sum;
        }
    }

    return result;
}

frMatrix4x4 frMatricesMultiplyv(va_list list)
{
    frMatrix4x4 arg = va_arg(list, frMatrix4x4);
    frMatrix4x4 result = frMatrixIdentity();
    
    while(isfinite(arg.data[0]))
    {
        result = frMatrixMultiply(result, arg);

        arg = va_arg(list, frMatrix4x4);
    }

    return result;
}

frMatrix4x4 frImplMatricesMultiply(int unused, ...)
{
    va_list list;
    va_start(list, unused);

    frMatrix4x4 result = frMatricesMultiplyv(list);

    va_end(list);
    return result;
}

frMatrix4x4 frMatrixInverse(frMatrix4x4 input)
{
    union frUnitedVector
    {
        frVector4 vector4;
        double array[4];
    };

    frMatrix4x4 rc = frMatrixIdentity(), rn = frMatrixIdentity();
    union frUnitedVector cc;
    union frUnitedVector lf;

    double den;

    for(size_t j = 0; j < 4; j++)
    {
        for(size_t i = 0; i < 4; i++)
            cc.array[i] = FR_MDATA_EL(input.data, i, j);

        cc.array[j] -= 1;

        lf.vector4 = frMatrixApply(&rc, cc.vector4);
        den = 1 + lf.array[j];

        if(!fabs(den))
            return frMatrixIdentity();

        for(size_t i = 0; i < 4; i++)
            for(size_t k = 0; k <= j; k++)
            {
                FR_MDATA_EL(rn.data, i, k) -= lf.array[i] * 
                                              FR_MDATA_EL(rc.data, j, k) /
                                              den;
            }

        for(size_t i = 0; i < 4; i++)
            for(size_t k = 0; k <= j; k++)
                FR_MDATA_EL(rc.data, i, k) = FR_MDATA_EL(rn.data, i, k);
    }

    return rc;
}

frMatrix4x4 frMatrixTranspose(frMatrix4x4 input)
{
    frMatrix4x4 result;

    for(size_t i = 0; i < 4; i++)
        for(size_t j = 0; j < 4; j++)
            FR_MDATA_EL(result.data, i, j) = FR_MDATA_EL(input.data, j, i);

    return result;
}

frVector4 frMatrixApply(const frMatrix4x4* matrix, const frVector4 v)
{
    const double* m = matrix->data;

    return (frVector4)
        { m[0] * v.x + m[1] * v.y + m[2] * v.z + m[3] * v.w,
          m[4] * v.x + m[5] * v.y + m[6] * v.z + m[7] * v.w,
          m[8] * v.x + m[9] * v.y + m[10] * v.z + m[11] * v.w,
          m[12] * v.x + m[13] * v.y + m[14] * v.z + m[15] * v.w };
}

void frMatrixPrint(const frMatrix4x4* matrix)
{
    if(!matrix) return;
    const double* data = matrix->data;

    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
            printf("%lf ", *data++);

        putchar('\n');
    }
}
