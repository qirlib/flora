#ifndef FLORA_RENDERER_H
#define FLORA_RENDERER_H

/*! \file
 * The header file of flora_renderer library.
 * This library provides means to draw 3d objects onto a virtual
 * screen represented by a buffer which can be passed to external
 * applications.
 * This is done by creating a context and drawing a frame.
 * Frame is drawn by calling frStartNewFrame() and then calling
 * frDraw*() functions with context parameters set up as user desires.
 * This library uses a concept called a programmable pipeline.
 * The user passes an array of verticles which positions are determined
 * by calling user-defined vertex shader, supplying it with user-defined
 * data called vertex uniform. Then the renderer construct primitives
 * which determines which pixels are to be drawn. Then the pixels
 * (assuming color buffer is enabled) are filled with colors
 * which are obtained by calling user-defined fragment shader which
 * uses user-defined data called fragment uniform.
 * There is no finishFrame() function. Information can be queried
 * just as the last call to any frDraw*() function is finished.
 *
 * There are a lot of frVector*() functions.
 * All of them work with frVector4, which is a 4-element vector.
 * However, frVector3*() functions treat frVector4 objects
 * as if they were 3-element vectors, ignoring w element.
 *
 * There are a lot of FR_VEC* shortcuts for frVector*() functions.
 *
 * There are a lot of FR_MIX* functions, which interpolate two or more
 * objects using values stored in variables i0, i1, i2. It is assumed
 * that these variables are already defined.
 *
 * This library can optionally use some non-C99 POSIX functions
 * in fr_buffer2d.c.
 * FR_CF is used to indicate that function result depends only on its
 * immediate arguments (no external memory accesses allowed).
 * FR_UNUSED is used to mark unused attributes.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(__unix__) && !defined(_POSIX_C_SOURCE)
#define _POSIX_C_SOURCE 200112L
#endif

#ifdef __GNUC__
#define FR_CF __attribute__((const))
#define FR_UNUSED __attribute__((unused))
#else
#define FR_CF
#define FR_UNUSED
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>

/*! Color used to represent that something is wrong. */
#define FR_INVALID_COLOR_REPRESENTATION (frRgb(0, 0, 127))

/*! Get pointer to i-th array element in arr with element size being elsize. */
#define FR_ARR_EL(arr, elsize, i) \
    ((void*)(&((unsigned char*)arr)[(elsize) * (i)]))

/* math */

/*! The PI number constant. */
#define FR_PI   (3.14159265358979323846)
/*! A small number. */
#define FR_TINY (0.001)

/*! Convert x from degrees to radians. */
#define FR_TO_RADIANS(x) ((x) * FR_PI / 180.0)
/*! Convert x from radians to degrees. */
#define FR_TO_DEGREES(x) ((x) * 180.0 / FR_PI)

/*! Interpolate x and y using user-defined variables i0 and i1 as weights. */
#define FR_MIX(x, y) ((x) * i0 + (y) * i1)
/*! Interpolate x, y and z using user-defined variables i0, i1 and i2
 * as weights. */
#define FR_MIX3(x, y, z) ((x) * i0 + (y) * i1 + (z) * i2)

/*! Interpolate x and y in equal parts. */
#define FR_MIXEQ(x, y) ((x) * 0.5 + (y) * 0.5)
/*! Interpolate x, y and w in equal parts. */
#define FR_MIX3EQ(x, y, z) \
    ((x) * (1 / 3.0) + (y) * (1 / 3.0) + (z) * (1 / 3.0))
/*! Interpolate x, y, z, w in equal parts. */
#define FR_MIX4EQ(x, y, z, w) \
    ((x) * (1 / 4.0) + (y) * (1 / 4.0) + (z) * (1 / 4.0) + (w) * (1 / 4.0))

/* frVector4 */

/*!
 * 4-element vector.
 */
typedef struct 
{
    double x;
    double y;
    double z;
    double w;
} frVector4;

/*!
 * Return the length of (x, y) vector.
 * @param x X coordinate.
 * @param y Y coordinate.
 * @return The length of the vector.
 */
FR_CF double frVector2Length(double x, double y);

/*! Return (x, y, z) vector. w is set to be 1 */
FR_CF frVector4 frVector3Create(double x, double y, double z);
/*! Return x + y vector, treating them as 3-element vectors. */
FR_CF frVector4 frVector3Add(frVector4 x, frVector4 y);
/*! Return a - b vector, treating them as 3-element vectors. */
FR_CF frVector4 frVector3Sub(frVector4 a, frVector4 b);
/*! Return a * b vector, treating it as 3-element vector. */
FR_CF frVector4 frVector3Mul(frVector4 a, double b);
/*! Return dot product of a and b, treating them as 3-element vectors. */
FR_CF double frVector3Dot(frVector4 a, frVector4 b);
/*! Return cross product of a and b, treating them as 3-element vectors. */
FR_CF frVector4 frVector3Cross(frVector4 a, frVector4 b);
/*! Return length of vector, treating it as 3-element vector. */
FR_CF double frVector3Length(frVector4 vector);
/*! Return distance between a and b, treating them as 3-element vectors. */
FR_CF double frVector3Distance(frVector4 a, frVector4 b);
/*! Return normalized version of vector, treating it as 3-element vector. */
FR_CF frVector4 frVector3Normalize(frVector4 vector);
/*! Return vector pointing in specified direction,
 * treating it as 3-element vector. */
FR_CF frVector4 frVector3DirectionByAngle(double yRot, double xRot);
/*! Return if vector1 and vector2 are equal,
 * treating them as 3-element vectors. */
FR_CF bool frVector3Equal(frVector4 vector1, frVector4 vector2);

/*! Return normal to surface (center, center + a, center + b). */
FR_CF frVector4 frVector3GetNormal2(frVector4 center, frVector4 a, frVector4 b);
/*! Return normal obtained by interpolating normals to
 * surfaces (center, center + a, center + b) and
 * (center, center + b, center + c) */
FR_CF frVector4 frVector3GetNormal3(frVector4 center,
                                    frVector4 a, frVector4 b, frVector4 c);
/*! Return normal obtained by interpolating normals to surfaces
 * (center, center + a, center + b),
 * (center, center + b, center + c),
 * (center, center + c, center + d) and
 * (center, center + d, center + a) */
FR_CF frVector4 frVector3GetNormal4Looped(frVector4 center,
                                          frVector4 a,
                                          frVector4 b,
                                          frVector4 c,
                                          frVector4 d);

/*! Return (x, y, z, w) vector */
FR_CF frVector4 frVector4Create(double x, double y, double z, double w);
/*! Return a - b vector, treating them as 4-element vectors. */
FR_CF frVector4 frVector4Sub(frVector4 v1, frVector4 v2);

/*! Print vector to stdout. */
void frVectorPrint(frVector4 vector);

/* C++ does not support compound expressions */
#ifdef __cplusplus
#define FR_VEC3(x, y, z)      (frVector3Create((x), (y), (z)))
#define FR_VEC4(x, y, z, w)   (frVector4Create((x), (y), (z), (w)))
#else
#define FR_VEC3(x, y, z)      ((frVector4){ (x), (y), (z), 1 })
#define FR_VEC4(x, y, z, w)   ((frVector4){ (x), (y), (z), (w) })
#endif

#define FR_VEC3_ZERO          (FR_VEC3(0, 0, 0))
#define FR_VEC4_INVALID       (FR_VEC4(NAN, NAN, NAN, NAN))

#define FR_VEC3_EQ(x, y)      (frVector3Equal((x), (y)))
#define FR_VEC3_ADD(x, y)     (frVector3Add((x), (y)))
#define FR_VEC3_MUL(x, y)     (frVector3Mul((x), (y)))
#define FR_VEC3_NEG(a)        (FR_VEC3(-(a).x, -(a).y, -(a).z))
#define FR_VEC4_SUB(x, y)     (frVector4Sub((x), (y)))

#define FR_MIX_VEC4(a, b) FR_VEC4(FR_MIX((a).x, (b).x), \
                                  FR_MIX((a).y, (b).y), \
                                  FR_MIX((a).z, (b).z), \
                                  FR_MIX((a).w, (b).w))

#define FR_MIX3_VEC4(a, b, c) FR_VEC4(FR_MIX3((a).x, (b).x, (c).x), \
                                      FR_MIX3((a).y, (b).y, (c).y), \
                                      FR_MIX3((a).z, (b).z, (c).z), \
                                      FR_MIX3((a).w, (b).w, (c).w))

#define FR_MIXEQ_VEC3(a, b) FR_VEC3(FR_MIXEQ((a).x, (b).x), \
                                    FR_MIXEQ((a).y, (b).y), \
                                    FR_MIXEQ((a).z, (b).z))

#define FR_MIX3EQ_VEC3(a, b, c) FR_VEC3(FR_MIX3EQ((a).x, (b).x, (c).x), \
                                        FR_MIX3EQ((a).y, (b).y, (c).y), \
                                        FR_MIX3EQ((a).z, (b).z, (c).z))

#define FR_MIX4EQ_VEC3(a, b, c, d) \
    FR_VEC3(FR_MIX4EQ((a).x, (b).x, (c).x, (d).x), \
            FR_MIX4EQ((a).y, (b).y, (c).y, (d).y), \
            FR_MIX4EQ((a).z, (b).z, (c).z, (d).z))

/* frMatrix4x4 */

/*!
 * Represents 4x4 matrix.
 */
typedef struct
{
    double data[16];
} frMatrix4x4;

/*!
 * Create matrix using values supplied.
 * @return Newly created matrix.
 */
FR_CF frMatrix4x4 frCreateMatrix(double, double, double, double,
                                 double, double, double, double,
                                 double, double, double, double,
                                 double, double, double, double);

/*! Access matrix field (i, j) by data. */
#define FR_MDATA_EL(d, i, j) (d[(i) * 4 + (j)])

/* Create a matrix with x being value of every field. */
#define FR_MATRIX_REP(x) (frCreateMatrix(x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x))
#define FR_MATRIX_INVALID FR_MATRIX_REP(NAN)

/*! Return Identity matrix, */
FR_CF frMatrix4x4 frMatrixIdentity();

/*! Return translation matrix. */
FR_CF frMatrix4x4 frMatrixCreateTranslation(double x, double y, double z);
/*! Return translation matrix. */
FR_CF frMatrix4x4 frMatrixCreateTranslationVec(frVector4 vec);
/*! Return rotation matrix, rotating around x. */
FR_CF frMatrix4x4 frMatrixCreateRotationX(double angle);
/*! Return rotation matrix, rotating around y. */
FR_CF frMatrix4x4 frMatrixCreateRotationY(double angle);
/*! Return rotation matrix, rotating around z. */
FR_CF frMatrix4x4 frMatrixCreateRotationZ(double angle);
/*! Return scale matrix. */
FR_CF frMatrix4x4 frMatrixCreateScale(double x, double y, double z);
/*! Return frustum-to-cube perspective projection matrix. */
FR_CF frMatrix4x4 frMatrixCreateFrustum(double left, double right,
                                        double bottom, double top,
                                        double near, double far);
/*! Return cube-to-cube orthographic matrix. */
FR_CF frMatrix4x4 frMatrixCreateOrtho(double left, double right,
                                      double bottom, double top,
                                      double near, double far);
/*! Return cube-to-cube orthographic matrix which is created using the
 * same values as frMatrixCreatePerspective() uses. */
FR_CF frMatrix4x4 frMatrixCreateOrthoCorrected(double fov,
                                               double aspectRatio,
                                               double near,
                                               double far);
/*!
 * Return frustum-to-cube projection matrix.
 * @param fov Field of view.
 * @param aspectRatio Aspect ratio (width divided by height).
 * @param near Near plane.
 * @param far Far plane.
 * @return
 */
FR_CF frMatrix4x4 frMatrixCreatePerspective(double fov,
                                            double aspectRatio,
                                            double near,
                                            double far);
/*! Return view matrix which represents camera being in eye,
 * looking at target with up vector begin up. */
FR_CF frMatrix4x4 frMatrixCreateLookAt(frVector4 eye,
                                       frVector4 target,
                                       frVector4 up);

/*! Return multiplication of matrices src1 and src2. */
FR_CF frMatrix4x4 frMatrixMultiply(const frMatrix4x4 src1,
                                   const frMatrix4x4 src2);

FR_CF frMatrix4x4 frMatricesMultiplyv(va_list list);
/*! Implementation function. Do not access directly. */
FR_CF frMatrix4x4 frImplMatricesMultiply(int, ...);

/*! Multiply matrices supplied. */
#define frMatricesMultiply(...) \
    (frImplMatricesMultiply(0, __VA_ARGS__, FR_MATRIX_INVALID))

/*! Calculate the inverse of matrix m, */
FR_CF frMatrix4x4 frMatrixInverse(frMatrix4x4 m);
/*! Calculate the transposed version of matrix m, */
FR_CF frMatrix4x4 frMatrixTranspose(frMatrix4x4 m);

/*! Transform vector v using matrix m. */
frVector4 frMatrixApply(const frMatrix4x4* m, const frVector4 v);

/*! Print matrix to stdout, */
void frMatrixPrint(const frMatrix4x4* matrix);

/* frColor */

/*! Color component type. 8-bit representation is used. */
typedef uint8_t frColorComponent;
/*! Color type. 32-bit ARGB representation is used. */
typedef uint32_t frColor;

/*! Extract red channel from color c. */
#define frColorR(c) (((c) >> 16) & 0xFF)
/*! Extract green channel from color c. */
#define frColorG(c) (((c) >> 8)  & 0xFF)
/*! Extract blue channel from color c. */
#define frColorB(c) ((c) & 0xFF)

/*! Construct frColor out of components. */
FR_CF inline frColor frRgb(frColorComponent r,
                           frColorComponent g,
                           frColorComponent b)
{
    return (255 << 24) | (r << 16) | (g << 8) | (b);
}

#define FR_MIX_COLORS(a, b) (frRgb(FR_MIX(frColorR(a), frColorR(b)), \
                                   FR_MIX(frColorG(a), frColorG(b)), \
                                   FR_MIX(frColorB(a), frColorB(b))))

#define FR_MIX3_COLORS(a, b, c) \
    (frRgb(FR_MIX3(frColorR(a), frColorR(b), frColorR(c)), \
           FR_MIX3(frColorG(a), frColorG(b), frColorG(c)), \
           FR_MIX3(frColorB(a), frColorB(b), frColorB(c))))

/*! Print color to stdout. */
void frColorPrint(frColor);

#define FR_WHITE  (frRgb(255, 255, 255))
#define FR_GRAY   (frRgb(127, 127, 127))
#define FR_BLACK  (frRgb(0, 0, 0))
#define FR_RED    (frRgb(255, 0, 0))
#define FR_GREEN  (frRgb(0, 255, 0))
#define FR_BLUE   (frRgb(0, 0, 255))
#define FR_YELLOW (frRgb(255, 255, 0))

/* frBuffer2d */

/*!
 * Buffer used to store two-dimensional arrays.
 */
typedef struct
{
    /*! Pointer to the allocated space. */
    void* data;

    /*! Logical width of the buffer, */
    size_t width;
    /*! Logical height of the buffer, */
    size_t height;

    /*! Size of the element of the buffer, */
    size_t elementSize;
    /*! Number or elements for which memory is reseved, */
    size_t elementsToReserve;
} frBuffer2d;

/*! Access frBuffer2d, treating it as linear buffer.
 * f is buffer, i is index, t is type. */
#define FR_BUF_LINEAR_EL(f, i, t) (((t*)((f).data))[i])
/*! Access frBuffer2d, treating it as linear buffer.
 * f is pointer to a buffer, i is index, t is type. */
#define FR_BUFP_LINEAR_EL(f, i) (((f)->data[(i) * (f)->elementSize]))
/*! Get pointer to (i, j) element of buffer f. */
#define FR_BUF_EL(f, i, j) \
    ((void*)(((unsigned char*)(f).data) \
        [(f).elementSize * ((i) * ((f).width) + (j))]))
/*! Get pointer to (i, j) element of buffer specified by pointer f. */
#define FR_BUFP_EL(f, i, j, type) \
    (((type*)(f)->data)[((i) * ((f)->width) + (j))])

/*!
 * Allocate memory and initialize buffer.
 * @param buffer Pointer to buffer object to be initialized.
 * @param width Requested width.
 * @param height Requested height.
 * @param elementsToReserve Requested number of elements to reserve.
 * @param elementSize Requested size of element.
 * @return Successfulness of operation.
 */
bool frBuffer2dCreate(frBuffer2d* buffer,
                      size_t width,
                      size_t height,
                      size_t elementsToReserve,
                      size_t elementSize);
/*!
 * Resize buffer according to width, height.
 * @param buffer Pointer to the buffer object to be resized.
 * @param width New width. Must be non-zero.
 * @param height new height. Must be non-zero.
 * @return Successfulness of operation.
 */
bool frBuffer2dResize(frBuffer2d* buffer, size_t width, size_t height);
/*!
 * Fill the buffer with copies of data.
 * @param buffer Pointer to the buffer to be filled.
 * @param data Data to be copied in.
 */
void frBuffer2dFill(frBuffer2d* buffer, void* data);
/*!
 * Clear the buffer (clear the memory, set all bytes to zero).
 * @param buffer Buffer.
 */
void frBuffer2dClear(frBuffer2d* buffer);
/*!
 * Deallocate the memory and invalidate buffer object.
 * @param buffer Buffer.
 */
void frBuffer2dDestroy(frBuffer2d* buffer);
/*! Return invalid buffer. */
frBuffer2d frBuffer2dGetInvalid();

/*!
 * Assign color to the pixel.
 * @param renderbuffer Renderbuffer in question.
 * @param x X coordinate.
 * @param y Y coordinate.
 * @param color Color to be assigned.
 */
inline void frSetPixel(frBuffer2d* renderbuffer,
                       size_t x,
                       size_t y,
                       uint32_t color)
{
    FR_BUFP_EL(renderbuffer, y, x, uint32_t) = color;
}

/*!
 * Convert z-Buffer to color buffer so that contents of z-Buffer
 * can be displayed.
 * @param zBuffer Buffer in question.
 * @return Successfulness of operation.
 */
bool frReformatDepthBuffer(frBuffer2d* zBuffer);

/* texturing */

/*! Texture access unit. It is meant for textures to be stored
 * using frBuffer2d.
 * A minimum amount of data
 * needed to access frBuffer2d. */
typedef struct
{
    frColor* data;
    size_t width;
    size_t height;
} frTexture;

/*! Access texture element of texture t by coordinates (x, y). */
#define FR_TEX_EL(t, x, y)  ((t).data[(y) * (t).width + (x)])

/*! Return invalid texture unit. */
frTexture frGetInvalidTexture();
/*! Create texture access unit out of frBuffer2d, */
frTexture frBufferToTexture(frBuffer2d* buffer);

/*! A function type for functions used to access textures. */
typedef frColor (*frAccessTextureFunc)(frTexture texture, double u, double v);

/*!
 * Return color by choosing nearest pixel in texture.
 * @param texture Texture in question.
 * @param u U-coordinate.
 * @param v V-coordinate.
 * @return Color yielded.
 */
frColor frAccessTexture(frTexture texture, double u, double v);
/*!
 * Return color by interpolating four nearest pixels in texture.
 * @param texture Texture in question.
 * @param u U-coordinate.
 * @param v V-coordinate.
 * @return Color yielded.
 */
frColor frAccessTextureInterpolated(frTexture texture, double u, double v);

/* shaders */

/*!
 * Vertex shader function type.
 * @param vertexIn vertexIn is a pointer to a vertex to be processed.
 * @param uniformVsData Pointer to uniform data for vertex shader.
 */
typedef frVector4 (*frVsFunc)(void* vertexIn, void* uniformVsData);
/*!
 * Fragment shader function type.
 * v0, v1 and v2 point to source verticles.
 * i0, i1 and i2 can be used to interpolate data.
 *
 * @param uniformFsData Pointer to fragment uniform data.
 * @param v0 Pointer to vertex 0.
 * @param v1 Pointer to vertex 1. If primitive being drawn is a point
 * then the value is undefined.
 * @param v2 Pointer to vertex 2. If primitive being drawn is not a triangle
 * then the value is undefined.
 * @param i0 Interpolation value 0. If primitive being drawn is a point
 * then the value is undefined.
 * @param i1 Interpolation value 1. If primitive being drawn is a point
 * then the value is undefined.
 * @param i2 Interpolation value 2. If primitive being drawn is not
 * a triangle then the value is undefined.
 */
typedef frColor (*frFsFunc)(void* uniformFsData,
                            void* v0, void* v1, void* v2,
                            double i0, double i1, double i2);

/* vertex */

/*! Type of primitive constructed from verticles during frDraw(). */
typedef enum
{
    frPrimitiveTypeNone,
    frPrimitiveTypePoints,
    frPrimitiveTypeLines,
    frPrimitiveTypeTriangles,
    frPrimitiveTypeTrianglesStrip,
    frPrimitiveTypeTrianglesFan
} frPrimitiveType;

/*! Determines the culling mode used. None means culling is disabled. */
typedef enum
{
    frCullingModeBack  = 0,
    frCullingModeFront = 1,
    frCullingModeNone  = 2
} frCullingMode;

/*! Determines clipping mode used. */
typedef enum
{
    frClippingModeNear = 0,
    frClippingModeNearFar = 1,
    frClippingModeAll = 2
} frClippingMode;

/*! Context is a set of color buffer and z-buffer and rendering options.
 * Drawing in different contexts is done independently. */
typedef struct
{
    frBuffer2d frame; /*!< Current frame. Read-only. */
    frBuffer2d zBuffer; /*!< z-Buffer. Read-only. */
    
    frFsFunc fragmentShader; /*!< Meant only for internal use. */
    void* uniformFsData; /*!< Meant only for internal use. */

    frCullingMode cullingMode; /*!< Current culling mode. */
    frClippingMode clippingMode;  /*!< Current clipping mode. */

    bool colorBufferEnabled;  /*!< Determines if color buffer is enabled. */
    bool zBufferEnabled;  /*!< Determines if z-Buffer is enabled. */

    size_t width; /*!< Width of the current frame. Read-only. */
    size_t height;  /*!< Height of the current frame. Read-only. */

    size_t maxExpectedWidth; /*!< Maximum expected width. Read-only. */
    size_t maxExpectedHeight; /*!< Maximum expected height. Read-only. */

    bool wireframeMode; /*!< Determines if wireframe mode is enabled. */
} frContext;

#define FR_CONTEXT_INIT_FLAGS_NONE            (0)
#define FR_CONTEXT_INIT_DISABLE_COLOR_BUFFER  (1 << 0)
#define FR_CONTEXT_INIT_DISABLE_Z_BUFFER      (1 << 1)

typedef int frContextInitFlags;

/*!
 * Create context.
 * Parameters maxExpected* do not impose any restrictions on the size of
 * the frame. They are for optimization purposes only.
 * @param context Pointer to context object.
 * @param maxExpectedWidth Maximum expected width.
 * @param maxExpectedHeight Maximum expected height.
 * @param flags If FR_CONTEXT_INIT_DISABLE_COLOR_BUFFER is set,
 * colorBufferEnabled is set to false and color buffer is not allocated.
 * If FR_CONTEXT_INIT_DISABLE_Z_BUFFER is set,
 * zBufferEnabled is set to false and z-Buffer is not allocated.
 * @return Successfulness of the operation.
 */
bool frContextInit(frContext* context,
                   size_t maxExpectedWidth,
                   size_t maxExpectedHeight,
                   frContextInitFlags flags);
/*!
 * Free all the resources assosiated with context.
 * @param context Context in question.
 */
void frContextDestroy(frContext* context);

/*!
 * Prepares the context for the drawing process of width x height frame.
 * @param context Context in question.
 * @param width Width of new frame.
 * @param height Height of new frame.
 * @return Successfulness of the operation.
 */
bool frStartNewFrame(frContext* context, size_t width, size_t height);

/*!
 * Draws requested primitive onto virtual screen using data supplied.
 * @param context Context in question.
 * @param vertexShader Vertex shader to be used.
 * @param fragmentShader Fragment shader to be used.
 * @param uniformVsData Vertex uniform to be used.
 * @param uniformFsData Fragment uniform to be used.
 * @param type Type of primitive to generate.
 * @param vertices Pointer to vertices array.
 * @param vertexSize Size of a vertex in bytes.
 * @param verticesCount Count of verticles.
 */
void frDraw(frContext* context,
            frVsFunc vertexShader,
            frFsFunc fragmentShader,
            void* uniformVsData,
            void* uniformFsData,
            frPrimitiveType type,
            const void* vertices,
            size_t vertexSize,
            size_t verticesCount);
/*!
 * Draws requested primitive onto virtual screen using data supplied.
 * @param context Context in question.
 * @param vertexShader Vertex shader to be used.
 * @param fragmentShader Fragment shader to be used.
 * @param uniformVsData Vertex uniform to be used.
 * @param uniformFsData Fragment uniform to be used.
 * @param type Type of primitive to generate. frPrimitiveTypeFan is not
 * supported.
 * @param vertices Pointer to vertices array.
 * @param vertexSize Size of a vertex in bytes.
 * @param indicies Pointer to indicies array.
 * @param indiciesCount Number of indicies.
 */
void frDrawWithIndicies(frContext* context,
                        frVsFunc vertexShader,
                        frFsFunc fragmentShader,
                        void* uniformVsData,
                        void* uniformFsData,
                        frPrimitiveType type,
                        const void* vertices,
                        size_t vertexSize,
                        const unsigned int* indicies,
                        size_t indiciesCount);

#ifdef __cplusplus
}
#endif

#endif
