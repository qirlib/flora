#ifndef FLORA_GENERATOR_H
#define FLORA_GENERATOR_H

#include <flora_renderer.h>
#include <flora_intersection.h>

#include <vector>
#include <string>

/*! \file
 * The header file of flora_generator library.
 * flora_generator allows to generate 3d models of various plants
 * using L-Systems. L-Systems are defined by user which must
 * supply them in a pre-defined special text format. Format is described in
 * project report. LSystem class can be used for storage of L-Systems.
 * LSystem class has interpret() function which generates a sequence of
 * tokens (string composed of letters) which can be later used by
 * TreeRenderData::regenerate(). regenerate() method
 * interprets a sequence of tokens by feeding it to a turtle and
 * generating rendering data which can be later used to render
 * the resulting 3d model.
 * For more details, see project report.
 */

/*!
 * Main namespace of flora_generator library.
 */
namespace FG
{
/*!
 * The namespace used to hide implementation details. Anything in it
 * must to be used by external users.
 */
namespace implfg
{

/*! Node of the skeleton tree constructed by turtle. */
struct TreeBranchingStructure
{
    double widthStart; /*!< Width of the beginning of the branch. */
    double widthEnd;  /*!< Width of the end of the branch. */
    double height;  /*!< Height of the branch. */

    frMatrix4x4 transform; /*!< Relative position of the branch. */

    TreeBranchingStructure* parent;
    std::vector<TreeBranchingStructure*> children;
};

/*! Encapsulates declaration of form A(a, b, c), where
 * A is name and a, b, c are parameters. */
struct Declaration
{
    std::string name;
    std::vector<std::string> parameters;
};

/*! A function. A part of LSystem internal
 * representation. Functions declared in the particular source of
 * an L-System are called general functions and the ones declared
 * in the source of this project are called built-in functions. */
class Function
{
public:
    virtual ~Function() = default;

    /*! Return the name of the function.
     * @return String which contains the function's name. */
    virtual const std::string& getName() = 0;
    /*! Return the type of function. For details, see documentation for
     * class Function.
     * @return True if function is built-in. */
    virtual bool isBuiltIn() = 0;

    /*! Return number of parameters acccepted by function.
     * @return Number of parameters. */
    virtual size_t getParametersCount() = 0;
    /*!
     * Evaluate function.
     * @param functions The array of functions which can be used during
     * evaluation.
     * @param arguments Arguments of the call.
     * @param log String which is filled with the evaluation attempt
     * information. The content depends on particular function.
     * @return The value yielded. The contents depends on particular function.
     */
    virtual double yield(std::vector<Function*>& functions,
                         std::vector<double> arguments,
                         std::string& log) = 0;
};

struct Expression;

/*! Represents function call or token derivation expression
 * of form A(expr1, expr2, ...)
 * where A is functionName and expr1, expr2, etc are expressions. */
struct FunctionCall
{
    std::string functionName;
    std::vector<Expression> expressions;
};

/*! Represents alias. For details, see documentation for members. */
struct Alias
{
    /*! Stores name of alias and names of parameters. */
    Declaration declaration;
    /*! Stores token derivation expressions, */
    std::vector<FunctionCall> calls;
};

/*! The type of value stored in ExpressionPart. */
enum class ExpressionPartDataType
{
    Identifier,
    Number,
    Operator,
    FunctionCall
};

/*! An element of array used for reverse-polish expression representation
 * stored in Expression. */
struct ExpressionPart
{
    /*! Construct container for either identifier of operator. */
    ExpressionPart(std::string id, ExpressionPartDataType type);
    /*! Construct container for number. */
    ExpressionPart(double number);
    /*! Construct container for function call. */
    ExpressionPart(FunctionCall call);

    ~ExpressionPart();

    ExpressionPart(const ExpressionPart&);
    ExpressionPart& operator=(const ExpressionPart&);

    union Data
    {
        Data();
        ~Data();

        std::string string;
        double number;
        FunctionCall call;
    } data;

    /*! Type of data stored in 'data' member. */
    ExpressionPartDataType type;
};

/* Represents expression. Reverse Polish notation is used. */
struct Expression
{
    /* An array representing expression organized
     * using Reverse Polish notation. */
    std::vector<ExpressionPart> parts;

    /*!
     * Yield the value of the expression. A list of identifiers that
     * correspond to specific values can be passed. Values are given
     * by arguments. Identifiers and arguments must be of the same size.
     * @param functions An array of functions that can be called
     * inside expression.
     * @param identifiers A list of parameters's names when
     * evaluating expression
     * which is a part of token derivation expression, empty array otherwise.
     * @param arguments A list of arguments (parameter's values)
     * when evaluating expression which is a part of token derivation
     * expression, empty array otherwise. Must be the same size with
     * identifiers.
     * @param log String which is filled with the information
     * about errors which occured during evaluation, if any. Left intact
     * if no errors occured.
     * @return The value of expression. NAN if any error occured.
     */
    double yield(std::vector<Function*>& functions,
                 const std::vector<std::string>& identifiers,
                 const std::vector<double>& arguments,
                 std::string& log);
};

/*! Represents the rule of an L-System. A part of internal representation
 * in LSystem. For details, see documentation for members. */
struct InterpretationRule
{
    /*! Stores the specification of token to be replaced. */
    Declaration declaration;
    /*! Stores probability-related value which is used to test if this
     * rule is to be selected among many. If a probability generated by
     * a random number generator is less than this value and
     * this rule is currently examined then this rule
     * is selected. See the implementations
     * of LSystem::verify() and LSystem::interpret() for more details. */
    double probabilityRange;

    /*! Stores token derivation expression (a sequence of tokens
     * which are inserted instead of source token). */
    std::vector<FunctionCall> calls;
};

}

/*! A token (letter) of the string to be fed to the turtle.
 * Consists of name and numbers assosiated. */
struct InterpretationToken
{
    std::string name;
    std::vector<double> parameters;
};

/*! Vertex used by render objects (derivations of RenderObject) */
struct RenderObjectVertex
{
    frVector4 position;
    frVector4 normal;

    double u, v;
};

/*! Represents an elementary object to be drawn by separate draw call.
 * Uses external shaders which must be setup by user given the data
 * supplied by this class. */
class RenderObject
{
public:
    virtual ~RenderObject() = default;

    /*!
     * Return relative position of object to be drawn.
     * @return Matrix which represents transform.
     */
    virtual frMatrix4x4 getTransform() = 0;
    /*!
     * Return texture used by this object.
     * @return Texture access unit.
     */
    virtual frTexture getTexture() = 0;
    /*!
     * Return bounding box which covers the object.
     * @return Bounding box.
     */
    virtual BoundingAABox getBoundingAABox() = 0;
    /*!
     * Return polygon count of the object.
     * @return Polygon count.
     */
    virtual unsigned getPolygonCount() = 0;

    /*!
     * Draw the object. All uniforms must be set up beforehand.
     * @param context frContext to be used.
     * @param vertexShader Vertex shader to be used.
     * @param fragmentShader Fragment shader to be used.
     * @param vertexUniform Vertex uniform to be used.
     * @param fragmentUniform Fragment uniform to be used.
     */
    virtual void draw(frContext* context,
                      frVsFunc vertexShader,
                      frFsFunc fragmentShader,
                      void* vertexUniform,
                      void* fragmentUniform) = 0;
protected:
    RenderObject() = default;
};

/*! Represent rendering data generated by a turtle
 * fed by a sequence of tokens. Rendering data is generated by
 * calling regenerate(). If successful, the list of render objects
 * representing the result can be obtained via getRenderObjects() */
class TreeRenderData
{
public:
    /*! Default constructor. No rendering data is created and the state
     * is invalid. */
    TreeRenderData();

    /*!
     * Attempt to generate rendering data using turtle using tokens as food.
     * Invalidates previous state.
     * Successfulness of the attempt can be queried later via isValid()
     * @param tokens A string of tokens (letters) to be fed to the turtle.
     * @param log String filled with information about rendering data
     * generation attempt.
     */
    void regenerate(std::vector<InterpretationToken>& tokens,
                    std::string& log);

    TreeRenderData(const TreeRenderData&) = delete;
    TreeRenderData& operator=(const TreeRenderData&) = delete;

    TreeRenderData(TreeRenderData&&) = default;
    TreeRenderData& operator=(TreeRenderData&&) = default;

    /*!
     * Return bounding box covering all of the render objects stored.
     * @return Pointer to a bounding box of rendering data.
     */
    BoundingAABox* getBoundingAABox();
    /*! Return polygon count of all render objects stored.
     * @return Polygon count of rendering data. */
    unsigned getPolygonCount();

    ~TreeRenderData();

    /*!
     * Return all of render objects stored.
     * @return An array of render objects.
     */
    std::vector<RenderObject*>& getRenderObjects();
    /*
     * Return the state of last regeneration attempt
     * (existence of rendering data).
     * @return True if any regeneration was successful.
     */
    bool isValid();
private:
    std::vector<RenderObject*> renderObjects;
    /*! Temporary object used in regenerate() made a member
     * for memory optimization purposes. */
    std::vector<implfg::TreeBranchingStructure*> structures;
    BoundingAABox box;
    unsigned polygonCount = 0;
    bool valid;
};

/*!
 * Encapsulates the notion of L-System. The object need to be supplied
 * with code and verified. Verification generates the internal representation
 * (which is independent of source code)
 * of an L-System. The internal representation can be used by calling
 * interpret() method or queried by using additional methods.
 */
class LSystem
{
public:
    /*! The number of maximum iterations allowed if not specified
     * in the source */
    static const int DEFAULT_MAXIMUM_ITERATIONS = 100;
    /*! The number of preferred iterations if not specified
     * in the source */
    static const int DEFAULT_PREFERRED_ITERATIONS = 1;

    LSystem();
    ~LSystem();

    LSystem(const LSystem&) = delete;
    LSystem& operator=(const LSystem&) = delete;

    LSystem(LSystem&&) = default;
    LSystem& operator=(LSystem&&) = default;

    /*!
     * Interpret L-System to generate a number of tokens to be passed to
     * the turtle. The L-System must be built beforehand.
     * For defails, see project report.
     * @param iterations Number of iterations to be executed.
     * @param seed The seed of the random number generator to be used.
     * @param log String which is filled with the information about
     * interpretation attempt. Cleared before interpretation.
     * @param success Successfulness of interpretation attempt.
     * @return An array of tokens which can be fed to turtle.
     */
    std::vector<InterpretationToken> interpret(unsigned iterations,
                                               unsigned seed,
                                               std::string& log,
                                               bool& success);

    /*!
     * Attempt to verify the system. Changes the internal
     * representation of this object if succeeded. Does not invalidate
     * the previous representation if failed. The successfulness status
     * can be later queried with getVerificationStatus().
     * @return The successfulness of verification attempt.
     */
    bool verify();

    /*!
     * Set the source of the L-system. The source can later be
     * used for verification (i. e. change of internal L-System
     * representation). The source string previously assosiated with
     * object, if any, is destroyed.
     * @param source The source to be stored.
     */
    void setSource(std::string source);
    /*! Returns the source string assosiated with object.
     * @return The source string stored. */
    const std::string& getSource();

    /*!
     * Return the status of last verification attempt.
     * @return Status of last verification attempt.
     * False if there were no attempts.
     */
    bool getVerificationStatus() const;
    /*! Return the log of last verification attempt.
     * @return String with the information about last verification attempt. */
    const std::string& getLog();

    /*!
     * Return the maximum number of iterations allowed by the L-System.
     * @return The maximum number of iterations.
     * DEFAULT_MAXIMUM_ITERATIONS if there is no internal representation.
     */
    int getMaximumIterations() const;
    /*!
     * Return the recommended number of iterations specifed by the L-System.
     * @return The preferred number of iterations.
     * DEFAULT_PREFERRED_ITERATIONS if there is no internal representation.
     */
    int getPreferredIterations() const;
private:
    std::string sourceString;

    bool buildStatus;
    std::string log;

    std::vector<implfg::Function*> functions;
    std::vector<implfg::Alias> aliases;
    std::vector<InterpretationToken> axiom;
    std::vector<implfg::InterpretationRule> rules;

    int maximumIterations, preferredIterations;
};

}

#endif
