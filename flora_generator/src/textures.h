#ifndef FLORA_WOOD_TEXTURE_H
#define FLORA_WOOD_TEXTURE_H

#include <flora_renderer.h>

/*!
 * Initialize textures returned by getWoodTexture()
 * and getLeafTexture() functions.
 */
void initializeWoodLeafTextures();

/*!
 * Return wood texture access unit.
 * initializeWoodLeafTextures() must be called beforehand.
 * Otherwise the behavior is undefined.
 * @return Texture access unit.
 */
frTexture getWoodTexture();
/*!
 * Return leaf texture access unit.
 * initializeWoodLeafTextures() must be called beforehand.
 * Otherwise the behavior is undefined.
 * @return Texture access unit.
 */
frTexture getLeafTexture();

#endif
