#include <flora_generator.h>

#include <utility>
#include <string>
#include <random>

#include "functions/randomize_function.h"

using namespace FG;
using namespace FG::implfg;

std::vector<InterpretationToken> LSystem::interpret(unsigned int iterations,
                                                    unsigned int seed,
                                                    ::std::string& log,
                                                    bool& success)
{
    log = {};

    success = false;

    std::mt19937 rd(seed);
    std::uniform_real_distribution<double> distr(0, 1);
    double probability;

    for(Function* function : functions)
        if(function->getName() == "Randomize")
        {
            ((RandomizeFunction*)function)->setSeed(seed);
            break;
        }

    std::vector<InterpretationToken> arr1 = axiom;
    std::vector<InterpretationToken> arr2;

    std::vector<InterpretationToken>* src = &arr1;
    std::vector<InterpretationToken>* dest = &arr2;

    size_t j;

    double tmp;

    for(unsigned int iter = 0; iter < iterations; iter++)
    {
        dest->clear();

        for(size_t i = 0; i < src->size(); i++)
        {
            probability = distr(rd);

            for(j = 0; j < rules.size(); j++)
            {
                if(rules[j].declaration.name == src->at(i).name)
                {
                    if(probability <= rules[j].probabilityRange)
                    {
                        break;
                    }
                }
            }

            if(j == rules.size())
            {
                dest->push_back(src->at(i));
                continue;
            }

            for(FunctionCall& call : rules[j].calls)
            {
                InterpretationToken token;

                token.name = call.functionName;

                for(Expression& expression : call.expressions)
                {
                    tmp = expression.yield(functions,
                                           rules[j].declaration.parameters,
                                           src->at(i).parameters,
                                           log);

                    if(tmp == NAN)
                    {
                        log += "Ошибка вычисления выражения.\n";
                        return { };
                    }

                    token.parameters.push_back(tmp);
                }

                dest->push_back(token);
            }
        }

        std::swap(src, dest);
    }

    /* aliases */

    dest->clear();

    for(size_t i = 0; i < src->size(); i++)
    {
        for(j = 0; j < aliases.size(); j++)
            if(aliases[j].declaration.name == src->at(i).name)
                break;

        if(j == aliases.size())
        {
            dest->push_back(src->at(i));
            continue;
        }

        for(FunctionCall& call : aliases[j].calls)
        {
            InterpretationToken token;

            token.name = call.functionName;

            for(Expression& expression : call.expressions)
            {
                tmp = expression.yield(functions,
                                       aliases[j].declaration.parameters,
                                       src->at(i).parameters,
                                       log);

                if(tmp == NAN)
                {
                    log += "Ошибка вычисления выражения.\n";
                    return { };
                }

                token.parameters.push_back(tmp);
            }

            dest->push_back(token);
        }
    }

    success = true;

    return *dest;
}
