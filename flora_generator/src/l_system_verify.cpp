#include <flora_generator.h>

#include <cctype>
#include <sstream>
#include <stack>
#include <algorithm>

#include "functions/expression_function.cpp"
#include "functions/add_builtin_functions.h"
#include "functions/randomize_function.h"

/*! \file
 * Parse text supplied by user and generate internal representation
 * of an L-System.
 * The input text is first divided in tokens (lexems).
 * Tokens are then grouped by statements (a sequence of tokens which had
 * a semicolon token after them).
 * Then all of statements are parsed by passing them to
 * the corresponding functions.
 */

using namespace FG;
using namespace FG::implfg;

/*! An array of operators allowed. */
const std::vector<std::string> OPERATORS = { "+", "-", "*", "/" };

/*!
 * Get precendence of operator op. The higher the returned value the higher
 * precendence the operator has.
 * @param op Operator to be reviewed.
 * @return The precendence of op. -1 if op is unknown.
 */
static int getPrecendence(const std::string& op)
{
    if(op == "+" || op == "-")
        return 1;

    if(op == "*" || op == "/")
        return 2;

    return -1;
}

/*! An array of symbols allowed to appear in identifiers. */
const char EXTRA_ALLOWED_SYMBOLS[] =
    { '[', ']', '~', '!', '@', '#', '$', '%', '^', '&',
       '_', '~', '\'', '\"', '\\', '{', '}', '?' };

/*! Count of EXTRA_ALLOWED_SYMBOLS. */
const size_t EXTRA_ALLOWED_SYMBOLS_SIZE =
    sizeof(EXTRA_ALLOWED_SYMBOLS) / sizeof(EXTRA_ALLOWED_SYMBOLS[0]);

/*! Type of ParsingToken */
enum ParsingTokenType
{
    TokenString,
    TokenBracket,
    TokenOperator,
    TokenNumber,
    TokenComma,
    TokenArrow,
    TokenSemicolon,
    TokenEOF,
    TokenInvalid
};

/* Token (lexem) parsed by parseToken() function. */
struct ParsingToken
{
    std::string str;
    ParsingTokenType type;
};

/* Represents a sequence of tokens thought to be a single statement. */
struct Statement
{
    std::vector<ParsingToken> tokens;
};

/*!
 * Return value indicating whenether token is a string token
 * with corresponding string.
 * @param token Token to be analyzed.
 * @param str A string used for comparison.
 * @return True if token is a string token with token.str equivalent to str.
 */
static bool isStringToken(const ParsingToken& token, std::string str)
{
    return token.type == TokenString && token.str == str;
}

/*!
 * Return value indicating whenether token is opening bracket or not.
 * @param token Token to be analyzed.
 * @return True if token represents opening bracket, false otherwise.
 */
static bool isOpeningBracket(const ParsingToken& token)
{
    return token.type == TokenBracket && token.str == "(";
}

/*!
 * Return value indicating whenether token is closing bracket or not.
 * @param token Token to be analyzed.
 * @return True if token represents closing bracket, false otherwise.
 */
static bool isClosingBracket(const ParsingToken& token)
{
    return token.type == TokenBracket && token.str == ")";
}

/*!
 * Return value indicating whenether token is comma or not.
 * @param token Token to be analyzed.
 * @return True if token represents comma, false otherwise.
 */
static bool isComma(const ParsingToken& token)
{
    return token.type == TokenComma;
}

/*!
 * Remove all the whitespace at the beginning of string.
 * @param src String to be modified.
 */
static void skipWhitespace(std::string& src)
{
    size_t i = 0;
    while(i < src.size() && std::isspace(src[i]))
        i++;

    src.erase(0, i);
}

/*!
 * Return value indicating whenether a char is allowed in identifier or not.
 * @param c char to be analyzed.
 * @param firstChar true if c is a first character in identifier.
 * @return True if a character is allowed, false otherwise.
 */
static bool isAllowedStringChar(char c, bool firstChar)
{
    if(std::isalpha(c) || (!firstChar && std::isdigit(c)))
        return true;

    for(size_t i = 0; i < EXTRA_ALLOWED_SYMBOLS_SIZE; i++)
        if(c == EXTRA_ALLOWED_SYMBOLS[i])
            return true;

    return false;
}

/*!
 * Parse a string token from input string.
 * @param src String to be analyzed.
 * @return String token.
 */
static std::string getParsingTokenString(std::string& src)
{
    size_t i = 0;
    while(i < src.size() && isAllowedStringChar(src[i], false))
        i++;

    std::string result = src.substr(0, i);
    src.erase(0, i);

    return result;
}

/*!
 * Parse next token from source string.
 * @param src Source string.
 * @param log Parsing log.
 * @return Token parsed. Token has TokenEOF type if source string is empty.
 * Token has TokenInvalid type if parsing failed.
 */
static ParsingToken parseToken(std::string& src, std::string& log)
{
    skipWhitespace(src);

    if(src.empty())
        return { "", TokenEOF };

    std::string tokenString;

    if(src[0] == ';')
    {
        src.erase(0, 1);
        return { "", TokenSemicolon };
    }

    if(src[0] == ',')
    {
        src.erase(0, 1);
        return { "", TokenComma };
    }

    if(src[0] == '(' || src[0] == ')')
    {
        tokenString = src.substr(0, 1);
        src.erase(0, 1);
        return { tokenString, TokenBracket };
    }

    if((tokenString = src.substr(0, 2)) == "->")
    {
        src.erase(0, 2);
        return { "", TokenArrow };
    }

    for(const std::string& oper : OPERATORS)
        if(oper == src.substr(0, oper.size()))
        {
            tokenString = src.substr(0, oper.size());
            src.erase(0, oper.size());

            return { tokenString, TokenOperator };
        }

    if(std::isdigit(src[0]) || src[0] == '-')
    {
        const int MAX_NUMBER_LENGTH = 16;
        std::istringstream strstream(src.substr(0, MAX_NUMBER_LENGTH));
        double data;
        strstream >> data;

        if(!strstream.fail())
        {
            tokenString = std::to_string(data);
            src.erase(0, strstream.tellg());
            return { tokenString, TokenNumber };
        }
    }

    if(isAllowedStringChar(src[0], true))
        return { getParsingTokenString(src), TokenString };
    
    log += std::string("Ошибка разбора строк на составляющие.\n"
                       "Возможно, присутствует неправильно "
                       "именованный идентификатор.\n");

    return { "", TokenInvalid };
}

/*!
 * Parse source and construct a sequence of statements.
 * @param src Source string.
 * @param statements An array of statements parsed.
 * @param log Parsing log.
 * @return True if no errors occured, false otherwise.
 */
static bool parseStatements(std::string& src,
                            std::vector<Statement>& statements,
                            std::string& log)
{
    ParsingToken token;

    std::vector<ParsingToken> tokens;

    while((token = parseToken(src, log)).type != TokenInvalid &&
          token.type != TokenEOF)
    {
        if(token.type == TokenSemicolon)
        {
            statements.push_back({ std::move(tokens) });
            tokens = std::vector<ParsingToken>();
        }
        else
            tokens.push_back(token);
    }

    if(token.type == TokenInvalid)
    {
        log += "[инструкция ";
        log += std::to_string(statements.size() + 1);
        log += ", токен ";
        log += std::to_string(tokens.size() + 1);
        log += "]\nВерификация не удалась!\n";

        return false;
    }

    if(!tokens.empty())
    {
        log += "Незаконечнная инструкция в конце файла.\n"
               "Возможно, пропущена ';'?\n";

        return false;
    }

    return true;
}

/*! Status returned by statement-parsing functions. */
enum class ParseStatus
{
    Success,
    SkipToNextSection,
    Abort
};

/*!
 * Verify that the next token in statement is arrow and remove it.
 * @param s Statement being analyzed.
 * @param log Parsing log.
 * @return Success on success, Abort if arrow was not found.
 */
static ParseStatus parseArrow(Statement& s, std::string& log)
{
    if(s.tokens.size() > 0 && s.tokens[0].type == TokenArrow)
    {
        s.tokens.erase(s.tokens.begin(), s.tokens.begin() + 1);
        return ParseStatus::Success;
    }
  
    log += "Пропущена стрела в инструкции.\n";

    return ParseStatus::Abort;
}

/*!
 * Parse declaration.
 * @param s Statement being analyzed.
 * @param log Parsing log.
 * @param declaration Declaration parsed. Undetermined if failed to parse.
 * @return Success on success, Abort if failed to parse.
 */
static ParseStatus parseDeclaration(Statement& s,
                                    std::string& log,
                                    Declaration& declaration)
{
    if(s.tokens.size() == 0)
    {
        log += "Не удаётся разобрать объявление: найден конец инструкции.\n";
        return ParseStatus::Abort;
    }

    ParsingToken& name = s.tokens[0];
    if(name.type != TokenString)
    {
        log += "Не удаётся разобрать объявление: отсутствует "
               "или неверное имя.\n";
        return ParseStatus::Abort;
    }

    if(s.tokens.size() > 1 && isOpeningBracket(s.tokens[1]))
    {
        bool foundClosingBracket = false;
        std::vector<std::string> parameters;

        size_t i = 2;
        bool expectComma = false;
        while(i < s.tokens.size() && !foundClosingBracket)
        {
            ParsingToken& currentToken = s.tokens[i];

            switch(currentToken.type)
            {
            case TokenString:
                if(!expectComma)
                {
                    parameters.push_back(currentToken.str);
                }
                else
                {
                    log += "Не удаётся разобрать объявление: "
                           "запятая без идентификатора?\n";
                    return ParseStatus::Abort;
                }

                break;
            case TokenComma:
                if(!expectComma)
                {
                    log += "Не удаётся разобрать объявление: лишняя запятая.\n";
                    return ParseStatus::Abort;
                }

                break;
            case TokenBracket:
                if(((parameters.empty() && !expectComma) ||
                       (!parameters.empty() && expectComma))
                   && isClosingBracket(currentToken))
                {
                    foundClosingBracket = true;
                    break;
                }
                else
                {
                    log += "Не удаётся разобрать объявление: неправильно "
                            "составленный список.\n";
                    return ParseStatus::Abort;
                }
            default:
                log += "Не удаётся разобрать объявление: неправильно "
                       "составленный список.\n";
                return ParseStatus::Abort;
            }

            expectComma = !expectComma;

            i++;
        }

        if(!foundClosingBracket)
        {
            log += "Не удаётся разобрать объявление: "
                   "незаконченное объявление.\n";
            return ParseStatus::Abort;
        }

        declaration.name = name.str;
        declaration.parameters = std::move(parameters);

        s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + i);

        return ParseStatus::Success;
    }

    declaration.name = name.str;
    declaration.parameters = {};
    s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);

    return ParseStatus::Success;
}

static ParseStatus parseExpression(Statement& s,
                                   std::string& log,
                                   Expression& e);

/*!
 * Parse function call.
 * @param s Statement being analyzed.
 * @param log Parsing log.
 * @param c FunctionCall parsed. Undetermined if failed to parse.
 * @return Success on success, Abort if failed to parse.
 */
static ParseStatus parseFunctionCall(Statement& s,
                                     std::string& log,
                                     FunctionCall& c)
{
    if(s.tokens.size() == 0)
    {
        log += "Не удаётся разобрать подстановку/вызов функции: "
               "найден конец инструкции.\n";
        return ParseStatus::Abort;
    }

    ParsingToken& name = s.tokens[0];
    if(name.type != TokenString)
    {
        log += "Не удаётся разобрать подстановку/вызов функции: "
               "отсутствует или неверное имя.\n";
        return ParseStatus::Abort;
    }

    c.functionName = std::move(s.tokens[0].str);
    c.expressions = { };

    s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);

    if(!s.tokens.empty() && isOpeningBracket(s.tokens[0]))
    {
        s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);

        bool foundEnd = false;
        bool expectComma = false;
        while(!s.tokens.empty() && !foundEnd)
        {
            if(isClosingBracket(s.tokens[0]))
            {
                if(!c.expressions.empty() && !expectComma)
                {
                    log += "Не удаётся разобрать подстановку/вызов функции: "
                           "закрывающая скобка после запятой без выражения.\n"
                           "Лишняя запятая?\n";
                    return ParseStatus::Abort;
                }

                s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);
                foundEnd = true;
            }
            else if(isComma(s.tokens[0]))
            {
                if(!expectComma)
                {
                    log += "Не удаётся разобрать подстановку/вызов функции: "
                           "отсутствует выражение между запятыми. "
                           "Лишняя запятая?\n";
                    return ParseStatus::Abort;
                }

                s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);
                expectComma = false;
            }
            else
            {
                if(expectComma)
                {
                    log += "Не удаётся разобрать подстановку/вызов функции: "
                           "отсутствует запятая между выражениями / "
                           "закрывающая скобка.\n";
                    return ParseStatus::Abort;
                }

                expectComma = true;

                Expression expression;

                if(parseExpression(s, log, expression) != ParseStatus::Success)
                {
                    log += "Не удаётся разобрать подстановку/вызов функции: "
                           "ошибка разбора выражения.\n";
                    return ParseStatus::Abort;
                }

                c.expressions.push_back(expression);
            }
        }

        if(!foundEnd)
        {
            log += "Не удаётся разобрать подстановку/вызов функции: "
                   "отсутствует закрывающая скобка.\n";
            return ParseStatus::Abort;
        }

        return ParseStatus::Success;
    }

    return ParseStatus::Success;
}

/*!
 * Parse expression.
 * @param s Statement being analyzed.
 * @param log Parsing log.
 * @param e Expression parsed. Undetermined if failed to parse.
 * @return Success on success, Abort if failed to parse.
 */
static ParseStatus parseExpression(Statement& s,
                                   std::string& log,
                                   Expression& e)
{
    if(s.tokens.empty())
    {
        log += "Не удаётся разобрать выражение: выражение отсутствует.\n";
        return ParseStatus::Abort;
    }

    int bracketLevel = 0;
    bool skipErasing;

    bool foundExpressionEnd = false, expectData = true;

    std::stack<ExpressionPart> stack;

    while(!s.tokens.empty() && !foundExpressionEnd)
    {
        ParsingToken token = s.tokens[0];
        skipErasing = false;

        switch(token.type)
        {
        case TokenString:
            if(!expectData)
            {
                log += "Не удалось разобрать выражение: "
                       "данные без оператора.\n";
                return ParseStatus::Abort;
            }

            expectData = false;

            if(s.tokens.size() > 1 && isOpeningBracket(s.tokens[1]))
            {
                FunctionCall call;
                if(parseFunctionCall(s, log, call) != ParseStatus::Success)
                {
                    log += "Не удалось разобрать вызов функции/подстановки.\n";
                    return ParseStatus::Abort;
                }

                skipErasing = true;
                e.parts.emplace_back(std::move(call));
            }
            else
            {
                e.parts.emplace_back(std::move(token.str),
                                ExpressionPartDataType::Identifier);
            }
            break;
        case TokenNumber:
            if(!expectData)
            {
                log += "Не удалось разобрать выражение: число без оператора.\n";
                return ParseStatus::Abort;
            }

            expectData = false;

            e.parts.emplace_back(stod(token.str, nullptr));
            break;
        case TokenOperator:
            if(expectData)
            {
                log += "Не удалось разобрать выражение: "
                        "оператор после оператора.\n";
                return ParseStatus::Abort;
            }

            expectData = true;

            while(!stack.empty() &&
                  stack.top().data.string != "(" &&
                  getPrecendence(stack.top().data.string) >=
                      getPrecendence(token.str))
            {
                e.parts.push_back(stack.top());
                stack.pop();
            }

            stack.emplace(std::move(token.str),
                          ExpressionPartDataType::Operator);

            break;
        case TokenBracket:
            if(isOpeningBracket(token))
            {
                if(!e.parts.empty() && !expectData)
                {
                    log += "Не удалось разобрать выражение: "
                           "открывающая скобка без оператора.\n";
                    return ParseStatus::Abort;
                }

                stack.push(ExpressionPart(std::move(token.str),
                           ExpressionPartDataType::Operator));

                bracketLevel++;

                expectData = true;
            }
            else
            {
                if(expectData)
                {
                    log += "Не удалось разобрать выражение: "
                           "закрывающая скобка после "
                           "оператора/открывающей скобки.\n";
                    return ParseStatus::Abort;
                }

                expectData = false;

                while(!stack.empty() && stack.top().data.string != "(")
                {
                    e.parts.push_back(stack.top());
                    stack.pop();
                }

                if(stack.empty())
                {
                    foundExpressionEnd = true;
                    break;
                }

                bracketLevel--; 

                stack.pop();
            }

            break;
        case TokenComma:
            foundExpressionEnd = true;
            break;
        default:
            log += "Не удаётся разобрать выражение: неизвестный токен.\n";
            return ParseStatus::Abort;
        }

        if(!foundExpressionEnd && !skipErasing)
            s.tokens.erase(s.tokens.begin(), s.tokens.begin() + 1);
    }

    if(bracketLevel != 0)
    {
        log += "Не удаётся разобрать выражение: "
               "несоответствие открывающих/закрывающих скобок.\n";
        return ParseStatus::Abort;
    }

    if(expectData)
    {
        log += "Не удаётся разобрать выражение: заканчивается оператором.\n";
        return ParseStatus::Abort;
    }

    while(!stack.empty())
    {
        e.parts.push_back(stack.top());
        stack.pop();
    }

    return ParseStatus::Success;
}

/*!
 * Search for identifiers in expression which do not appear in declaration.
 * Such identifiers are declared to be unknown and make expression unresolvable.
 * @param declaration Declaration with parameters to be analyzed.
 * @param expression Expression to be analyzed.
 * @return First unknown identifer found, "" if search failed.
 */
static std::string findUnknownIdentifiers(Declaration& declaration,
                                          Expression& expression)
{
    std::string unknown;

    for(ExpressionPart& part : expression.parts)
    {
        if(part.type == ExpressionPartDataType::Identifier)
        {
            auto result = std::find(declaration.parameters.begin(), 
                                    declaration.parameters.end(),
                                    part.data.string);

            if(result == declaration.parameters.end())
            {
                return part.data.string;
            }
        }
        else if(part.type == ExpressionPartDataType::FunctionCall)
        {
            for(Expression& expr : part.data.call.expressions)
            {
                unknown = findUnknownIdentifiers(declaration, expr);

                if(unknown != "")
                    return unknown;
            }
        }
    }

    return "";
}

/*!
 * Parse statement which is thought to be located in preamble.
 * If statement is an axiom then parse it and indicate the end of
 * the preamble.
 * See project report for details.
 * @param s Statement to be analyzed.
 * @param log Parsing log.
 * @param functions Reference to functions array to be filled.
 * @param aliases Reference to aliases to be filled.
 * @param axiom Reference to axiom data to be filled.
 * @param maximumIterations Place to store maximumIterations.
 * @param preferredIterations Place to store preferredIterations.
 * @return Success if succeeded in parsing statement which is not an axiom,
 * SkipToNextSection if succeeded in parsing axiom,
 * Abort otherwise.
 */
static ParseStatus
    parsePreambleStatement(Statement& s,
                           std::string& log,
                           std::vector<Function*>& functions,
                           std::vector<Alias>& aliases,
                           std::vector<InterpretationToken>& axiom,
                           int& maximumIterations,
                           int& preferredIterations)
{
    if(s.tokens.size() == 0)
    {
        log += "Предупреждение: найдена пустая инструкция. Лишняя ';'?\n";
        return ParseStatus::Success;
    }
    
    ParsingToken token = std::move(s.tokens[0]);
    s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);

    double tmp;

    if(isStringToken(token, "func"))
    {
        Declaration declaration;
        Expression expression;

        if(parseDeclaration(s, log, declaration)
               != ParseStatus::Success ||
           parseArrow(s, log) != ParseStatus::Success ||
           parseExpression(s, log, expression) != ParseStatus::Success)
        {
            log += "Не удалось разобрать функцию.\n";
            return ParseStatus::Abort;
        }

        std::string unknown;
        if((unknown = findUnknownIdentifiers(declaration, expression)) != "")
        {
            log += "Не удалось разобрать функцию. '";
                    log += unknown;
                    log += "' не найдено.\n";
            return ParseStatus::Abort;
        }

        ExpressionFunction* function;
        try
        {
            function = new ExpressionFunction(declaration.name,
                                              expression,
                                              declaration.parameters);
        }
        catch(...)
        {
            log += "Не удалось разобрать функцию. Исключение при создании.";
                    return ParseStatus::Abort;
        }

        functions.push_back(function);
    }
    else if(isStringToken(token, "alias"))
    {
        Declaration declaration;

        if(parseDeclaration(s, log, declaration)
               != ParseStatus::Success ||
           parseArrow(s, log) != ParseStatus::Success)
        {
            log += "Не удалось разобрать подмену.\n";
            return ParseStatus::Abort;
        }

        Alias alias;
        alias.declaration = declaration;

        while(!s.tokens.empty())
        {
            FunctionCall call;

            if(parseFunctionCall(s, log, call) != ParseStatus::Success)
            {
                log += "Не удалось разобрать подмену. "
                       "Ошибка разбора подстановки.\n";
                return ParseStatus::Abort;
            }

            for(Expression& expression : call.expressions)
            {
                std::string unknown;
                if((unknown = findUnknownIdentifiers(declaration, expression))
                    != "")
                {
                    log += "Не удалось разобрать подмену. '";
                    log += unknown;
                    log += "' не найдено.\n";
                    return ParseStatus::Abort;
                }
            }

            alias.calls.push_back(call);
        }

        aliases.push_back(alias);
    }
    else if(isStringToken(token, "axiom"))
    {
        Declaration declaration;

        while(!s.tokens.empty())
        {
            InterpretationToken interToken;
            FunctionCall call;

            if(parseFunctionCall(s, log, call) != ParseStatus::Success)
            {
                log += "Не удалось разобрать аксиому. "
                       "Ошибка разбора подстановки.\n";
                return ParseStatus::Abort;
            }

            interToken.name = call.functionName;

            for(Expression& expression : call.expressions)
            {
                std::string unknown;
                if((unknown = findUnknownIdentifiers(declaration, expression))
                    != "")
                {
                    log += "Не удалось разобрать аксиому. Требуется '";
                    log += unknown;
                    log += "', но выражения в аксиоме не могут "
                           "использовать параметры.\n";
                    return ParseStatus::Abort;
                }

                tmp = expression.yield(functions, {}, {}, log);

                if(tmp == NAN)
                {
                    log += "Не удалось разобрать аксиому. "
                           "Ошибка вычисления выражения.\n";
                    return ParseStatus::Abort;
                }

                interToken.parameters.push_back(tmp);
            }

            axiom.push_back(interToken);
        }

        return ParseStatus::SkipToNextSection;
    }
    else if(isStringToken(token, "maximumIterations"))
    {
        if(s.tokens.size() == 1 && s.tokens[0].type == TokenNumber)
        {
            maximumIterations = stoi(s.tokens[0].str, nullptr);

            if(maximumIterations < 0)
            {
                log += "Неправильное значение maximumIterations\n";
                return ParseStatus::Abort;
            }
        }
        else
        {
            log += "Некорректнно записанная инструкция maximumIterations\n";
            return ParseStatus::Abort;
        }

        s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);
    }
    else if(isStringToken(token, "preferredIterations"))
    {
        if(s.tokens.size() == 1 && s.tokens[0].type == TokenNumber)
        {
            preferredIterations = stoi(s.tokens[0].str, nullptr);

            if(preferredIterations < 0)
            {
                log += "Неправильное значение preferredIterations\n";
                return ParseStatus::Abort;
            }
        }
        else
        {
            log += "Некорректнно записанная инструкция preferredIterations\n";
            return ParseStatus::Abort;
        }

        s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 1);
    }
    else
    {
        log += "Неизвестная инструкция в преамбуле.\n";
        return ParseStatus::Abort;
    }

    return ParseStatus::Success;
}

/*!
 * Parse a list of special parameters which can be assosiated
 * with a specific rule.
 * See project report for details.
 * @param s Statement to be analyzed.
 * @param log Parsing log.
 * @param rule Rule to be modified.
 * @return Success on success, Abort otherwise.
 */
static ParseStatus parseAdditionalRuleParameters(Statement& s,
                                                 std::string& log,
                                                 InterpretationRule& rule)
{
    while(!s.tokens.empty() && s.tokens[0].type != TokenArrow)
    {
        if(s.tokens.size() > 1 && isStringToken(s.tokens[0], "!probi"))
        {
            if(s.tokens[1].type == TokenNumber)
            {
                rule.probabilityRange = stod(s.tokens[1].str, nullptr);

                s.tokens.erase(s.tokens.begin() + 0, s.tokens.begin() + 2);
            }
            else
            {
                log += "Не удалось разобрать !probi: "
                   "параметр должен быть числом.\n";
                return ParseStatus::Abort;
            }
        }
        else
        {
            log += "Не удалось разобрать параметры правила вывода: "
                   "неизвестный или неправильно заданный параметр.\n";
            return ParseStatus::Abort;
        }
    }

    return ParseStatus::Success;
}

/*!
 * Parse a statement which is thought to be a rule.
 * @param s Statement to analyze.
 * @param log Parsing log.
 * @param rules Rules array to be expanded by a new rule.
 * @return Success on success, Abort otherwise.
 */
static ParseStatus parseRule(Statement& s,
                             std::string& log,
                             std::vector<InterpretationRule>& rules)
{
    InterpretationRule rule;
    Declaration declaration;

    if(parseDeclaration(s, log, declaration) != ParseStatus::Success)
    {
        log += "Не удалось разобрать правило вывода.\n";
        return ParseStatus::Abort;
    }

    rule.probabilityRange = 1;

    if(parseAdditionalRuleParameters(s, log, rule) != ParseStatus::Success)
    {
        log += "Не удалось разобрать параметры правила вывода.\n";
        return ParseStatus::Abort;
    }

    if(parseArrow(s, log) != ParseStatus::Success)
    {
        log += "Не удалось разобрать правило вывода: отсутствует стелка.\n";
        return ParseStatus::Abort;
    }

    while(!s.tokens.empty())
    {
        FunctionCall call;

        if(parseFunctionCall(s, log, call) != ParseStatus::Success)
        {
            log += "Не удалось разобрать правило вывода. "
                   "Ошибка разбора подстановки.\n";
            return ParseStatus::Abort;
        }

        for(Expression& expression : call.expressions)
        {
            std::string unknown;
            if((unknown = findUnknownIdentifiers(declaration, expression))
                != "")
            {
                log += "Не удалось разобрать правило вывода. '";
                        log += unknown;
                        log += "' не найдено.\n";
                return ParseStatus::Abort;
            }
        }

        rule.calls.push_back(call);
    }

    rule.declaration = declaration;

    rules.push_back(rule);

    return ParseStatus::Success;
}

bool LSystem::verify()
{
    std::string src = sourceString;

    log.clear();

    buildStatus = false;

    std::vector<Function*> functions;
    std::vector<Alias> aliases;
    std::vector<InterpretationToken> axiom;
    std::vector<InterpretationRule> rules;

    int maximumIterations = DEFAULT_MAXIMUM_ITERATIONS,
        preferredIterations = DEFAULT_PREFERRED_ITERATIONS;

    addBuiltinFunctions(functions);

    for(Function* function : functions)
        if(function->getName() == "Randomize")
        {
            ((RandomizeFunction*)function)->setSeed(0);
            break;
        }

    std::vector<Statement> statements;
    if(!parseStatements(src, statements, log))
        return false;

    ParseStatus status;
    bool rulesSection = false;

    for(size_t i = 0; i < statements.size(); i++)
    {
        if(!rulesSection)
        {
            status = parsePreambleStatement(statements[i],
                                            log,
                                            functions,
                                            aliases,
                                            axiom,
                                            maximumIterations,
                                            preferredIterations);

            if(status == ParseStatus::SkipToNextSection)
                rulesSection = true;
        }
        else
            status = parseRule(statements[i], log, rules);

        if(status == ParseStatus::Abort)
        {
            log += "[инструкция ";
            log += std::to_string(i + 1);
            log += "]\nВерификация не удалась!\n";

            for(Function* function : functions)
                if(!function->isBuiltIn())
                    delete function;

            return false;
        }
    }

    if(!rulesSection)
    {
        log += "Отсутствует аксиома.\n";
        return false;
    }

    if(preferredIterations > maximumIterations)
    {
        log += "preferredIterations превышает maximumIterations.\n";
        return false;
    }

    std::vector<std::string> ruleNames;

    for(InterpretationRule& rule : rules)
        ruleNames.push_back(rule.declaration.name);

    std::sort(ruleNames.begin(), ruleNames.end());

    auto last = std::unique(ruleNames.begin(), ruleNames.end());
    ruleNames.erase(last, ruleNames.end());

    int numberOfParameters;
    double totalProbability, probabilitySoFar;
    for(std::string& currentRuleName : ruleNames)
    {
        numberOfParameters = -1;
        totalProbability = -1;
        probabilitySoFar = 0;

        for(InterpretationRule& rule : rules)
        {
            if(rule.declaration.name != currentRuleName)
                continue;

            if(numberOfParameters == -1)
            {
                numberOfParameters = rule.declaration.parameters.size();
            }
            else if(numberOfParameters !=
                    (int)rule.declaration.parameters.size())
            {
                log += "Несоответствие кол-ва аргументов для правил вывода для "
                       "буквы '";
                log += rule.declaration.name;
                log += "'.\n";
                return false;
            }

            if(totalProbability == -1)
                totalProbability = rule.probabilityRange;
            else
                totalProbability += rule.probabilityRange;
        }

        for(InterpretationRule& rule : rules)
        {
            if(rule.declaration.name != currentRuleName)
                continue;

            probabilitySoFar += rule.probabilityRange;

            if(totalProbability != -1)
                rule.probabilityRange = probabilitySoFar / totalProbability;
        }
    }

    if(rules.empty())
    {
        log += "Отсутствует хотя бы одно правило вывода.\n";
        return false;
    }

    for(Function* function : this->functions)
        if(!function->isBuiltIn())
            delete function;

    this->functions = functions;
    this->aliases = aliases;
    this->axiom = axiom;
    this->rules = rules;

    this->maximumIterations = maximumIterations;
    this->preferredIterations = preferredIterations;

    buildStatus = true;
    log += "Верификация успешно завершена.";

    return true;
}
