#include <flora_generator.h>

#include <stack>
#include <flora_renderer.h>

#include "render_objects/branch_render_object.h"
#include "render_objects/leaf_render_object.h"

using namespace FG;
using namespace FG::implfg;

/*!
 * A convenience function.
 * Clear all the data supplied.
 * @param structures An array of structures.
 * @param renderObjects An array of render objects.
 */
static void clearEverything(std::vector<TreeBranchingStructure*>& structures,
                            std::vector<RenderObject*>& renderObjects)
{
    for(TreeBranchingStructure* structure : structures)
        delete structure;

    for(RenderObject* obj : renderObjects)
        delete obj;

    structures.clear();
    renderObjects.clear();
}

void TreeRenderData::regenerate(std::vector<InterpretationToken> &tokens,
                                std::string& log)
{
    valid = false;

    polygonCount = 0;

    clearEverything(structures, renderObjects);

    std::stack<frMatrix4x4> matrices;
    std::stack<TreeBranchingStructure*> stackStr;

    matrices.push(frMatrixIdentity());
    stackStr.push(nullptr);

    TreeBranchingStructure* root = nullptr;
    TreeBranchingStructure* current = nullptr;

    try
    {
        for(InterpretationToken& token : tokens)
        {
            if(token.name == "BranchForward")
            {
                if(token.parameters.size() != 3)
                {
                    log += "Неверное кол-во команд функции BranchForward (";
                    log += std::to_string(token.parameters.size());
                    log += ", должно быть 3)\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                if(token.parameters[0] <= 0 ||
                   token.parameters[1] <= 0 ||
                   token.parameters[2] <= 0)
                {
                    log += "Неверные параметры функции BranchForward\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                if(!current)
                {
                    current = new TreeBranchingStructure{ token.parameters[0],
                                                          token.parameters[1],
                                                          token.parameters[2],
                                                          matrices.top(),
                                                          nullptr,
                                                          {} };

                    root = current;

                    structures.push_back(current);

                    stackStr.top() = root;
                }
                else
                {
                    current = new TreeBranchingStructure{ token.parameters[0],
                                                          token.parameters[1],
                                                          token.parameters[2],
                                                          matrices.top(),
                                                          current,
                                                          {} };

                    structures.push_back(current);

                    current->parent->children.push_back(current);
                }

                matrices.top() =
                    frMatrixMultiply(
                        matrices.top(),
                        frMatrixCreateTranslation(0,
                                                  token.parameters[2],
                                                  0));
            }
            else if(token.name == "PushState")
            {
                if(token.parameters.size() != 0)
                {
                    log += "Неверное кол-во команд функции PushState (";
                    log += token.parameters.size();
                    log += ", должно быть 0)\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                matrices.push(matrices.top());
                stackStr.push(current);
            }
            else if(token.name == "PopState")
            {
                if(token.parameters.size() != 0)
                {
                    log += "Неверное кол-во команд функции PopState (";
                    log += token.parameters.size();
                    log += ", должно быть 0)\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                if(matrices.size() > 1)
                    matrices.pop();
                else
                {
                    log += "Кол-во команд PopState превысило PushState. "
                            "Несоответствие стека.\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                stackStr.pop();
                if(stackStr.top() == nullptr)
                    stackStr.top() = root;
            }
            else if(token.name == "Rotate")
            {
                if(token.parameters.size() != 3)
                {
                    log += "Неверное кол-во команд функции Rotate (";
                    log += std::to_string(token.parameters.size());
                    log += ", должно быть 3)\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                matrices.top() =
                    frMatricesMultiply(
                        (frMatrix4x4)matrices.top(),
                        frMatrixCreateRotationX(
                            FR_TO_RADIANS(token.parameters[0])),
                        frMatrixCreateRotationY(
                            FR_TO_RADIANS(token.parameters[1])),
                        frMatrixCreateRotationZ(
                            FR_TO_RADIANS(token.parameters[2])));
            }
            else if(token.name == "PlaceLeaf")
            {
                if(token.parameters.size() != 3)
                {
                    log += "Неверное кол-во команд функции PlaceLeaf (";
                    log += std::to_string(token.parameters.size());
                    log += ", должно быть 3)\n";
                    clearEverything(structures, renderObjects);
                    return;
                }

                renderObjects.push_back(
                    new LeafRenderObject(
                        frMatrixMultiply((frMatrix4x4)matrices.top(),
                                         frMatrixCreateTranslation(
                                            token.parameters[0],
                                            token.parameters[1],
                                            token.parameters[2]))));
            }
        }
    }
    catch(...)
    {
        clearEverything(structures, renderObjects);
        log += "Недостаточно памяти.\n";
    }

    if(!root)
    {
        log += "Не создано ни одной ветви.\n";
        return;
    }

    std::stack<int> i;

    i.push(0);
    current = root;

    RenderObject* obj;

    std::mt19937 randomEngine;

    try
    {
        obj = new BranchRenderObject(randomEngine,
                                     root->transform,
                                     root->widthStart,
                                     root->widthEnd,
                                     root->height);

        renderObjects.push_back(obj);
    }
    catch(...)
    {
        log += "Недостаточно памяти.\n";
        clearEverything(structures, renderObjects);
        return;
    }

    while(!i.empty())
    {
        if(i.top() < (int)current->children.size())
        {
            current = current->children[i.top()];
            i.push(0);
        }
        else
        {
            current = current->parent;

            i.pop();

            if(!i.empty())
                i.top()++;

            continue;
        }

        try
        {
            obj = new BranchRenderObject(randomEngine,
                                         current->transform,
                                         current->widthStart,
                                         current->widthEnd,
                                         current->height);

            renderObjects.push_back(obj);
        }
        catch(...)
        {
            log += "Недостаточно памяти.\n";
            clearEverything(structures, renderObjects);
            return;
        }
    }

    for(TreeBranchingStructure* structure : structures)
        delete structure;

    structures.clear();
 
    box = renderObjects[0]->getBoundingAABox();

    for(RenderObject* object : renderObjects)
    {
        box.expandBy(object->getBoundingAABox());
        polygonCount += object->getPolygonCount();
    }

    valid = true;
}

TreeRenderData::TreeRenderData()
    : renderObjects(), valid(false)
{ }

TreeRenderData::~TreeRenderData()
{
    for(RenderObject* obj : renderObjects)
        delete obj;
}

BoundingAABox* TreeRenderData::getBoundingAABox()
{
    if(valid)
        return &box;
    else
        return nullptr;
}

unsigned TreeRenderData::getPolygonCount()
{
    return polygonCount;
}

std::vector<RenderObject*>& TreeRenderData::getRenderObjects()
{
    return renderObjects;
}

bool TreeRenderData::isValid()
{
    return valid;
}
