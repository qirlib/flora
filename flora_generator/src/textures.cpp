#include "textures.h"

#include <random>

static bool initialized = false;

static frBuffer2d woodTextureBuffer;
static frTexture woodTexture = frGetInvalidTexture();

static frBuffer2d leafTextureBuffer;
static frTexture leafTexture = frGetInvalidTexture();

static const size_t WOOD_TEXTURE_SIZE = 128;
static const size_t LEAF_TEXTURE_SIZE = 16;

static const frColor WOOD_RGB_0 = frRgb(150, 60, 0);
static const frColor WOOD_RGB_1 = frRgb(102, 66, 42);
static const frColor WOOD_RGB_2 = frRgb(69, 35, 12);

static const frColor LEAF_RGB_0 = frRgb(81, 122, 2);
static const frColor LEAF_RGB_1 = frRgb(160, 200, 86);
static const frColor LEAF_RGB_2 = frRgb(139, 177, 56);

/*! Deinitialize textures. */
static void deinitializeTextures()
{
    frBuffer2dDestroy(&woodTextureBuffer);
    frBuffer2dDestroy(&leafTextureBuffer);
}

/*!
 * Initialize texture by allocating buffer and generating
 * contents using random number generator. Contents are generated
 * by interpolating three colors.
 * @param texture
 * @param randomDevice randomDevice to be used.
 * @param rgb0 First color to be used in interpolation.
 * @param rgb1 Second color to be used in interpolation.
 * @param rgb2 Third color to be used in interpolation.
 */
static void initializeTexture(frBuffer2d& texture,
                              std::mt19937& randomDevice,
                              frColor rgb0,
                              frColor rgb1,
                              frColor rgb2)
{
    std::uniform_real_distribution<double> distr
        = std::uniform_real_distribution<double>(0, 1);

    double i0, i1, i2;
    for(size_t i = 0; i < texture.width * texture.height; i++)
    {
        i0 = distr(randomDevice);
        i1 = distr(randomDevice);
        i2 = distr(randomDevice);

        double ratio = 1 / (i0 + i1 + i2);

        i0 *= ratio; i1 *= ratio; i2 *= ratio;

        FR_BUF_LINEAR_EL(texture, i, frColor) =
            FR_MIX3_COLORS(rgb0, rgb1, rgb2);
    }
}

void initializeWoodLeafTextures()
{
    if(initialized)
        return;

    atexit(deinitializeTextures);

    std::mt19937 randomDevice(123);

    frBuffer2dCreate(&woodTextureBuffer,
                     WOOD_TEXTURE_SIZE, WOOD_TEXTURE_SIZE, 0,
                     sizeof(frColor));
    initializeTexture(woodTextureBuffer, randomDevice, 
                      WOOD_RGB_0, WOOD_RGB_1, WOOD_RGB_2);

    frBuffer2dCreate(&leafTextureBuffer,
                     LEAF_TEXTURE_SIZE, LEAF_TEXTURE_SIZE, 0,
                     sizeof(frColor));
    initializeTexture(leafTextureBuffer, randomDevice, 
                      LEAF_RGB_0, LEAF_RGB_1, LEAF_RGB_2);

    woodTexture = frBufferToTexture(&woodTextureBuffer);
    leafTexture = frBufferToTexture(&leafTextureBuffer);

    initialized = true;
}

frTexture getWoodTexture()
{
    return woodTexture;
}

frTexture getLeafTexture()
{
    return leafTexture;
}
