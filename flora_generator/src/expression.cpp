#include <flora_generator.h>

#include <stack>
#include <algorithm>
#include <utility>

using namespace FG::implfg;

ExpressionPart::ExpressionPart(std::string id, ExpressionPartDataType type)
    : type(type)
{
    new(&data.string) std::string(std::move(id));
}

ExpressionPart::ExpressionPart(double number)
    : type(ExpressionPartDataType::Number)
{
    data.number = number;
}

ExpressionPart::ExpressionPart(FunctionCall call)
    : type(ExpressionPartDataType::FunctionCall)
{
    new(&data.call) FunctionCall(std::move(call));
}

ExpressionPart::~ExpressionPart()
{
    if(type == ExpressionPartDataType::Identifier ||
       type == ExpressionPartDataType::Operator)
    {
        (&data.string)->std::string::~basic_string();
    }
    else if(type == ExpressionPartDataType::FunctionCall)
    {
        (&data.call)->FunctionCall::~FunctionCall();
    }
}

ExpressionPart::Data::Data()
{ }

ExpressionPart::Data::~Data()
{ }

ExpressionPart::ExpressionPart(const ExpressionPart& other)
{
    switch(other.type)
    {
    case ExpressionPartDataType::Identifier:
    case ExpressionPartDataType::Operator:
        new(&data.string) std::string(other.data.string);
        break;
    case ExpressionPartDataType::Number:
        data.number = other.data.number;
        break;
    case ExpressionPartDataType::FunctionCall:
        new(&data.call) FunctionCall(other.data.call);
        break;
    }

    type = other.type;
}

ExpressionPart& ExpressionPart::operator=(const ExpressionPart& other)
{
    if(this != &other)
    {
        switch(other.type)
        {
        case ExpressionPartDataType::Identifier:
        case ExpressionPartDataType::Operator:
            new(&data.string) std::string(other.data.string);
            break;
        case ExpressionPartDataType::Number:
            data.number = other.data.number;
            break;
        case ExpressionPartDataType::FunctionCall:
            new(&data.call) FunctionCall(other.data.call);
            break;
        }

        type = other.type;
    }

    return *this;
}

double Expression::yield(std::vector<Function*>& functions,
                         const std::vector<std::string>& identifiers,
                         const std::vector<double>& arguments,
                         std::string& log)
{
    if(identifiers.size() != arguments.size())
        return NAN;

    std::stack<double> stack;

    size_t i;
    double tmp;

    std::vector<double> funcArgs;

    for(ExpressionPart& part : parts)
    {
        switch(part.type)
        {
        case ExpressionPartDataType::Identifier:
            for(i = 0; i < identifiers.size(); i++)
                if(identifiers[i] == part.data.string)
                {
                    stack.push(arguments[i]);
                    break;
                }

            if(i == identifiers.size())
            {
                log += "Ошибка вычисления выражения. "
                       "Неизвестный идентификатор.\n";
                return NAN;
            }

            break;
        case ExpressionPartDataType::Number:
            stack.push(part.data.number);
            break;
        case ExpressionPartDataType::Operator:
            tmp = stack.top();
            stack.pop();

            if(part.data.string == "+")
                tmp = stack.top() + tmp;
            else if(part.data.string == "-")
                tmp = stack.top() - tmp;
            else if(part.data.string == "*")
                tmp = stack.top() * tmp;
            else if(part.data.string == "/")
            {
                if(tmp == 0)
                {
                    log += "Ошибка вычисления выражения. Деление на ноль.\n";
                    return NAN;
                }

                tmp = stack.top() / tmp;
            }
            else
            {
                log += "Ошибка вычисления выражения. Неизвестный оператор.\n";
                return NAN;
            }

            stack.push(tmp);
            break;
        case ExpressionPartDataType::FunctionCall:
            for(i = 0; i < functions.size(); i++)
                if(functions[i]->getName() == part.data.string)
                {
                    if(part.data.call.expressions.size() !=
                       functions[i]->getParametersCount())
                    {
                        log += "Ошибка вычисления выражения. "
                                "Несовпадающее кол-во параметров/аргументов.\n";
                        return NAN;
                    }

                    funcArgs = {};

                    for(Expression& expression : part.data.call.expressions)
                    {
                        funcArgs.push_back(expression.yield(functions,
                                                            identifiers,
                                                            arguments,
                                                            log));
                    }

                    stack.push(functions[i]->yield(functions, funcArgs, log));
                    break;
                }

            if(i == functions.size())
            {
                log += "Ошибка вычисления выражения. Неизвестная функция.\n";
                return NAN;
            }

            break;
        }
    }

    return stack.top();
}
