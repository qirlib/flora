#ifndef FLORAINC_FLORA_GENERATOR_SRC_FUNCTIONS_EXPRESSION_FUNCTION_H
#define FLORAINC_FLORA_GENERATOR_SRC_FUNCTIONS_EXPRESSION_FUNCTION_H

#include <flora_generator.h>

/*! Function which value is determined by evaluating an expression.
 * It is assumed all of expression functions are not built-in and
 * defined in source. */
class ExpressionFunction : public FG::implfg::Function
{
public:
    /*!
     * Construct ExpressionFunction.
     * @param name The name of the function.
     * @param expr Expression to be evaluated during function call.
     * @param parameters Parameters of function.
     */
    ExpressionFunction(std::string name,
                       FG::implfg::Expression expr,
                       std::vector<std::string> parameters);
    ~ExpressionFunction() override = default;

    /*!
     * Return the name of the function.
     * @return The function's name,
     */
    const std::string& getName() override;

    /*!
     * Return false since all of the expression functions are assumed to
     * be defined in source.
     * @return False.
     */
    bool isBuiltIn() override;
    /*!
     * Return number of parameters taken.
     * @return The number of parameters.
     */
    size_t getParametersCount() override;
    /*!
     * Evaluate expression and return result.
     * @param functions The array of functions which can be used during
     * evaluation (appear in expression and subexpressions).
     * @param arguments Arguments of the call.
     * @param log String which is filled with the evaluation attempt
     * information.
     * @return The value yielded by the expression stored.
     * NAN if any error occured during
     * expression evaluation or if size of arguments array does
     * not match number of parameters.
     */
    double yield(std::vector<FG::implfg::Function*>& functions,
                 std::vector<double> arguments,
                 std::string& log) override;
private:
    std::string name;
    FG::implfg::Expression expression;
    std::vector<std::string> parameters;
};

#endif
