#ifndef FLORAINC_FLORA_GENERATOR_SRC_FUNCTIONS_ADD_BUILTIN_FUNCTIONS_H
#define FLORAINC_FLORA_GENERATOR_SRC_FUNCTIONS_ADD_BUILTIN_FUNCTIONS_H

#include <flora_generator.h>

#include <vector>

/*!
 * Add all of built-in functions to functions array.
 * @param functions Array to be expanded by built-in functions.
 */
void addBuiltinFunctions(std::vector<FG::implfg::Function*>& functions);

#endif
