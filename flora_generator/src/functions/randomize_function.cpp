#include "randomize_function.h"

using namespace FG::implfg;

RandomizeFunction* RandomizeFunction::getRandomizeFunction()
{
    static RandomizeFunction function;
    return &function;
}

void RandomizeFunction::setSeed(unsigned seed)
{
    randomDevice.seed(seed);
}

const std::string& RandomizeFunction::getName()
{
    static const std::string randomizeName = "Randomize";
    return randomizeName;
}

bool RandomizeFunction::isBuiltIn()
{
    return true;
}

size_t RandomizeFunction::getParametersCount()
{
    return 2;
}

double RandomizeFunction::yield(std::vector<Function*>&,
                                std::vector<double> arguments,
                                std::string&)
{
    std::uniform_real_distribution<double> distr
        = std::uniform_real_distribution<double>(arguments[0], 
                                                 arguments[1]);

    return distr(randomDevice);
}
