#include "expression_function.h"

using namespace FG::implfg;

ExpressionFunction::ExpressionFunction(std::string name,
                                       Expression expr,
                                       std::vector<std::string> parameters)
    : name(std::move(name)), expression(expr), parameters(parameters)
{ }

const std::string& ExpressionFunction::getName()
{
    return name;
}

bool ExpressionFunction::isBuiltIn()
{
    return false;
}

size_t ExpressionFunction::getParametersCount()
{
    return parameters.size();
}

double ExpressionFunction::yield(std::vector<Function*>& functions,
                                 std::vector<double> arguments,
                                 std::string& log)
{
    if(arguments.size() != parameters.size())
        return NAN;

    return expression.yield(functions, parameters, arguments, log);
}
