#include "add_builtin_functions.h"

#include "randomize_function.h"

void addBuiltinFunctions(std::vector<FG::implfg::Function*>& functions)
{
    functions.push_back(RandomizeFunction::getRandomizeFunction());
}
