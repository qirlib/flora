#include "leaf_render_object.h"

#include <algorithm>

#include "../textures.h"

using namespace FG;

std::vector<RenderObjectVertex> LeafRenderObject::vertices1 = {};
std::vector<RenderObjectVertex> LeafRenderObject::vertices2 = {};
BoundingAABox LeafRenderObject::box = {};

LeafRenderObject::LeafRenderObject(frMatrix4x4 transform)
    : transform(transform)
{
    const double scale = 0.23;
    frVector4 normal = FR_VEC3(0, 0, -1);

    if(vertices1.size() == 0)
    {
        vertices1.push_back({ FR_VEC3(0, 0, 0),
                              normal, 0, 4 / 13.0 });
        vertices1.push_back({ FR_VEC3(2 / 13.0, 3 / 13.0, 0),
                              normal, 2 / 13.0, 3 / 13.0 + 4 / 13.0 });
        vertices1.push_back({ FR_VEC3(6 / 13.0, 4 / 13.0, 0),
                              normal, 6 / 13.0, 4 / 13.0 + 4 / 13.0 });
        vertices1.push_back({ FR_VEC3(11 / 13.0, 3 / 13.0, 0),
                              normal, 11 / 13.0, 3 / 13.0 + 4 / 13.0 });
        vertices1.push_back({ FR_VEC3(1, 0, 0),
                              normal, 1, 4 / 13.0 });
        vertices1.push_back({ FR_VEC3(11 / 13.0, -3 / 13.0, 0),
                              normal, 11 / 13.0, 1 / 13.0 });
        vertices1.push_back({ FR_VEC3(6 / 13.0, -4 / 13.0, 0),
                              normal, 6 / 13.0, 0 });
        vertices1.push_back({ FR_VEC3(2 / 13.0, -3 / 13.0, 0),
                              normal, 2 / 13.0, 1 / 13.0 });
        vertices1.push_back({ FR_VEC3(0, 0, 0),
                              normal, 0, 4 / 13.0 });

        for(RenderObjectVertex& vertex : vertices1)
            vertex.position = FR_VEC3_MUL(vertex.position, scale);

        vertices2.resize(vertices1.size());

        std::reverse_copy(std::begin(vertices1),
                          std::end(vertices1),
                          std::begin(vertices2));

        for(RenderObjectVertex& vertex : vertices2)
            vertex.normal = FR_VEC3_NEG(vertex.normal);

        vertices1.shrink_to_fit();
        vertices2.shrink_to_fit();

        box = BoundingAABox::computeAABox(vertices1.data(), vertices1.size());

        initializeWoodLeafTextures();
    }
}

frMatrix4x4 LeafRenderObject::getTransform()
{
    return transform;
}

frTexture LeafRenderObject::getTexture()
{
    return getLeafTexture();
}

BoundingAABox LeafRenderObject::getBoundingAABox()
{
    return box.getTransformed(transform);
}

unsigned LeafRenderObject::getPolygonCount()
{
    return POLYGON_COUNT;
}

void LeafRenderObject::draw(frContext* context,
                            frVsFunc vertexShader,
                            frFsFunc fragmentShader,
                            void* vertexUniform,
                            void* fragmentUniform)
{
    frDraw(context,
           vertexShader,
           fragmentShader,
           vertexUniform,
           fragmentUniform,
           frPrimitiveTypeTrianglesFan,
           vertices1.data(),
           sizeof(RenderObjectVertex),
           vertices1.size());
    frDraw(context,
           vertexShader,
           fragmentShader,
           vertexUniform,
           fragmentUniform,
           frPrimitiveTypeTrianglesFan,
           vertices2.data(),
           sizeof(RenderObjectVertex),
           vertices2.size());
}
