#ifndef FLORAINC_FLORA_GENERATOR_SRC_RENDER_OBJECTS_LEAF_RENDER_OBJECT_H
#define FLORAINC_FLORA_GENERATOR_SRC_RENDER_OBJECTS_LEAF_RENDER_OBJECT_H

#include <flora_generator.h>

#include <random>

/*! Render object representing leaf using triangles. */
class LeafRenderObject : public FG::RenderObject
{
public:
    /*! Number of triangles used. */
    const unsigned POLYGON_COUNT = 6;

    /*!
     * Construct leaf with relative position determined by transform.
     * @param transform Matrix representing relative position of a leaf.
     */
    LeafRenderObject(frMatrix4x4 transform);

    ~LeafRenderObject() override = default;

    frMatrix4x4 getTransform() override;
    frTexture getTexture() override;
    BoundingAABox getBoundingAABox() override;
    unsigned getPolygonCount() override;

    void draw(frContext*, frVsFunc, frFsFunc, void*, void*) override;
private:
    frMatrix4x4 transform;

    static std::vector<FG::RenderObjectVertex> vertices1, vertices2;
    static BoundingAABox box;
};

#endif
