#ifndef FLORAINC_FLORA_GENERATOR_SRC_RENDER_OBJECTS_BRANCH_RENDER_OBJECT_H
#define FLORAINC_FLORA_GENERATOR_SRC_RENDER_OBJECTS_BRANCH_RENDER_OBJECT_H

#include <flora_generator.h>

#include <random>

/*! Render object representing branch using a cylindrical surface. */
class BranchRenderObject : public FG::RenderObject
{
public:
    /*!
     * Generate a render object representing branch using data supplied.
     * @param randomEngine Reference to the random engine to be used.
     * @param transform Relative position of a branch.
     * @param widthStart Width at the beginning of a branch.
     * @param widthEnd Width at the end of a branch.
     * @param height The height of a branch.
     */
    BranchRenderObject(std::mt19937& randomEngine,
                       frMatrix4x4 transform,
                       double widthStart,
                       double widthEnd,
                       double height);

    ~BranchRenderObject() override = default;

    frMatrix4x4 getTransform() override;
    frTexture getTexture() override;
    BoundingAABox getBoundingAABox() override;
    unsigned getPolygonCount() override;

    void draw(frContext*, frVsFunc, frFsFunc, void*, void*) override;
private:
    frMatrix4x4 transform;
    std::vector<FG::RenderObjectVertex> vertices;
    BoundingAABox box;

    size_t roundVerticles, horizontalVerticles;
    static std::vector<unsigned int> indicies;
};

#endif
