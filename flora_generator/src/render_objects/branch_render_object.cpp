#include "branch_render_object.h"

#include <random>

#include "../textures.h"

using namespace FG;

std::vector<unsigned int> BranchRenderObject::indicies = {};

BranchRenderObject::BranchRenderObject(std::mt19937&,
                                       frMatrix4x4 transform,
                                       double widthStart,
                                       double widthEnd,
                                       double height)
    : transform(transform)
{
    const size_t MIN_ROUND_VERTICLES = 10;
    const size_t MIN_HORIZONTAL_VERTICLES = 2;

    const double ROUND_VERT_LOG_MULTIPLIER = 8;
    const double HORIZONTAL_VERT_LOG_MULTIPLIER = 0.5;

    roundVerticles = (size_t)round(ROUND_VERT_LOG_MULTIPLIER *
                                   log(fmax((widthStart + widthEnd) / 2.0, 1))) 
                         + MIN_ROUND_VERTICLES;
    horizontalVerticles = (size_t)round(HORIZONTAL_VERT_LOG_MULTIPLIER *
                                        log(fmax(height, 1)))
                              + MIN_HORIZONTAL_VERTICLES;

    vertices.reserve(roundVerticles * horizontalVerticles);
    indicies.reserve(roundVerticles * 2 + 2);

    RenderObjectVertex vertex;

    double u = 0, v = 0, angle = 0, angleStep = 2 * FR_PI / roundVerticles;
    double verticalStep = height / (horizontalVerticles - 1);

    double width = widthStart,
           widthStep = (widthEnd - widthStart) / (horizontalVerticles - 1);

    const double U_PER_ROUND_VERTICLE = 2.0;
    const double V_PER_HORIZ_VERTICLE = 2.2;

    for(size_t i = 0; i < horizontalVerticles; i++)
    {
        angle = 0;
        u = 0;

        for(size_t j = 0; j < roundVerticles; j++)
        {
            vertex.position = FR_VEC3(cos(angle) * (width / 2.0),
                                      verticalStep * i,
                                      sin(angle) * (width / 2.0));

            vertex.u = u;
            vertex.v = v;

            vertices.push_back(vertex);
            angle += angleStep;

            u += U_PER_ROUND_VERTICLE / roundVerticles;
        }

        v += V_PER_HORIZ_VERTICLE / horizontalVerticles;

        width += widthStep;
    }

    box = BoundingAABox::computeAABox(vertices.data(), vertices.size());

    const int lastRow = horizontalVerticles - 1;

    int hAdd, hSub;
    for(size_t i = 0; i < roundVerticles; i++)
    {
#define ELEM(x, y) (vertices[(x) + (y) * roundVerticles])
        hAdd = i != roundVerticles - 1 ? 1 : -roundVerticles + 1;
        hSub = i != 0 ? 1 : -roundVerticles + 1;

        ELEM(i, 0).normal = frVector3GetNormal3(ELEM(i, 0).position,
                                                ELEM(i - hSub, 0).position,
                                                ELEM(i, 1).position,
                                                ELEM(i + hAdd, 0).position);

        ELEM(i, lastRow)
                .normal = frVector3GetNormal3(ELEM(i, lastRow).position,
                                              ELEM(i + hAdd, lastRow).position,
                                              ELEM(i, lastRow - 1).position,
                                              ELEM(i - hSub, lastRow).position);
    }

    for(size_t i = 1; i < horizontalVerticles - 1; i++)
    {
        for(size_t j = 0; j < roundVerticles; j++)
        {
            hAdd = j != roundVerticles - 1 ? 1 : -roundVerticles + 1;
            hSub = j != 0 ? 1 : -roundVerticles + 1;
            ELEM(j, i).normal =
                frVector3GetNormal4Looped(ELEM(j, i).position,
                                          ELEM(j, i + 1).position,
                                          ELEM(j + hAdd, i).position,
                                          ELEM(j, i - 1).position,
                                          ELEM(j - hSub, i).position);
        }
    }
#undef ELEM

    initializeWoodLeafTextures();
}

frMatrix4x4 BranchRenderObject::getTransform()
{
    return transform;
}

frTexture BranchRenderObject::getTexture()
{
    return getWoodTexture();
}

BoundingAABox BranchRenderObject::getBoundingAABox()
{
    return box.getTransformed(transform);
}

unsigned BranchRenderObject::getPolygonCount()
{
    return roundVerticles * (horizontalVerticles - 1) * 2;
}

void BranchRenderObject::draw(frContext* context,
                              frVsFunc vertexShader,
                              frFsFunc fragmentShader,
                              void* vertexUniform,
                              void* fragmentUniform)
{
    indicies.resize(roundVerticles * 2 + 2);

    size_t i;
    for(i = 0; i < roundVerticles; i++)
    {
        indicies[i * 2] = i;
        indicies[i * 2 + 1] = i + roundVerticles;
    }

    indicies[i * 2] = 0;
    indicies[i * 2 + 1] = roundVerticles;

    for(i = 0; i < horizontalVerticles - 1; i++)
    {
        frDrawWithIndicies(context,
                           vertexShader,
                           fragmentShader,
                           vertexUniform,
                           fragmentUniform,
                           frPrimitiveTypeTrianglesStrip,
                           vertices.data() + i * roundVerticles,
                           sizeof(RenderObjectVertex),
                           indicies.data(),
                           indicies.size());
    }
}
