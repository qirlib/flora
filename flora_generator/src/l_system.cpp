#include <flora_generator.h>

using namespace FG;
using namespace FG::implfg;

LSystem::LSystem()
    : sourceString(),
      buildStatus(false),
      log("Не было попытки верификации."),
      functions(),
      aliases(),
      axiom(),
      rules(),
      maximumIterations(DEFAULT_MAXIMUM_ITERATIONS),
      preferredIterations(DEFAULT_PREFERRED_ITERATIONS)
{ }

LSystem::~LSystem()
{
    for(Function* function : this->functions)
        if(!function->isBuiltIn())
            delete function;
}

void LSystem::setSource(std::string source)
{
    sourceString = source;
}

const std::string& LSystem::getSource()
{
    return sourceString;
}

bool LSystem::getVerificationStatus() const
{
    return buildStatus;
}

const std::string& LSystem::getLog()
{
    int i = (int)log.size() - 1;

    while(i >= 0 && std::isspace(log[i])) i--;
    log.erase(i + 1);

    return log;
}

int LSystem::getMaximumIterations() const
{
    return maximumIterations;
}

int LSystem::getPreferredIterations() const
{
    return preferredIterations;
}
