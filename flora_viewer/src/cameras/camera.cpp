#include "camera.h"

Camera::Camera(QWidget* widget)
    : renderArea(widget)
{ }

bool Camera::processMouseMoveEvent(QMouseEvent*)
{ return false; }
bool Camera::processMousePressEvent(QMouseEvent*)
{ return false; }
bool Camera::processMouseReleaseEvent(QMouseEvent*)
{ return false; }
bool Camera::processMouseDoubleClickEvent(QMouseEvent*)
{ return false; }
bool Camera::processWheelEvent(QWheelEvent*)
{ return false; }

bool Camera::processKeyPressEvent(QKeyEvent*)
{ return false; }
bool Camera::processKeyReleaseEvent(QKeyEvent*)
{ return false; }
