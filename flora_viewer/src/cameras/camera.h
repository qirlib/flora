#ifndef FLORAINC_FLORA_VIEWER_SRC_CAMERAS_CAMERA_H
#define FLORAINC_FLORA_VIEWER_SRC_CAMERAS_CAMERA_H

#include <flora_renderer.h>

/*! \file
 * Camera classes are used to store and query data about
 * current state of the camera which is
 * changed according to input events.
 * Camera classes should implement a subset of process*Event() methods
 * to make virtual camera controllable. Every process*Event() method
 * should modify camera's internal state when needed and return value
 * that determines whenether scene should be redrawn or not.
 */

#include <QWidget>

/*!
 * Represents camera.
 * Camera processes input events and generates
 * view matrix. The behavior is determined by the subclasses.
 */
class Camera
{
public:
    Camera(QWidget* widget);
    virtual ~Camera() = default;

    /*! See camera.h file documentation for details. */
    virtual bool processMouseMoveEvent(QMouseEvent* event);
    /*! See camera.h file documentation for details. */
    virtual bool processMousePressEvent(QMouseEvent* event);
    /*! See camera.h file documentation for details. */
    virtual bool processMouseReleaseEvent(QMouseEvent* event);
    /*! See camera.h file documentation for details. */
    virtual bool processMouseDoubleClickEvent(QMouseEvent* event);
    /*! See camera.h file documentation for details. */
    virtual bool processWheelEvent(QWheelEvent* event);

    /*! See camera.h file documentation for details. */
    virtual bool processKeyPressEvent(QKeyEvent* event);
    /*! See camera.h file documentation for details. */
    virtual bool processKeyReleaseEvent(QKeyEvent* event);

    /*!
     * Return current view matrix.
     * @return View matrix.
     */
    virtual frMatrix4x4 getViewMatrix() = 0;
    /*!
     * Return the position of the matrix.
     * @return Position vector.
     */
    virtual frVector4 getPosition() = 0;

    /*!
     * Return rotation of camera around Y axis.
     * @return Rotation in radians.
     */
    virtual double getYRotation() = 0;
    /*!
     * Return rotation of camera around Z axis.
     * @return Rotation in radians.
     */
    virtual double getZRotation() = 0;
protected:
    QWidget* renderArea;
};

#endif
