#ifndef FLORAINC_FLORA_VIEWER_SRC_CAMERAS_AXIS_CAMERA_H
#define FLORAINC_FLORA_VIEWER_SRC_CAMERAS_AXIS_CAMERA_H

#include "camera.h"

#include <QWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QTimer>
#include <QTimerEvent>
#include <QTime>

/*!
 * Implements camera which is set up to be rotating around Y axis.
 */
class AxisCamera : public Camera
{
public:
    /*! Vector which indicates UPWARD direction. */
    static constexpr frVector4 VECTOR_UP = { 0, 1, 0, 1 };

    const double DEFAULT_MINIMUM_DISTANCE = 0.1;
    const double DEFAULT_MAXIMUM_DISTANCE = 10000;
    const double DEFAULT_ANGLE_SPEED_COEF = 0.01;
    const double DEFAULT_MOVEMENT_SPEED = 0.1;

    AxisCamera(QWidget* widget);
    ~AxisCamera() override = default;

    /*!
     * Return center on which camera is focused.
     * @return Center of the camera.
     */
    frVector4 getCenter();
    frVector4 getPosition() override;

    bool processMouseMoveEvent(QMouseEvent* event) override;
    bool processMousePressEvent(QMouseEvent* event) override;
    bool processMouseReleaseEvent(QMouseEvent* event) override;
    bool processWheelEvent(QWheelEvent* event) override;

    bool processKeyPressEvent(QKeyEvent* event) override;

    frMatrix4x4 getViewMatrix() override;

    double getYRotation() override;
    double getZRotation() override;
private:
    frVector4 center;

    double verticalOffsetSpeedCoef;
    double distance, distanceSpeedCoef, minimumDistance;
    double angleY, angleYSpeedCoef;
    double angleVert, angleVertSpeedCoef;

    int oldX, preMoveX;
    int oldY, preMoveY;

    bool hiddenCursorMode;
};

#endif
