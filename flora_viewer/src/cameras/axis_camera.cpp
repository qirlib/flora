#include "axis_camera.h"

#include "../util.h"

constexpr frVector4 AxisCamera::VECTOR_UP;

AxisCamera::AxisCamera(QWidget* renderArea)
    : Camera(renderArea),
      center(FR_VEC3(0, 0, 0)),
      verticalOffsetSpeedCoef(0.01),
      distance(120),
      distanceSpeedCoef(0.01),
      minimumDistance(DEFAULT_MINIMUM_DISTANCE),
      angleY(FR_PI / 8),
      angleYSpeedCoef(DEFAULT_ANGLE_SPEED_COEF),
      angleVert(FR_PI / 8),
      angleVertSpeedCoef(DEFAULT_ANGLE_SPEED_COEF),
      hiddenCursorMode(false)
{ }

frVector4 AxisCamera::getCenter()
{
    return center;
}

frVector4 AxisCamera::getPosition()
{
    double horizontalDistance = distance * cos(angleVert);

    return FR_VEC3(center.x + horizontalDistance * cos(angleY),
                   center.y + distance * sin(angleVert),
                   center.z + horizontalDistance * sin(angleY));
}

static double clampAngle(double angle)
{
    while(angle < 0)
        angle += 2 * FR_PI;

    while(angle > 2 * FR_PI)
        angle -= 2 * FR_PI;

    return angle;
}

bool AxisCamera::processMouseMoveEvent(QMouseEvent* event)
{
    if(!hiddenCursorMode)
        return false;

    int deltaX = event->x() - oldX;
    int deltaY = event->y() - oldY;

    if(event->buttons() & Qt::LeftButton)
    {
        center.x += cos(angleY) * DEFAULT_MOVEMENT_SPEED * deltaY;
        center.z += sin(angleY) * DEFAULT_MOVEMENT_SPEED * deltaY;

        center.x -= cos(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED * deltaX;
        center.z -= sin(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED * deltaX;
    }
    else
    {
        angleY += deltaX * angleYSpeedCoef;
        angleVert += deltaY * angleVertSpeedCoef;

        angleY = clampAngle(angleY);
        angleVert = clampValue(angleVert,
                               -FR_PI / 2 + FR_TINY,
                                FR_PI / 2 - FR_TINY);
    }

    oldX = renderArea->width() / 2;
    oldY = renderArea->height() / 2;

    QCursor::setPos(renderArea->mapToGlobal(QPoint(oldX, oldY)));

    return true;
}

bool AxisCamera::processMousePressEvent(QMouseEvent* event)
{
    if(event->button() != Qt::LeftButton && event->button() != Qt::RightButton)
        return false;

    if(!hiddenCursorMode)
    {
        hiddenCursorMode = true;

        oldX = preMoveX = event->x();
        oldY = preMoveY = event->y();

        QCursor cursor = renderArea->cursor();
        cursor.setShape(Qt::BlankCursor);
        renderArea->setCursor(cursor);
    }

    return false;
}

bool AxisCamera::processMouseReleaseEvent(QMouseEvent* event)
{
    if(event->buttons() & (Qt::LeftButton | Qt::RightButton))
        return false;

    hiddenCursorMode = false;

    QCursor cursor = renderArea->cursor();
    cursor.setShape(Qt::ArrowCursor);
    renderArea->setCursor(cursor);

    QCursor::setPos(renderArea->mapToGlobal(QPoint(preMoveX, preMoveY)));

    return true;
}

bool AxisCamera::processWheelEvent(QWheelEvent* event)
{
    if(event->modifiers() == Qt::ShiftModifier)
    {
        center.y += event->delta() * verticalOffsetSpeedCoef;
    }
    else
    {
        distance -= event->delta() * distanceSpeedCoef;
        distance = clampValue(distance,
                              minimumDistance,
                              DEFAULT_MAXIMUM_DISTANCE);
    }

    return true;
}

bool AxisCamera::processKeyPressEvent(QKeyEvent* event)
{
    frVector4 oldCenter = center;

    if(event->key() == Qt::Key_S || event->key() == Qt::Key_Down)
    {
        center.x += cos(angleY) * DEFAULT_MOVEMENT_SPEED;
        center.z += sin(angleY) * DEFAULT_MOVEMENT_SPEED;
    }

    if(event->key() == Qt::Key_W || event->key() == Qt::Key_Up)
    {
        center.x -= cos(angleY) * DEFAULT_MOVEMENT_SPEED;
        center.z -= sin(angleY) * DEFAULT_MOVEMENT_SPEED;
    }
    
    if(event->key() == Qt::Key_A || event->key() == Qt::Key_Left)
    {
        center.x += cos(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED;
        center.z += sin(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED;
    }
    
    if(event->key() == Qt::Key_D || event->key() == Qt::Key_Right)
    {
        center.x -= cos(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED;
        center.z -= sin(angleY + FR_PI / 2) * DEFAULT_MOVEMENT_SPEED;
    }

    return !frVector3Equal(oldCenter, center);
}

frMatrix4x4 AxisCamera::getViewMatrix()
{
    return frMatrixCreateLookAt(getPosition(),
                                center,
                                AxisCamera::VECTOR_UP);
}

double AxisCamera::getYRotation()
{
    return angleY;
}

double AxisCamera::getZRotation()
{
    return angleVert;
}
