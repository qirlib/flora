#ifndef FLORAINC_FLORA_VIEWER_SRC_RENDERAREA_H
#define FLORAINC_FLORA_VIEWER_SRC_RENDERAREA_H

#include <vector>
#include <QWidget>

#include "renderarea_frame_generator.h"

/*!
 * Rendering area widget.
 * It can be set up that new frames are generated automatically
 * every time the widget is redrawn or it can be set up so
 * that new frames are only generated when requested by user.
 * Frame generation is performed by RenderAreaFrameGenerator.
 *
 * There are a set of process*Event() functions in which events are forwarded
 * to camera currently referenced by RenderAreaFrameGenerator.
 * The update() method is called if camera's internal state was changed.
 */
class RenderArea : public QWidget
{
    Q_OBJECT
public:
    /*!
     * Set up RenderArea.
     * @param parent Widget-parent of renderArea.
     */
    explicit RenderArea(QWidget *parent);

    ~RenderArea() override = default;

    /*! Set whenether new frames are generated automatically or not. */
    void setAutomaticRedraw(bool);
    /*! Return true if new frames are generated automatically,
     * false otherwise. */
    bool isAutomaticallyRedrawn();

    /*! Return generator associated. */
    RenderAreaFrameGenerator& getGenerator();

    /*! Force regeneration of a new frame. */
    void forceRegeneration();

    /*! Return if there is valid frame to draw. */
    bool hasValidFrame();
    /*! Return current frame displayed. */
    const QImage& getCurrentFrame();

    /*! See class documentation for details. */
    void resizeEvent(QResizeEvent*) override;

    /*! See class documentation for details. */
    void mouseMoveEvent(QMouseEvent*) override;
    /*! See class documentation for details. */
    void mousePressEvent(QMouseEvent*) override;
    /*! See class documentation for details. */
    void mouseReleaseEvent(QMouseEvent*) override;
    /*! See class documentation for details. */
    void mouseDoubleClickEvent(QMouseEvent*) override;
    /*! See class documentation for details. */
    void wheelEvent(QWheelEvent*) override;

    /*! See class documentation for details. */
    void keyPressEvent(QKeyEvent*) override;
    /*! See class documentation for details. */
    void keyReleaseEvent(QKeyEvent*) override;

    /*!
     * An event handler called by Qt Event System where the widget
     * rendering is meant to be done.
     * The rendering of each new frame can be requested by calling
     * update() function.
     * Generates a new frame each time called if automatic redraw mode
     * is enabled.
     * The widget is filled with red color if new frame could not be
     * generated.
     */
    void paintEvent(QPaintEvent*) override;
private:
    QImage currentFrame;

    RenderAreaFrameGenerator generator;

    bool automaticallyRedrawFrame;
    bool haveValidFrame;
signals:
    /*!
     * Signal is emitted every time frame is finished.
     */
    void onFrameFinished();
};

#endif
