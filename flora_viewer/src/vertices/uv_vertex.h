#ifndef FLORAINC_FLORA_VIEWER_SRC_VERTICLES_UV_VERTEX_H
#define FLORAINC_FLORA_VIEWER_SRC_VERTICLES_UV_VERTEX_H

#include <flora_renderer.h>

/*!
 * Verticle that has u, v coordinates associated with it.
 */
struct UVVertex
{
    frVector4 position;
    double u;
    double v;
};

#endif
