#ifndef FLORAINC_FLORA_VIEWER_SRC_VERTICLES_COLORED_VERTEX_H
#define FLORAINC_FLORA_VIEWER_SRC_VERTICLES_COLORED_VERTEX_H

#include <flora_renderer.h>

/*!
 * Verticle that has a color assosiated with it.
 */
struct ColoredVertex
{
    frVector4 position;
    frColor color;
};

#endif
