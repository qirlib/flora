#include "error_handling.h"

#include <cstdio>
#include <cstdlib>
#include <cstdarg>

void fatalError(const char* format, ...)
{
    va_list args;
    va_start(args, format);

    fprintf(stderr, "%s", "fatal: ");
    vfprintf(stderr, format, args);
    fprintf(stderr, "%s", "\n");

    va_end(args);

    exit(EXIT_FAILURE);
}
