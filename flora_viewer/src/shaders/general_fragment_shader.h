#ifndef FLORAINC_FLORA_VIEWER_SRC_SHADERS_GENERAL_FRAGMENT_SHADER_H
#define FLORAINC_FLORA_VIEWER_SRC_SHADERS_GENERAL_FRAGMENT_SHADER_H

#include <flora_renderer.h>

#include "../frame_generator_settings.h"
#include "../util.h"

/*! Uniform data of textureFragmentShader<>() */
struct TFSUniformData
{
    frVector4 globalLightVector;
    frBuffer2d* shadowDepthBuffer;
    frMatrix4x4* shadowProjectionView;
    frTexture texture;
    frMatrix4x4 modelView;
    frMatrix4x4 normalTransform;
};

const double SHADOW_COLOR_COEF = 0.5;

/*!
 * The namespace used to hide implementation details. Anything in it
 * must to be used by external users.
 */
namespace implfv
{
    /*!
     * Return the coefficient which determines how much the fragment
     * is shadowed. Appropriate color can be obtained by multiplying
     * all color components by that coefficient.
     * @param v Position of the fragment.
     * @param buffer Shadow map.
     * @return Shadowing coefficient.
     */
    inline double shadowed(frVector4 v, frBuffer2d* buffer)
    {
        size_t x = (size_t)trunc(v.x);
        size_t y = (size_t)trunc(v.y);

        if(x >= buffer->width || y >= buffer->height)
            return 1;

        int xAdd = x < buffer->width - 1 ? 1 : -1;
        int yAdd = x < buffer->height - 1 ? 1 : -1;

        uint32_t depth = (uint32_t)(round((v.z + 1) *
                                   ((double)UINT32_MAX / 2.0)));

        double shadow1 = depth > FR_BUFP_EL(buffer, y, x, uint32_t) ? 
                         SHADOW_COLOR_COEF : 1;
        double shadow2 = depth > FR_BUFP_EL(buffer, y, x + xAdd, uint32_t) ?
                         SHADOW_COLOR_COEF : 1;
        double shadow3 = depth > FR_BUFP_EL(buffer, y + yAdd, x, uint32_t) ? 
                         SHADOW_COLOR_COEF : 1;
        double shadow4 = depth > FR_BUFP_EL(buffer,
                                            y + yAdd,
                                            x + xAdd,
                                            uint32_t) ? 
                         SHADOW_COLOR_COEF : 1;

        double i1 = v.x - x, i0 = 1 - i1;

        double shadow12 = FR_MIX(shadow1, shadow2);
        double shadow34 = FR_MIX(shadow3, shadow4);

        i1 = v.y - y, i0 = 1 - i1;

        double resultShadow = FR_MIX(shadow12, shadow34);

        return FR_MIXEQ(resultShadow, std::min(shadow12, shadow34));
    }
}

/*!
 * A generic all-in-one shader template for triangles.
 * @tparam T Type of verticle.
 * @tparam illuminationEnabled Determines whenether illumination is
 * calculated or not.
 * @tparam shadowsEnabled Determines whenether shadows are calculated or not.
 * @tparam texturingEnabled Determines whenether texturing is enabled or not.
 * @tparam accessTexture A functions which is used to access texture.
 * @param uniformPtr Pointer to TFSUniformData fragment uniform.
 * @param v0Ptr Vertex 0.
 * @param v1Ptr Vertex 1.
 * @param v2Ptr Vertex 2.
 * @param i0 Interpolation value 0.
 * @param i1 Interpolation value 1.
 * @param i2 Interpolation value 2.
 * @return
 */
template<typename T,
         bool illuminationEnabled, 
         bool shadowsEnabled,
         bool texturingEnabled,
         frAccessTextureFunc accessTexture>
frColor textureFragmentShader(void* uniformPtr,
                              void* v0Ptr, void* v1Ptr, void* v2Ptr,
                              double i0, double i1, double i2)
{
    TFSUniformData* uniform = static_cast<TFSUniformData*>(uniformPtr);

    T* v0 = static_cast<T*>(v0Ptr);
    T* v1 = static_cast<T*>(v1Ptr);
    T* v2 = static_cast<T*>(v2Ptr);

    frColor color;
    frVector4 normal;
    double coef = 1, lightNormalDot, lightNormalCos;

    if(texturingEnabled)
    {
        double u = FR_MIX3(v0->u, v1->u, v2->u);
        double v = FR_MIX3(v0->v, v1->v, v2->v);

        color = accessTexture(uniform->texture, u, v);
    }
    else
    {
        color = FR_MIX3_COLORS(FR_GRAY, FR_WHITE, FR_YELLOW);
    }

    if(illuminationEnabled || shadowsEnabled)
    {
        normal = frVector3Normalize(
            frMatrixApply(&uniform->normalTransform,
                          FR_MIX3_VEC4(v0->normal, v1->normal, v2->normal)));

        lightNormalDot = frVector3Dot(uniform->globalLightVector, normal);
        lightNormalCos = -std::min(0.0, lightNormalDot);
    }

    if(illuminationEnabled)
    {
        const double MIN_DAYTIME_ILLUMINATION_FACTOR = 0.8;
        double daytimeFactor =
            std::max(0.0,
                     frVector3Dot(uniform->globalLightVector,
                                  FR_VEC3(0, -1, 0)))
                         * MIN_DAYTIME_ILLUMINATION_FACTOR;

        coef *= daytimeFactor + lightNormalCos * (1 - daytimeFactor);
    }

    if(shadowsEnabled)
    {
        if(lightNormalDot >= 0)
        {
            coef *= SHADOW_COLOR_COEF;
        }
        else
        {
            frVector4 cross =
                frVector3Normalize(frVector3Cross(normal,
                                                  uniform->globalLightVector));

            frBuffer2d* depthBuffer = uniform->shadowDepthBuffer;

            frVector4 v = FR_MIX3_VEC4(v0->position,
                                       v1->position,
                                       v2->position);

            v = frMatrixApply(&uniform->modelView, v);

            frVector4 vP = FR_VEC3_ADD(v, FR_VEC3_MUL(normal, 0.08));
            frVector4 vN = FR_VEC3_ADD(v, FR_VEC3_MUL(normal, 0.01));
            frVector4 vC = FR_VEC3_ADD(v, FR_VEC3_MUL(cross, 0.05));

            vP = frMatrixApply(uniform->shadowProjectionView, vP);
            vN = frMatrixApply(uniform->shadowProjectionView, vN);
            vC = frMatrixApply(uniform->shadowProjectionView, vC);

            double shadow1, shadow2, shadow3;

            shadow1 = implfv::shadowed(vP, depthBuffer);
            shadow2 = implfv::shadowed(vN, depthBuffer);
            shadow3 = implfv::shadowed(vC, depthBuffer);

            coef *= FR_MIXEQ(FR_MIX3EQ(shadow1, shadow2, shadow3),
                             std::min({ shadow1, shadow2, shadow3 }));
        }
    }

    if(illuminationEnabled || shadowsEnabled)
    {
        color = frRgb(frColorR(color) * coef,
                      frColorG(color) * coef,
                      frColorB(color) * coef);
    }

    return color;
#undef GET_DEPTH
}

/*!
 * Return a version of a textureFragmentShader<>() which corresponds
 * to the contents of settings.
 * @tparam T Type of verticle.
 * @param settings Settings which determine which features are enabled.
 * @return Fragment shader selected.
 */
template<typename T>
frFsFunc getAppropriateGeneralFragmentShader(FrameGeneratorSettings settings)
{
    if(settings.illuminationEnabled)
    {
        if(settings.shadowsEnabled)
        {
            if(settings.texturingEnabled)
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, true, true, true,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, true, true, true,
                                                 frAccessTexture>;
                }
            }
            else
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, true, true, false,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, true, true, false,
                                                 frAccessTexture>;
                }
            }
        }
        else
        {
            if(settings.texturingEnabled)
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, true, false, true,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, true, false, true,
                                                 frAccessTexture>;
                }
            }
            else
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, true, false, false,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, true, false, false,
                                                 frAccessTexture>;
                }
            }
        }
    }
    else
    {
        if(settings.shadowsEnabled)
        {
            if(settings.texturingEnabled)
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, false, true, true,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, false, true, true,
                                                 frAccessTexture>;
                }
            }
            else
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, false, true, false,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, false, true, false,
                                                 frAccessTexture>;
                }
            }
        }
        else
        {
            if(settings.texturingEnabled)
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, false, false, true,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, false, false, true,
                                                 frAccessTexture>;
                }
            }
            else
            {
                if(settings.bilinearFilteringEnabled)
                {
                    return textureFragmentShader<T, false, false, false,
                                                 frAccessTextureInterpolated>;
                }
                else
                {
                    return textureFragmentShader<T, false, false, false,
                                                 frAccessTexture>;
                }
            }
        }
    }
}

/*!
 * Setup uniform data of textureFragmentShader<>().
 * @param texture Texture to be used in the pass.
 * @param transform Modelview matrix to be used.
 * @param settings Settings which determine parameters of the shader.
 * @return textureFragmentShader uniform data structure.
 */
TFSUniformData setupTFSUniformData(frTexture texture,
                                   frMatrix4x4 transform,
                                   FrameGeneratorSettings settings);

#endif
