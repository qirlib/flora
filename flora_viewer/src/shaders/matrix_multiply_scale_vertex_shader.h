#ifndef FLORAINC_FLORA_VIEWER_SRC_SHADERS_MATRIX_MULTIPLY_SCALE_VERTEX_SHADER_TEMPLATE_H
#define FLORAINC_FLORA_VIEWER_SRC_SHADERS_MATRIX_MULTIPLY_SCALE_VERTEX_SHADER_TEMPLATE_H

#include <flora_renderer.h>

/*!
 * Vertex uniform used by matrixMultiplyScaleVertexShader<>().
 */
struct MMSVSUniform
{
    /*! MVP matrix to be used. */
    frMatrix4x4 transform;
    /*! Factor by which every verticle position is multiplied. */
    double scaleFactor;
};

/*!
 * Vertex shader which multiplies vectors by mvp matrix, scaling
 * them beforehand by scaleFactor uniform parameter.
 * It is assumed the vertex has a 'position' member.
 * @tparam T Type of vertex.
 * @param vertexIn Vertex.
 * @param uniformPtr Pointer to MMSVSUniform uniform data.
 * @return Position of the verticle.
 */
template<typename T>
frVector4 matrixMultiplyScaleVertexShader(void* vertexIn, void* uniformPtr)
{
    MMSVSUniform* uniform = static_cast<MMSVSUniform*>(uniformPtr);

    return frMatrixApply(&uniform->transform,
                         FR_VEC3_MUL(static_cast<T*>(vertexIn)->position,
                                     uniform->scaleFactor));
}
 
#endif
