#include "general_fragment_shader.h"

namespace implfv
{
    double shadowed(frVector4 vector, frBuffer2d* buffer);
}

TFSUniformData setupTFSUniformData(frTexture texture,
                                   frMatrix4x4 modelViewTransform,
                                   FrameGeneratorSettings settings)
{
    TFSUniformData data;

    data.modelView = modelViewTransform;
    data.normalTransform =
        frMatrixTranspose(frMatrixInverse(modelViewTransform));

    data.globalLightVector = settings.globalLightVector;
    data.shadowDepthBuffer = settings.shadowDepthBuffer;
    data.shadowProjectionView = settings.shadowProjectionView;
    data.texture = texture;

    return data;
}
