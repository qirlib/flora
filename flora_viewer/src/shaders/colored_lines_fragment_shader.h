#ifndef FLORAINC_FLORA_VIEWER_SRC_SHADERS_COLORED_LINES_FRAGMENT_SHADER_H
#define FLORAINC_FLORA_VIEWER_SRC_SHADERS_COLORED_LINES_FRAGMENT_SHADER_H

#include <flora_renderer.h>

/*!
 * Fragment shader used for lines which interpolates
 * two colors assosiated with adjacent verticles.
 * @tparam T Type of verticle.
 * @param v0Ptr Pointer to vertex 0.
 * @param v1Ptr Pointer to vertex 1.
 * @param i0 Interpolation value 0.
 * @param i1 Interpolation value 1.
 * @return Interpolated color.
 */
template<typename T>
frColor coloredLinesFragmentShader(void*,
                                   void* v0Ptr, void* v1Ptr, void*,
                                   double i0, double i1, double)
{
    frColor c0 = static_cast<T*>(v0Ptr)->color;
    frColor c1 = static_cast<T*>(v1Ptr)->color;

    return FR_MIX_COLORS(c0, c1);
}

#endif
