#ifndef FLORAINC_FLORA_VIEWER_SRC_SHADERS_MATRIX_MULTIPLY_VERTEX_SHADER_TEMPLATE_H
#define FLORAINC_FLORA_VIEWER_SRC_SHADERS_MATRIX_MULTIPLY_VERTEX_SHADER_TEMPLATE_H

#include <flora_renderer.h>

/*!
 * Vertex shader which multiplies verticle position by applying mvp matrix.
 * It is assumed the vertex has a 'position' member.
 * @tparam T Type of vertex.
 * @param vertexIn Vertex.
 * @param uniformVsData Pointer to frMatrix4x4 matrix.
 * @return Position of the verticle.
 */
template<typename T>
frVector4 matrixMultiplyVertexShader(void* vertexIn, void* uniformVsData)
{
    return frMatrixApply(static_cast<frMatrix4x4*>(uniformVsData),
                         static_cast<T*>(vertexIn)->position);
}
 
#endif
