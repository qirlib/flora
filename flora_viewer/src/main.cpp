#include "mainwindow.h"

#include <QApplication>

/*!
 * Entry point of the program. Sets up Qt Environment.
 * @return Program execution status.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
