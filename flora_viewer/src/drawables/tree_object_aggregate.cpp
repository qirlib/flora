#include "tree_object_aggregate.h"

TreeObjectAggregator::TreeObjectAggregator(Landscape* landscape)
    : Drawable(true), landscape(landscape)
{ }

void TreeObjectAggregator::setTreeObjectsReference(std::vector<TreeObject>* objects)
{
    this->treeObjects = objects;
}

bool TreeObjectAggregator::needsToBeRedrawn()
{
    for(TreeObject& object : *treeObjects)
        if(object.needsToBeRedrawn())
            return true;

    return false;
}

unsigned TreeObjectAggregator::getPolygonCount()
{
    unsigned sum = 0;

    for(TreeObject& object : *treeObjects)
        sum += object.getPolygonCount();

    return sum;
}

BoundingFigure* TreeObjectAggregator::getBoundingFigure()
{
    if(treeObjects->empty())
        return nullptr;

    bool boxSet = false;

    BoundingAABox* boxPtr;
    for(TreeObject& object : *treeObjects)
    {
        boxPtr = dynamic_cast<BoundingAABox*>(object.getBoundingFigure());

        if(!boxPtr)
            continue;

        if(boxSet)
        {
            box.expandBy(*boxPtr);
        }
        else
        {
            box = *boxPtr;
            boxSet = true;
        }
    }

    if(boxSet)
        return &box;
    else
        return nullptr;
}

void TreeObjectAggregator::draw(frContext* context,
                                FrameGeneratorSettings settings)
{
    const double LAYING_DEPTH = 0.5;

    if(treeObjects)
    {
        for(TreeObject& object : *treeObjects)
        {
            if(object.isValid())
            {
                object.setY(landscape->getHeightOfPoint(object.getX(),
                                                        object.getZ())
                            - LAYING_DEPTH);
                object.draw(context, settings);
            }
        }
    }
}
