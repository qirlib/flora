#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_DRAWABLE_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_DRAWABLE_H

#include <flora_renderer.h>

#include "../frame_generator_settings.h"
#include <flora_intersection.h>

/*!
 * Represents object which can be drawn by render engine.
 */
class Drawable
{
public:
    /*!
     * Construct drawable.
     * @param shadowCaster Indicates whenether the object is casting shadows.
     */
    Drawable(bool shadowCaster);
    virtual ~Drawable() = default;

    /*! Set visibility of the object. */
    virtual void setVisible(bool value);
    /*! Return visibility of the object. */
    virtual bool isVisible();

    /*!
     * Return if this object is shadow caster.
     * @return True if object casts shadows, false otherwise.
     */
    virtual bool isShadowCaster();
    /*! Set whenether this object is a shadow caster. */
    virtual void setShadowCaster(bool);
    /*!
     * Return value indicating if this object needs to be redrawn.
     * @return True if object needs to be redrawn, false otherwise.
     */
    virtual bool needsToBeRedrawn();

    /*!
     * Return polygon count of this object.
     * @return Polygon count.
     */
    virtual unsigned getPolygonCount();

    /*!
     * Return bounding figure which covers the drawable.
     * @return Bounding figure.
     */
    virtual BoundingFigure* getBoundingFigure() = 0;

    /*!
     * Draw object with parameters specified in settings.
     * @param context Context.
     * @param settings Different rendering settings.
     */
    virtual void draw(frContext* context, FrameGeneratorSettings settings) = 0;
protected:
    bool visible;
    bool shadowCaster;
};

#endif
