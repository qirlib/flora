#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_ORIGIN_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_ORIGIN_H

#include <flora_renderer.h>

#include "drawable.h"
#include "../vertices/colored_vertex.h"
#include "../shaders/colored_lines_fragment_shader.h"

/*!
 * Represents the origin of coordinate system.
 */
class Origin : public Drawable
{
public:
    Origin(double axisLength = 1);

    ~Origin() override = default;

    /*! Set length of lines used to represent axes. */
    void setAxisLength(double);
    /*! Get length of lines used to represent axes. */
    double getAxisLength();

    BoundingFigure* getBoundingFigure() override;

    void draw(frContext*, FrameGeneratorSettings) override;
private:
    static const ColoredVertex vertices[];
    static const unsigned int indicies[];
    static const size_t INDICIES_COUNT = 6;

    double axisLength;

    BoundingSphere sphere;
};

#endif
