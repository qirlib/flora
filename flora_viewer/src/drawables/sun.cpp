#include "sun.h"

#include "../shaders/matrix_multiply_scale_vertex_shader.h"
#include "../renderarea_frame_generator.h"

const double SUN_RADIUS_MULTIPLY = tan(Sun::SUN_ANGULAR_DIAMETER * 2);

const UVVertex Sun::vertices[] =
    { { FR_VEC3(0, 1, 1), 1, 1 },
      { FR_VEC3(0, 1, -1), 1, 0 },
      { FR_VEC3(0, -1, 1), 0, 1 },
      { FR_VEC3(0, -1, -1), 0, 0 } };

Sun::Sun() : Drawable(false)
{ }

BoundingFigure* Sun::getBoundingFigure()
{
    return nullptr;
}

unsigned Sun::getPolygonCount()
{
    return 2;
}

static frColor sunFragmentShader(void*,
                                 void* v0Ptr, void* v1Ptr, void* v2Ptr,
                                 double i0, double i1, double i2)
{
    UVVertex* v0 = (UVVertex*)v0Ptr;
    UVVertex* v1 = (UVVertex*)v1Ptr;
    UVVertex* v2 = (UVVertex*)v2Ptr;

    const frColor sunColor = frRgb(255, 251, 0);

    double u = FR_MIX3(v0->u, v1->u, v2->u);
    double v = FR_MIX3(v0->v, v1->v, v2->v);

    double distance = hypot(fabs(u - 0.5), fabs(v - 0.5));

    if(distance < 0.4)
    {
        return sunColor;
    }
    else if(distance > 0.5)
    {
        return FR_BLACK;
    }
    else
    {
        i1 = (distance - 0.4) * 10;
        i0 = 1 - i1;

        return FR_MIX_COLORS(sunColor, FR_BLACK);
    }
}

void Sun::draw(frContext* context, FrameGeneratorSettings settings)
{
    RenderAreaFrameGenerator* gen = settings.generator;

    double lightYRotation = gen->getLightSourceYRot();
    double lightZRotation = gen->getLightSourceZRot();

    frVector4 position = gen->getCamera()->getPosition();

    double sunDistance = (settings.farPlane + settings.nearPlane) / 2.0;

    MMSVSUniform uniform {};
    uniform.transform = frMatricesMultiply(*settings.projectionView,
                                           frMatrixCreateTranslationVec(position),
                                           frMatrixCreateRotationY(-lightYRotation),
                                           frMatrixCreateRotationZ(lightZRotation),
                                           frMatrixCreateTranslation(sunDistance, 0, 0));

    uniform.scaleFactor = sunDistance * SUN_RADIUS_MULTIPLY;

    bool zBufferEnabledOld = context->zBufferEnabled;
    context->zBufferEnabled = false;

    frDraw(context,
           matrixMultiplyScaleVertexShader<UVVertex>,
           sunFragmentShader,
           &uniform,
           nullptr,
           frPrimitiveTypeTrianglesStrip,
           vertices,
           sizeof(UVVertex),
           VERTICES_COUNT);

    context->zBufferEnabled = zBufferEnabledOld;
}