#include "landscape.h"

#include <random>

#include "../shaders/matrix_multiply_vertex_shader.h"
#include "../shaders/general_fragment_shader.h"

#define VERT_EL(i, j) (vertices[(i) * width + (j)])

static void calculateLandscapeNormals(LandscapeVertex* vertices,
                                      size_t width, size_t height);

Landscape::Landscape(size_t width,
                     size_t height,
                     double cellSize,
                     double minLevel,
                     double maxLevel,
                     double uvPerCell,
                     unsigned int seed)
    : Drawable(true),
      width(width),
      height(height),
      vertices(nullptr),
      indicies(nullptr)
{
    if(width < 2 || height < 2 || cellSize <= 0)
    {
        valid = false;
        return;
    }

    if(!frBuffer2dCreate(&texture, 1024, 1024, 0, sizeof(frColor)))
    {
        valid = false;
        return;
    }

    try
    {
        vertices = new LandscapeVertex[width * height];
        indicies = new unsigned int[width * 2];
    }
    catch(...)
    {
        valid = false;
        return;
    }

    minLevel *= cellSize;
    maxLevel *= cellSize;

    xOffset = -((width - 1) * cellSize / 2.0);
    yOffset = -((height - 1) * cellSize / 2.0);

    using namespace std;

    mt19937 rd(seed);
    uniform_real_distribution<double> distr1
        = std::uniform_real_distribution<double>(minLevel * cellSize,
                                                 maxLevel * cellSize);

    double u = 0, v = 0;

    for(size_t i = 0; i < height; i++)
    {
        u = 0;

        for(size_t j = 0; j < width; j++)
        {
            VERT_EL(i, j) =
                { FR_VEC3(xOffset + j * cellSize,
                          distr1(rd) +
                          cos(sqrt(pow(xOffset + (j - width / 4.0) * cellSize,
                                       2) +
                                   pow(yOffset + (i - height / 4.0) * cellSize,
                                       2))
                              / (double) 12) * 7,
                          yOffset + i * cellSize),
                  FR_VEC3(0, 0, 0),
                  u,
                  v };

            u += uvPerCell * cellSize;
        }

        v += uvPerCell * cellSize;
    }

    calculateLandscapeNormals(vertices, width, height);

    boundingBox = BoundingAABox::computeAABox<LandscapeVertex>(vertices,
                                                               width * height);
    
    unsigned int* ptr = indicies;
    for(size_t i = 0; i < width; i++)
    {
        *ptr++ = i;
        *ptr++ = i + width;
    }

    const frColor green0 = frRgb(64, 255, 0);
    const frColor green1 = frRgb(41, 166, 0);
    const frColor green2 = frRgb(0, 166, 0);

    uniform_real_distribution<double> distr2
        = std::uniform_real_distribution<double>(0, 1);

    double i0, i1, i2;
    for(size_t i = 0; i < texture.width * texture.height; i++)
    {
        i0 = distr2(rd);
        i1 = distr2(rd);
        i2 = distr2(rd);

        double ratio = 1 / (i0 + i1 + i2);

        i0 *= ratio; i1 *= ratio; i2 *= ratio;

        FR_BUF_LINEAR_EL(texture, i, frColor) =
            FR_MIX3_COLORS(green0, green1, green2);
    }

    valid = true;
}

static void calculateLandscapeNormals(LandscapeVertex* vertices,
                                      size_t width, size_t height)
{
    VERT_EL(0, 0).normal = frVector3GetNormal2(VERT_EL(0, 0).position,
                                               VERT_EL(1, 0).position,
                                               VERT_EL(0, 1).position);

    VERT_EL(width - 1, 0).normal =
        frVector3GetNormal2(VERT_EL(width - 1, 0).position,
                            VERT_EL(width - 1, 1).position,
                            VERT_EL(width - 2, 0).position);

    VERT_EL(width - 1, height - 1).normal =
        frVector3GetNormal2(VERT_EL(width - 1, height - 1).position,
                            VERT_EL(width - 2, height - 1).position,
                            VERT_EL(width - 1, height - 2).position);

    VERT_EL(0, height - 1).normal =
        frVector3GetNormal2(VERT_EL(0, height - 1).position,
                            VERT_EL(0, height - 2).position,
                            VERT_EL(1, height - 1).position);

    for(size_t i = 1; i < height - 1; i++)
    {
        VERT_EL(0, i).normal = frVector3GetNormal3(VERT_EL(0, i).position,
                                                   VERT_EL(0, i - 1).position,
                                                   VERT_EL(1, i).position,
                                                   VERT_EL(0, i + 1).position);
        VERT_EL(width - 1, i).normal =
            frVector3GetNormal3(VERT_EL(width - 1, i).position,
                                VERT_EL(width - 1, i + 1).position,
                                VERT_EL(width - 2, i).position,
                                VERT_EL(width - 1, i - 1).position);
    }

    for(size_t i = 1; i < width - 1; i++)
    {
        VERT_EL(i, 0).normal = frVector3GetNormal3(VERT_EL(i, 0).position,
                                            VERT_EL(i + 1, 0).position,
                                            VERT_EL(i, 1).position,
                                            VERT_EL(i - 1, 0).position);
        VERT_EL(i, height - 1).normal =
            frVector3GetNormal3(VERT_EL(i, height - 1).position,
                                VERT_EL(i - 1, height - 1).position,
                                VERT_EL(i, height - 2).position,
                                VERT_EL(i + 1, height - 1).position);
    }

    for(size_t i = 1; i < height - 1; i++)
    {
        for(size_t j = 1; j < width - 1; j++)
        {
            VERT_EL(i, j).normal =
                frVector3GetNormal4Looped(VERT_EL(i, j).position,
                                          VERT_EL(i + 1, j).position,
                                          VERT_EL(i, j + 1).position,
                                          VERT_EL(i - 1, j).position,
                                          VERT_EL(i, j - 1).position);
        }
    }
}

Landscape::~Landscape()
{
    if(valid)
    {
        frBuffer2dDestroy(&texture);

        delete[] vertices;
        delete[] indicies;
    }
}

double Landscape::getHeightOfPoint(double x, double y)
{
    if(x < xOffset || x > fabs(xOffset) ||
       y < yOffset || y > fabs(yOffset))
    {
        return 0;
    }

    x = (x - xOffset) / (2 * fabs(xOffset)) * width;
    y = (y - yOffset) / (2 * fabs(yOffset)) * height;

    auto x0 = (size_t)trunc(x);
    auto y0 = (size_t)trunc(y);

    int xAdd = x0 + 1 < width ? 1 : -(int)x0;
    int yAdd = y0 + 1 < height ? 1 : -(int)y0;

    double i1 = x - x0, i0 = 1 - i1;

    double p0 = FR_MIX(VERT_EL(x0, y0).position.y,
                       VERT_EL(x0 + xAdd, y0).position.y);
    double p1 = FR_MIX(VERT_EL(x0, y0 + yAdd).position.y,
                       VERT_EL(x0 + xAdd, y0 + yAdd).position.y);

    i1 = y - y0, i0 = 1 - i1;

    return FR_MIX(p0, p1);
}

double Landscape::getMinX()
{
    return xOffset;
}

double Landscape::getMinZ()
{
    return yOffset;
}

double Landscape::getMaxX()
{
    return fabs(xOffset);
}

double Landscape::getMaxZ()
{
    return fabs(yOffset);
}

unsigned Landscape::getPolygonCount()
{
    if(valid)
        return (width - 1) * (height - 1) * 2;
    else
        return 0;
}

BoundingFigure* Landscape::getBoundingFigure()
{
    return &boundingBox;
}

void Landscape::draw(frContext* context, FrameGeneratorSettings settings)
{
    if(!valid)
        return;

    frMatrix4x4 transform = *settings.projectionView;

    TFSUniformData fragmentUniform =
        setupTFSUniformData(frBufferToTexture(&texture),
                            frMatrixIdentity(),
                            settings);

    for(size_t i = 0; i < height - 1; i++) 
    {
        frDrawWithIndicies(context,
                           matrixMultiplyVertexShader<LandscapeVertex>,
                           getAppropriateGeneralFragmentShader<LandscapeVertex>
                               (settings),
                           &transform,
                           &fragmentUniform,
                           frPrimitiveTypeTrianglesStrip,
                           vertices + i * width,
                           sizeof(LandscapeVertex),
                           indicies,
                           width * 2);
    }
}
