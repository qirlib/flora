#include "axis_camera_center.h"

#include "../shaders/matrix_multiply_scale_vertex_shader.h"

const ColoredVertex AxisCameraCenter::vertices[] =
    { { FR_VEC3(-1, 0, 0), FR_BLACK },
      { FR_VEC3(1, 0, 0),  FR_RED   },
      { FR_VEC3(0, -1, 0), FR_BLACK },
      { FR_VEC3(0, 1, 0),  FR_GREEN },
      { FR_VEC3(0, 0, -1), FR_BLACK },
      { FR_VEC3(0, 0, 1),  FR_BLUE  } };

AxisCameraCenter::AxisCameraCenter(AxisCamera* camera, double linesLength)
    : Drawable(true),
      camera(camera),
      oldPosition(FR_VEC4_INVALID),
      linesLength(linesLength)
{ }

void AxisCameraCenter::setLinesLength(double length)
{
    linesLength = length;
}

double AxisCameraCenter::getLinesLength()
{
    return linesLength;
}

bool AxisCameraCenter::needsToBeRedrawn()
{
    return !FR_VEC3_EQ(oldPosition, camera->getCenter());
}

BoundingFigure* AxisCameraCenter::getBoundingFigure()
{
    sphere = BoundingSphere(camera->getCenter(), linesLength + FR_TINY);
    return &sphere;
}

void AxisCameraCenter::draw(frContext* context,
                            FrameGeneratorSettings settings)
{
    MMSVSUniform vertexUniform {};

    oldPosition = camera->getCenter();

    vertexUniform.transform =
        frMatrixMultiply(*settings.projectionView,
                         frMatrixCreateTranslationVec(oldPosition));

    vertexUniform.scaleFactor = linesLength;

    frDraw(context,
           matrixMultiplyScaleVertexShader<ColoredVertex>,
           coloredLinesFragmentShader<ColoredVertex>,
           &vertexUniform,
           nullptr,
           frPrimitiveTypeLines,
           vertices,
           sizeof(ColoredVertex),
           VERTICES_COUNT);
}
