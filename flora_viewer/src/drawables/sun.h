#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_SUN_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_SUN_H

#include "drawable.h"
#include "../vertices/uv_vertex.h"

/*!
 * Represents sun.
 */
class Sun : public Drawable
{
public:
    Sun();
    ~Sun() override = default;

    BoundingFigure* getBoundingFigure() override;
    unsigned getPolygonCount() override;

    void draw(frContext*, FrameGeneratorSettings) override;

    static constexpr double SUN_ANGULAR_DIAMETER = FR_TO_RADIANS(31.5 / 60.0);
private:
    static const UVVertex vertices[];
    static const size_t VERTICES_COUNT = 4;
};

#endif
