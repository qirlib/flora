#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_TREE_OBJECT_AGGREGATE_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_TREE_OBJECT_AGGREGATE_H

#include <vector>
#include "drawable.h"
#include "tree_object.h"
#include "landscape.h"

/*!
 * Represents a collection of TreeObjects which
 * are managed by external means. The positions of TreeObjects
 * are synchronized to Landscape.
 */
class TreeObjectAggregator : public Drawable
{
public:
    /*!
     * Construct a TreeObjectAggregator.
     * @param landscape Reference to landscape to which tree objects in
     * any collection referenced are bound to.
     */
    TreeObjectAggregator(Landscape* landscape);
    ~TreeObjectAggregator() override = default;

    /*!
     * Set a reference to a TreeObject collection.
     * @param objects Pointer to vector of TreeObjects.
     */
    void setTreeObjectsReference(std::vector<TreeObject>* objects);

    bool needsToBeRedrawn() override;

    unsigned getPolygonCount() override;

    BoundingFigure* getBoundingFigure() override;

    void draw(frContext* context, FrameGeneratorSettings settings) override;
private:
    std::vector<TreeObject>* treeObjects = nullptr;
    Landscape* landscape;

    BoundingAABox box;
};

#endif
