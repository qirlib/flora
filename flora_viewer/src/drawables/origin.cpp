#include "origin.h"

#include "../shaders/matrix_multiply_scale_vertex_shader.h"

const ColoredVertex Origin::vertices[] =
    { { FR_VEC3(0, 0, 0), FR_YELLOW },
      { FR_VEC3(1, 0, 0), FR_RED },
      { FR_VEC3(0, 1, 0), FR_GREEN },
      { FR_VEC3(0, 0, 1), FR_BLUE } };

const unsigned int Origin::indicies[] = { 0, 1, 0, 2, 0, 3 };

Origin::Origin(double length)
    : Drawable(false), axisLength(length)
{ }

void Origin::setAxisLength(double value)
{
    axisLength = value;
}

double Origin::getAxisLength()
{
    return axisLength;
}

BoundingFigure* Origin::getBoundingFigure()
{
    sphere = BoundingSphere(FR_VEC3_ZERO, axisLength + FR_TINY);

    return &sphere;
}

/* rendering */

void Origin::draw(frContext* context, FrameGeneratorSettings settings)
{
    MMSVSUniform vertexUniform{};
 
    vertexUniform.transform = *settings.projectionView;
    vertexUniform.scaleFactor = axisLength;

    frDrawWithIndicies(context,
                       matrixMultiplyScaleVertexShader<ColoredVertex>,
                       coloredLinesFragmentShader<ColoredVertex>,
                       &vertexUniform,
                       nullptr,
                       frPrimitiveTypeLines,
                       vertices,
                       sizeof(ColoredVertex),
                       indicies,
                       INDICIES_COUNT);
}
