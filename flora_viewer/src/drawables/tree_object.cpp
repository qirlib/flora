#include "tree_object.h"

#include "../shaders/matrix_multiply_vertex_shader.h"
#include "../shaders/general_fragment_shader.h"

TreeObject::TreeObject(std::vector<FG::LSystem>& lsystems, std::string lSystemUsed)
    : Drawable(true),
      lsystems(lsystems),
      x(0),
      y(0),
      z(0),
      angle(0),
      iterations(1),
      seed(0),
      lSystemUsed(std::move(lSystemUsed)),
      log("Не было попытки интерпретации."),
      valid(false)
{ }

TreeObject::TreeObject(TreeObject&& other) noexcept
    : Drawable(true),
      lsystems(other.lsystems),
      x(other.x),
      y(other.y),
      z(other.z),
      angle(other.angle),
      iterations(other.iterations),
      seed(other.seed),
      lSystemUsed(std::move(other.lSystemUsed)),
      tokens(std::move(other.tokens)),
      log(std::move(other.log)),
      valid(other.valid),
      renderData(std::move(other.renderData))
{ }

TreeObject& TreeObject::operator=(TreeObject&& other)
{
    lsystems = std::move(other.lsystems);
    x = other.x;
    y = other.y;
    z = other.z;
    angle = other.angle;
    iterations = other.iterations;
    seed = other.seed;
    lSystemUsed = std::move(other.lSystemUsed);
    tokens = std::move(other.tokens);
    log = std::move(other.log);
    valid = other.valid;
    renderData = std::move(other.renderData);

    return *this;
}

bool TreeObject::isVisible()
{
    return visible && valid;
}


bool TreeObject::needsToBeRedrawn()
{
    return valid;
}

bool TreeObject::isValid() const
{
    return valid;
}

void TreeObject::setLSystem(std::string lsystem)
{
    lSystemUsed = std::move(lsystem);
}

const std::string& TreeObject::getLSystem() const
{
    return lSystemUsed;
}

void TreeObject::setX(double x)
{
    this->x = x;
}

void TreeObject::setY(double y)
{
    this->y = y;
}

void TreeObject::setZ(double z)
{
    this->z = z;
}

void TreeObject::setAngleY(double value)
{
    angle = value;
}

double TreeObject::getX() const
{
    return x;
}

double TreeObject::getY() const
{
    return y;
}

double TreeObject::getZ() const
{
    return z;
}

double TreeObject::getAngleY() const
{
    return angle;
}

void TreeObject::setIterations(unsigned int iterations)
{
    this->iterations = iterations;
}

unsigned TreeObject::getIterartions() const
{
    return iterations;
}

void TreeObject::setSeed(unsigned int seed)
{
    this->seed = seed;
}

unsigned int TreeObject::getSeed() const
{
    return seed;
}

const std::string& TreeObject::getLog() const
{
    return log;
}

std::vector<FG::InterpretationToken>& TreeObject::getTokens()
{
    return tokens;
}

void TreeObject::regenerate(FG::LSystem& lsystem)
{
    log.clear();

    if(!lsystem.getVerificationStatus())
    {
        log = "L-система не прошла верификацию.";
        return;
    }

    tokens = lsystem.interpret(iterations, seed, log, valid);

    if(valid)
    {
        renderData.regenerate(tokens, log);
        valid = renderData.isValid();

        if(valid)
            log += "Интерпретация и генерация модели успешны.";
        else
            log += "Интерпретация успешна, генерация модели не удалась.";
    }
}

unsigned TreeObject::getPolygonCount()
{
    if(isValid())
        return renderData.getPolygonCount();
    else
        return 0;
}

BoundingFigure* TreeObject::getBoundingFigure()
{
    if(!valid)
        return nullptr;

    BoundingAABox* boxPtr = renderData.getBoundingAABox();
    
    if(!boxPtr)
        return nullptr;

    return &(box = boxPtr->getTransformed(getLocalTransform()));
}

frMatrix4x4 TreeObject::getLocalTransform() const
{
    return frMatrixMultiply(frMatrixCreateTranslation(x, y, z),
                            frMatrixCreateRotationY(-angle));
}

void TreeObject::draw(frContext* context, FrameGeneratorSettings settings)
{
    if(valid)
    {
        TFSUniformData fragmentUniform;
        frMatrix4x4 transform, modelviewTransform, localTransform;

        localTransform = getLocalTransform();

        frFsFunc fragmentShader = getAppropriateGeneralFragmentShader
                                      <FG::RenderObjectVertex>(settings);

        for(FG::RenderObject* object : renderData.getRenderObjects())
        {
            modelviewTransform =
                frMatrixMultiply(localTransform, object->getTransform());

            fragmentUniform = setupTFSUniformData(object->getTexture(),
                                                  modelviewTransform,
                                                  settings);

            transform = frMatrixMultiply(*settings.projectionView,
                                         modelviewTransform);

            object->draw(context,
                         matrixMultiplyVertexShader<FG::RenderObjectVertex>,
                         fragmentShader,
                         &transform,
                         &fragmentUniform);
        }
    }
}
