#include "drawable.h"

Drawable::Drawable(bool shadowCaster)
    : visible(true),
      shadowCaster(shadowCaster)
{ }

void Drawable::setVisible(bool value)
{
    visible = value;
}

bool Drawable::isVisible()
{
    return visible;
}

bool Drawable::isShadowCaster()
{
    return shadowCaster;
}

void Drawable::setShadowCaster(bool value)
{
    shadowCaster = value;
}

bool Drawable::needsToBeRedrawn()
{
    return false;
}

unsigned Drawable::getPolygonCount()
{
    return 0;
}
