#include "stars_skybox.h"

#include <random>

StarsSkybox::StarsSkybox(Camera* camera, size_t starsCount, unsigned seed)
    : Drawable(false), camera(camera), starsCount(starsCount)
{
    try
    {
        vertices = new frVector4[starsCount];
    }
    catch(...)
    {
        valid = false;
        return;
    }

    valid = true;

    using namespace std;
 
    mt19937 rd(seed);
    uniform_real_distribution<double> distr
        = std::uniform_real_distribution<double>(-0.5, 0.5);

    for(size_t i = 0; i < starsCount; i++)
        vertices[i] = FR_VEC3(distr(rd), distr(rd), distr(rd));
}

StarsSkybox::~StarsSkybox()
{
    if(valid)
        delete[] vertices;
}

BoundingFigure* StarsSkybox::getBoundingFigure()
{
    sphere = BoundingSphere(camera->getPosition(), 1);
    return &sphere;
}

static frVector4 starsVertexShader(void* vertexIn, void* uniformVsData)
{
    return frMatrixApply(static_cast<frMatrix4x4*>(uniformVsData),
                         *static_cast<frVector4*>(vertexIn));
}

static frColor starsFragmentShader(void*,
                                   void*, void*, void*,
                                   double, double, double)
{
    return FR_WHITE;
}

void StarsSkybox::draw(frContext* context, FrameGeneratorSettings settings)
{
    if(!valid)
        return;

    frMatrix4x4 transform =
        frMatricesMultiply(*settings.projectionView,
                           frMatrixCreateTranslationVec(camera->getPosition()));

    bool zBufferEnabledOld = context->zBufferEnabled;
    context->zBufferEnabled = false;

    frDraw(context,
           starsVertexShader,
           starsFragmentShader,
           &transform,
           nullptr,
           frPrimitiveTypePoints,
           vertices,
           sizeof(frVector4),
           starsCount);

    context->zBufferEnabled = zBufferEnabledOld;
}
