#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_LANDSCAPE_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_LANDSCAPE_H

#include <flora_renderer.h>
#include <flora_intersection.h>

#include "drawable.h"

struct LandscapeVertex
{
    frVector4 position;
    frVector4 normal;

    double u;
    double v;
};

class Landscape : public Drawable
{
public:
    /*! Default seed used for random number generator. */
    static const unsigned DEFAULT_SEED = 54321;

    /*!
     * Construct landscape by generating its 3d model.
     * @param width Width in number of verticles used. Must be at least two.
     * @param height Height in number of verticles used. Must be at least two.
     * @param cellSize Distance between two adjacent verticles.
     * @param minLevel Minimum level of noise allowed.
     * @param maxLevel Maximum level of noise allowed.
     * @param uvPerCell Amount of how much u, v values of each vertex
     * differ from previous vertex.
     * @param seed Seed used to initialize random number generator.
     */
    Landscape(size_t width,
              size_t height,
              double cellSize,
              double minLevel,
              double maxLevel,
              double uvPerCell,
              unsigned seed = DEFAULT_SEED);

    ~Landscape() override;

    /*!
     * Get height of (x, y) point.
     * @return The height of point requested.
     */
    double getHeightOfPoint(double x, double y);

    /*! Return minimum x which Landscape covers. */
    double getMinX();
    /*! Return minimum z which Landscape covers. */
    double getMinZ();
    /*! Return maximum x which Landscape covers. */
    double getMaxX();
    /*! Return maximum z which Landscape covers. */
    double getMaxZ();

    unsigned getPolygonCount() override;

    BoundingFigure* getBoundingFigure() override;

    void draw(frContext*, FrameGeneratorSettings) override;
private:
    size_t width;
    size_t height;

    double xOffset, yOffset;

    LandscapeVertex* vertices;
    unsigned int* indicies;

    frBuffer2d texture;

    BoundingAABox boundingBox;

    bool valid;
};

#endif