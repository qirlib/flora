#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_STARS_SKYBOX_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_STARS_SKYBOX_H

#include "drawable.h"
#include "../cameras/camera.h"

/*!
 * Represents stars on the sky.
 */
class StarsSkybox : public Drawable
{
public:
    static const size_t DEFAULT_STARS_COUNT = 4096;
    static const unsigned DEFAULT_SEED = 12345;

    /*!
     * Construct stars skybox.
     * @param camera Camera to be tracked.
     * @param starsCount Count of stars to generate.
     * @param seed Seed fed to random number generator.
     */
    StarsSkybox(Camera* camera,
                size_t starsCount = DEFAULT_STARS_COUNT,
                unsigned seed = DEFAULT_SEED);

    ~StarsSkybox() override;

    BoundingFigure* getBoundingFigure() override;

    void draw(frContext*, FrameGeneratorSettings) override;
private:
    frVector4* vertices;
    Camera* camera;

    size_t starsCount;
    bool valid;

    BoundingSphere sphere;
};

#endif
