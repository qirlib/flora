#ifndef FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_AXIS_CAMERA_CENTER_H
#define FLORAINC_FLORA_VIEWER_SRC_DRAWABLES_AXIS_CAMERA_CENTER_H

#include "drawable.h"
#include "../vertices/colored_vertex.h"
#include "../shaders/colored_lines_fragment_shader.h"
#include "../cameras/axis_camera.h"

/*!
 * Represents a figure which is used to indicate center of AxisCamera.
 */
class AxisCameraCenter : public Drawable
{
public:
    AxisCameraCenter(AxisCamera* camera, double linesLength = 0.5);
    ~AxisCameraCenter() override = default;

    /*! Set lines length. */
    void setLinesLength(double);
    /*! Get lines length. */
    double getLinesLength();

    bool needsToBeRedrawn() override;

    BoundingFigure* getBoundingFigure() override;

    void draw(frContext*, FrameGeneratorSettings) override;
private:
    static const ColoredVertex vertices[];
    static const size_t VERTICES_COUNT = 6;

    AxisCamera* camera;
    frVector4 oldPosition;
    double linesLength;

    BoundingSphere sphere;
};

#endif
