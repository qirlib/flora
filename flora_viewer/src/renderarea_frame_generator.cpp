#include "renderarea_frame_generator.h"

#include "error_handling.h"
#include "util.h"

/*! Matrix used to instantly convert coordinates from clip coordinates into
 * viewport coordinates when projecting using shadowProjectionView. */
const frMatrix4x4 RenderAreaFrameGenerator::SHADOW_MAPPING_BIAS_MATRIX
    = frCreateMatrix(0.5,    0,   0, 0.5,
                       0, -0.5,   0, 0.5,
                       0,    0,   1,   0,
                       0,    0,   0,   1);

RenderAreaFrameGenerator::RenderAreaFrameGenerator(QWidget* renderArea)
    : settings { FR_VEC4_INVALID,
                 nullptr, nullptr, nullptr,
                 this,
                 0, 0,
                 true, true, true, true },
      forceShadowsRecalculation(true),
      fov(DEFAULT_FOV),
      lightYRot(0),
      lightZRot(FR_PI / 8),
      shadowsTextureSize(4096),
      perspectiveUsed(true),
      automaticNearFarPlanes(true),
      outputFrameType(OutputFrameType::Frame),
      origin(new Origin()),
      axisCamera(new AxisCamera(renderArea)),
      axisCameraCenter(new AxisCameraCenter(axisCamera))
{
    frContextInit(&context,
                  MAX_EXPECTED_WIDTH,
                  MAX_EXPECTED_HEIGHT,
                  FR_CONTEXT_INIT_FLAGS_NONE);

    frContextInit(&shadowsContext,
                  MAX_SHADOW_TEXTURE_SIZE,
                  MAX_SHADOW_TEXTURE_SIZE,
                  FR_CONTEXT_INIT_DISABLE_COLOR_BUFFER);

    settings.shadowProjectionView = &shadowProjectionView;
    settings.shadowDepthBuffer = &shadowsContext.zBuffer;

    shadowsContext.cullingMode = frCullingModeFront;

    camera = axisCamera;

    settings.globalLightVector =
        FR_VEC3_NEG(frVector3Normalize(frVector3DirectionByAngle(lightYRot, 
                                                                 lightZRot)));
    skybox = new StarsSkybox(camera);
    origin->setVisible(false);
    landscape = new Landscape(128, 128, 1, 0, 0.5, 0.01);
    aggregator = new TreeObjectAggregator(landscape);
    axisCameraCenter->setVisible(false);
    axisCameraCenter->setShadowCaster(false);

    drawables.push_back(skybox);
    drawables.push_back(new Sun());
    drawables.push_back(origin);
    drawables.push_back(axisCameraCenter);
    drawables.push_back(landscape);
    drawables.push_back(aggregator);
}

RenderAreaFrameGenerator::~RenderAreaFrameGenerator()
{
    frContextDestroy(&context);
    frContextDestroy(&shadowsContext);

    delete camera;

    for(Drawable* drawable : drawables)
        delete drawable;
}

Frame RenderAreaFrameGenerator::getCurrentFrame()
{
    switch(outputFrameType)
    {
    case OutputFrameType::Frame:
        return (Frame)context.frame.data;
    case OutputFrameType::ZBuffer:
        return (Frame)context.zBuffer.data;
    case OutputFrameType::ShadowMap:
        return (Frame)shadowsContext.zBuffer.data;
    }

    fatalError("%s", "getCurrentFrame(): unknown outputFrameType");
}

size_t RenderAreaFrameGenerator::getCurrentFrameWidth()
{
    switch(outputFrameType)
    {
    case OutputFrameType::Frame:
    case OutputFrameType::ZBuffer:
        return context.width;
    case OutputFrameType::ShadowMap:
        return shadowsContext.width;
    }

    fatalError("%s", "getCurrentFrameWidth(): unknown outputFrameType");
}

size_t RenderAreaFrameGenerator::getCurrentFrameHeight()
{
    switch(outputFrameType)
    {
    case OutputFrameType::Frame:
    case OutputFrameType::ZBuffer:
        return context.height;
    case OutputFrameType::ShadowMap:
        return shadowsContext.height;
    }

    fatalError("%s", "getCurrentFrameHeight(): unknown outputFrameType");
}

Camera* RenderAreaFrameGenerator::getCamera()
{
    return camera;
}

/* rendering tab */

void RenderAreaFrameGenerator::setProjectionMatrixType(bool perspective)
{
    perspectiveUsed = perspective;

    skybox->setVisible(perspective);
}

bool RenderAreaFrameGenerator::getProjectionMatrixType()
{
    return perspectiveUsed;
}

void RenderAreaFrameGenerator::setFov(double value)
{
    fov = value;
}

double RenderAreaFrameGenerator::getFov()
{
    return fov;
}

void RenderAreaFrameGenerator::setTexturingEnabled(bool value)
{
    settings.texturingEnabled = value;
}

bool RenderAreaFrameGenerator::getTexturingEnabled()
{
    return settings.texturingEnabled;
}

void RenderAreaFrameGenerator::setBilinearFilteringEnabled(bool value)
{
    settings.bilinearFilteringEnabled = value;
}

bool RenderAreaFrameGenerator::getBilinearFilteringEnabled()
{
    return settings.bilinearFilteringEnabled;
}

void RenderAreaFrameGenerator::setIlluminationEnabled(bool value)
{
    settings.illuminationEnabled = value;
}

bool RenderAreaFrameGenerator::getIlluminationEnabled()
{
    return settings.illuminationEnabled;
}

void RenderAreaFrameGenerator::setShadowsEnabled(bool value)
{
    settings.shadowsEnabled = value;
}

bool RenderAreaFrameGenerator::getShadowsEnabled()
{
    return settings.shadowsEnabled;
}

void RenderAreaFrameGenerator::setLightSourceYRot(double value)
{
    lightYRot = value;

    settings.globalLightVector =
        FR_VEC3_NEG(frVector3Normalize(frVector3DirectionByAngle(lightYRot,
                                                                 lightZRot)));

    forceShadowsRecalculation = true;
}

double RenderAreaFrameGenerator::getLightSourceYRot()
{
    return lightYRot;
}

void RenderAreaFrameGenerator::setLightSourceZRot(double value)
{
    lightZRot = value;

    settings.globalLightVector =
        FR_VEC3_NEG(frVector3Normalize(frVector3DirectionByAngle(lightYRot,
                                                                 lightZRot)));

    forceShadowsRecalculation = true;
}

double RenderAreaFrameGenerator::getLightSourceZRot()
{
    return lightZRot;
}

void RenderAreaFrameGenerator::setShadowsTextureSize(size_t size)
{
    shadowsTextureSize = size;

    forceShadowsRecalculation = true;
}

size_t RenderAreaFrameGenerator::getShadowsTextureSize()
{
    return shadowsTextureSize;
}

void RenderAreaFrameGenerator::setCullingMode(frCullingMode mode)
{
    context.cullingMode = mode;
}

frCullingMode RenderAreaFrameGenerator::getCullingMode()
{
    return context.cullingMode;
}

void RenderAreaFrameGenerator::setClippingMode(frClippingMode mode)
{
    context.clippingMode = mode;
}

frClippingMode RenderAreaFrameGenerator::getClippingMode()
{
    return context.clippingMode;
}

void RenderAreaFrameGenerator::setAutomaticNearFarPlanes(bool value)
{
    automaticNearFarPlanes = value;
}

bool RenderAreaFrameGenerator::getAutomaticNearFarPlanes()
{
    return automaticNearFarPlanes;
}

void RenderAreaFrameGenerator::setForcedNearPlane(double value)
{
    forcedNear = value;
}

double RenderAreaFrameGenerator::getForcedNearPlane()
{
    return forcedNear;
}

void RenderAreaFrameGenerator::setForcedFarPlane(double value)
{
    forcedFar = value;
}

double RenderAreaFrameGenerator::getForcedFarPlane()
{
    return forcedFar;
}

double RenderAreaFrameGenerator::getNearPlane()
{
    return settings.nearPlane;
}

double RenderAreaFrameGenerator::getFarPlane()
{
    return settings.farPlane;
}

void RenderAreaFrameGenerator::setZBufferEnabled(bool value)
{
    context.zBufferEnabled = value;
}

bool RenderAreaFrameGenerator::getZBufferEnabled()
{
    return context.zBufferEnabled;
}

void RenderAreaFrameGenerator::setDrawOrigin(bool value)
{
    origin->setVisible(value);
}

bool RenderAreaFrameGenerator::getDrawOrigin()
{
    return origin->isVisible();
}

void RenderAreaFrameGenerator::setDrawAxisCameraCenter(bool value)
{
    axisCameraCenter->setVisible(value);
}

bool RenderAreaFrameGenerator::getDrawAxisCameraCenter()
{
    return axisCameraCenter->isVisible();
}

void RenderAreaFrameGenerator::setAxisCameraCenterCastsShadow(bool value)
{
    axisCameraCenter->setShadowCaster(value);

    forceShadowsRecalculation = true;
}

bool RenderAreaFrameGenerator::getAxisCameraCenterCastsShadow()
{
    return axisCameraCenter->isShadowCaster();
}

void RenderAreaFrameGenerator::setWireframeEnabled(bool value)
{
    context.wireframeMode = value;
}

bool RenderAreaFrameGenerator::getWireframeEnabled()
{
    return context.wireframeMode;
}

void RenderAreaFrameGenerator::setOutputFrameType(OutputFrameType type)
{
    outputFrameType = type;
}

OutputFrameType RenderAreaFrameGenerator::getOutputFrameType()
{
    return outputFrameType;
}

unsigned RenderAreaFrameGenerator::getPolygonCount()
{
    unsigned polygonCount = 0;

    for(Drawable* drawable : drawables)
        polygonCount += drawable->getPolygonCount();

    return polygonCount;
}

Landscape* RenderAreaFrameGenerator::getLandscape()
{
    return landscape;
}

void RenderAreaFrameGenerator::setTreeObjectsReference(
    std::vector<TreeObject>* reference)
{
    aggregator->setTreeObjectsReference(reference);
}

/* private */

/*!
 * Return projection matrix created according to arguments passed.
 * @param fov Field of view.
 * @param aspectRatio Width of the frame divided by height.
 * @param nearPlane Near plane of viewing volume.
 * @param farPlane Far plane of viewing volume.
 * @param perspectiveUsed Determines whenether the matrix is perspective
 * or orthographic.
 * @return
 */
static frMatrix4x4 getProjectionMatrix(double fov,
                                       double aspectRatio,
                                       double nearPlane,
                                       double farPlane,
                                       bool perspectiveUsed)
{
    if(perspectiveUsed)
    {
        return frMatrixCreatePerspective(fov,
                                         aspectRatio,
                                         nearPlane,
                                         farPlane);
    }
    else
    {
        return frMatrixCreateOrthoCorrected(fov,
                                            aspectRatio,
                                            nearPlane,
                                            farPlane);
    }
}

/* rendering */

/*!
 * Calculate far plane by inspecting drawables' bounding figures.
 * @param drawables An array of drawables.
 * @param cameraPosition Position of the camera.
 * @param yRot Rotation of the camera around y.
 * @param zRot Rotation of the camera around z.
 * @return Far plane calculated.
 */
static double calculateFarPlane(const std::vector<Drawable*>& drawables,
                                frVector4 cameraPosition,
                                double yRot,
                                double zRot)
{
#define CONVERT_MODEL(x) (frMatrixApply(&model, (x)))
    frMatrix4x4 model =
        frMatricesMultiply(frMatrixCreateRotationX(zRot),
                           frMatrixCreateRotationY(yRot - FR_PI / 2),
                           frMatrixCreateTranslationVec(
                               FR_VEC3_NEG(cameraPosition)));

    BoundingFigure* figure;
    BoundingAABox* box;
    BoundingSphere sphere;

    frVector4 NNN, NNP, NPN, NPP, PNN, PNP, PPN, PPP;

    double farPlane = 0;

    for(Drawable* drawable : drawables)
    {
        if(!drawable->isVisible())
            continue;

        figure = drawable->getBoundingFigure();
        if(!figure)
            continue;

        if((box = dynamic_cast<BoundingAABox*>(figure)) != nullptr)
        {
            NNN = CONVERT_MODEL(box->getNNNCorner());
            NNP = CONVERT_MODEL(box->getNNPCorner());
            NPN = CONVERT_MODEL(box->getNPNCorner());
            NPP = CONVERT_MODEL(box->getNPPCorner());
            PNN = CONVERT_MODEL(box->getPNNCorner());
            PNP = CONVERT_MODEL(box->getPNPCorner());
            PPN = CONVERT_MODEL(box->getPPNCorner());
            PPP = CONVERT_MODEL(box->getPPPCorner());

            farPlane = -std::min({-farPlane, NNN.z, NNP.z, NPN.z, NPP.z,
                                             PNN.z, PNP.z, PPN.z, PPP.z});
        }
        else
        {
            sphere = figure->convertToSphere();

            farPlane =
                std::max(farPlane,
                         std::max(-CONVERT_MODEL(sphere.getPosition()).z
                                      + sphere.getRadius(),
                                  0.0));
        }
    }

    return farPlane + FR_TINY;
#undef CONVERT_MODEL
}

/*!
 * Calculate orthographic matrix to be used when rendering shadow map.
 * @param drawables An array of drawables.
 * @param lightYRot Rotation of light source around Y.
 * @param lightZRot Rotation of light source around Z.
 * @return Orthographic matrix.
 */
static frMatrix4x4 calculateShadowOrthoBox(
    const std::vector<Drawable*>& drawables,
    double lightYRot,
    double lightZRot)
{
    frMatrix4x4 model =
        frMatricesMultiply(frMatrixCreateRotationX(lightZRot),
                           frMatrixCreateRotationY(lightYRot - FR_PI / 2));

    BoundingAABox box;

    size_t i = 0;

    while(!(drawables[i]->isVisible() && drawables[i]->isShadowCaster()
            && drawables[i]->getBoundingFigure() != nullptr))
    {
        i++;
    }

    BoundingFigure* figure = drawables[i]->getBoundingFigure();

    BoundingAABox* boxPtr;
    BoundingSphere sphere(FR_VEC3_ZERO, 0);

    if((boxPtr = dynamic_cast<BoundingAABox*>(figure)) != nullptr)
        box = boxPtr->getTransformed(model);
    else
        box = BoundingAABox(figure->convertToSphere().getTransformed(model));

    for(i = i + 1; i < drawables.size(); i++)
    {
        if(drawables[i]->isVisible() && drawables[i]->isShadowCaster() &&
           (figure = drawables[i]->getBoundingFigure()) != nullptr)
        {
            if((boxPtr = dynamic_cast<BoundingAABox*>(figure)) != nullptr)
                box.expandBy(boxPtr->getTransformed(model));
            else
                box.expandBy(figure->convertToSphere().getTransformed(model));
        }
    }

    box.expandTiny();

    frMatrix4x4 projection = frMatrixCreateOrtho(box.left(), box.right(),
                                                 box.bottom(), box.top(),
                                                 -box.far(), -box.near());

    return frMatrixMultiply(projection, model);
}

bool RenderAreaFrameGenerator::generateNewFrame(size_t width, size_t height)
{
    frMatrix4x4 view, projection;

    for(Drawable* drawable : drawables)
        if(drawable->needsToBeRedrawn())
        {
            forceShadowsRecalculation = true;
            break;
        }

    if(settings.shadowsEnabled &&
       outputFrameType != OutputFrameType::ZBuffer &&
       !context.wireframeMode &&
           (forceShadowsRecalculation ||
            outputFrameType == OutputFrameType::ShadowMap))
    {
        if(!frStartNewFrame(&shadowsContext,
                            shadowsTextureSize,
                            shadowsTextureSize))
        {
            return false;
        }

        shadowProjectionView = calculateShadowOrthoBox(drawables,
                                                       lightYRot,
                                                       lightZRot);

        settings.projectionView = &shadowProjectionView;

        for(Drawable* drawable : drawables)
        {
            if(drawable->isVisible() && drawable->isShadowCaster())
                drawable->draw(&shadowsContext, settings);
        }

        if(outputFrameType == OutputFrameType::ShadowMap)
        {
            frReformatDepthBuffer(&shadowsContext.zBuffer);
            return true;
        }
        else
            forceShadowsRecalculation = false;

        shadowProjectionView =
            frMatricesMultiply(frMatrixCreateScale(shadowsTextureSize - 1,
                                                   shadowsTextureSize - 1,
                                                   1),
                               SHADOW_MAPPING_BIAS_MATRIX,
                               shadowProjectionView);
    }

    if(context.zBufferEnabled && outputFrameType == OutputFrameType::ZBuffer)
        context.colorBufferEnabled = false;

    if(!frStartNewFrame(&context, width, height))
        return false;

    if(automaticNearFarPlanes)
    {
        settings.nearPlane = DEFAULT_NEAR_PLANE;
        settings.farPlane = calculateFarPlane(drawables,
                                              camera->getPosition(),
                                              camera->getYRotation(),
                                              camera->getZRotation());
        settings.farPlane =
            clampValue(settings.farPlane,
                       settings.nearPlane + MIN_NEAR_FAR_DISTANCE,
                       MAX_FAR);
    }
    else
    {
        settings.nearPlane = forcedNear;
        settings.farPlane = forcedFar;
    }

    view = camera->getViewMatrix();
    projection = getProjectionMatrix(fov,
                                     width / (double)height,
                                     settings.nearPlane,
                                     settings.farPlane,
                                     perspectiveUsed);

    projectionView = frMatrixMultiply(projection, view);

    settings.projectionView = &projectionView;

    for(Drawable* drawable : drawables)
    {
        if(drawable->isVisible())
            drawable->draw(&context, settings);
    }

    if(context.zBufferEnabled && outputFrameType == OutputFrameType::ZBuffer)
    {
        context.colorBufferEnabled = true;
        frReformatDepthBuffer(&context.zBuffer);
    }

    return true;
}
