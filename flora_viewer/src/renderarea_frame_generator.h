#ifndef FLORAINC_FLORA_VIEWER_SRC_RENDERAREA_FRAME_GENERATOR_H
#define FLORAINC_FLORA_VIEWER_SRC_RENDERAREA_FRAME_GENERATOR_H

#include <vector>

#include <flora_renderer.h>
#include <src/drawables/tree_object_aggregate.h>

#include "cameras/camera.h"
#include "cameras/axis_camera.h"
#include "drawables/landscape.h"
#include "drawables/stars_skybox.h"
#include "drawables/sun.h"
#include "drawables/origin.h"
#include "drawables/axis_camera_center.h"

#include "frame_generator_settings.h"

/*! Wrapper of unsigned char* as it is format used by flora_renderer. */
using Frame = unsigned char*;

/*! Determines what type of frame RenderAreaFrameGenerator returns. */
enum class OutputFrameType
{
    Frame,
    ZBuffer,
    ShadowMap
};

/*! A convenience class which is used to set up rendering and
 * generate new frames to be displayed by RenderArea. */
class RenderAreaFrameGenerator
{
public:
    /*! Maximum expected width of the frame. Does not impose any restrictions
     * on actual frame size. */
    const size_t MAX_EXPECTED_WIDTH = 1920;
    /*! Maximum expected width of the frame. Does not impose any restrictions
     * on actual frame size. */
    const size_t MAX_EXPECTED_HEIGHT = 1080;

    /*! Max shadow texture size. */
    const size_t MAX_SHADOW_TEXTURE_SIZE = 4096;

    /*! Default value of near plane. */
    const double DEFAULT_NEAR_PLANE = 0.1;

    constexpr static double MIN_NEAR = 0.01;
    constexpr static double MAX_FAR = 10000000;
    constexpr static double MIN_NEAR_FAR_DISTANCE = 0.0001;

    /*! Default field of view in degrees. */
    const double DEFAULT_FOV = 60;

    /*!
     * Constructor of RenderAreaFrameGenerator.
     * @param renderArea Widget that is used as output surface.
     */
    explicit RenderAreaFrameGenerator(QWidget* renderArea);
    ~RenderAreaFrameGenerator();

    /*!
     * Return current frame. Frame returned depends on output frame type set.
     * @return Return current frame.
     */
    Frame getCurrentFrame();
    /*!
     * Return current frame's width.
     * @return Width of the frame.
     */
    size_t getCurrentFrameWidth();
    /*!
     * Return current frame's height.
     * @return Height of the frame.
     */
    size_t getCurrentFrameHeight();

    /*! Return current camera associated. */
    Camera* getCamera();

    /* rendering tab */

    void setProjectionMatrixType(bool perspective);
    bool getProjectionMatrixType();

    void setFov(double);
    double getFov();

    void setTexturingEnabled(bool);
    bool getTexturingEnabled();
    void setBilinearFilteringEnabled(bool);
    bool getBilinearFilteringEnabled();

    void setIlluminationEnabled(bool);
    bool getIlluminationEnabled();
    void setShadowsEnabled(bool);
    bool getShadowsEnabled();

    void setLightSourceYRot(double);
    double getLightSourceYRot();
    void setLightSourceZRot(double);
    double getLightSourceZRot();

    void setShadowsTextureSize(size_t);
    size_t getShadowsTextureSize();

    void setCullingMode(frCullingMode);
    frCullingMode getCullingMode();

    void setClippingMode(frClippingMode);
    frClippingMode getClippingMode();

    void setAutomaticNearFarPlanes(bool);
    bool getAutomaticNearFarPlanes();
    void setForcedNearPlane(double value);
    double getForcedNearPlane();
    void setForcedFarPlane(double value);
    double getForcedFarPlane();
    double getNearPlane();
    double getFarPlane();

    void setZBufferEnabled(bool);
    bool getZBufferEnabled();

    void setDrawOrigin(bool);
    bool getDrawOrigin();
    void setDrawAxisCameraCenter(bool);
    bool getDrawAxisCameraCenter();
    void setAxisCameraCenterCastsShadow(bool);
    bool getAxisCameraCenterCastsShadow();

    void setWireframeEnabled(bool);
    bool getWireframeEnabled();

    /*! Determine which frame is returned by getCurrentFrame(). */
    void setOutputFrameType(OutputFrameType);
    /*! Return type of frame returned by getCurrentFrame(). */
    OutputFrameType getOutputFrameType();

    unsigned getPolygonCount();

    Landscape* getLandscape();

    /*! Set reference to externally mananged collection of tree objects. */
    void setTreeObjectsReference(std::vector<TreeObject>* reference);

    /*!
     * Generate a new frame according to parameters set.
     * @param width Width of a new frame.
     * @param height Height of a new frame.
     * @return Successfulness of the operation.
     */
    bool generateNewFrame(size_t width, size_t height);
private:
    static const frMatrix4x4 SHADOW_MAPPING_BIAS_MATRIX;

    std::vector<Drawable*> drawables;

    FrameGeneratorSettings settings;

    frContext context;
    frContext shadowsContext;
    Camera* camera;

    bool forceShadowsRecalculation;

    double fov;
    double lightYRot, lightZRot;

    double forcedNear, forcedFar;

    size_t shadowsTextureSize;

    bool perspectiveUsed;

    bool automaticNearFarPlanes;

    OutputFrameType outputFrameType;

    frMatrix4x4 projectionView;
    frMatrix4x4 shadowProjectionView;

    /* objects */

    StarsSkybox* skybox;
    Origin* origin;

    AxisCamera* axisCamera;
    AxisCameraCenter* axisCameraCenter;

    Landscape* landscape;

    TreeObjectAggregator* aggregator;
};

#endif
