#ifndef FLORAINC_FLORA_VIEWER_SRC_MAINWINDOW_H
#define FLORAINC_FLORA_VIEWER_SRC_MAINWINDOW_H

#include <flora_generator.h>

#include <QMainWindow>
#include <QModelIndex>
#include <QStandardItemModel>

#include "src/drawables/tree_object.h"
#include "renderarea.h"

namespace Ui {
class MainWindow;
}

/*!
 * Main window of the application. Container to RenderArea.
 * Has a lot of event handlers (slots) which are connected to
 * corresponding events. Names of event handlers are defined to be as such:
 * on[Object][Event](...).
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    Ui::MainWindow *ui;

    RenderArea *renderArea = nullptr;

    bool changingNearFar;

    std::vector<TreeObject> treeObjects;
    std::vector<FG::LSystem> lsystems;

    /* utility */
    void switchToTreeObject(int index);
    void switchToLSystem(int index);

    bool loadLSystemFromFile(const QString& string);

    void regenerateTreeObject();
    void updateInterpretationString();
    void updateTreeObjectPolygonCountString();

    TreeObject& getCurrentTreeObject();
    FG::LSystem& getTreeObjectLSystem(int index);
    FG::LSystem& getLSystem(const std::string& s);
    FG::LSystem& getLSystem(const QString& s);

    int oldIndex;

    bool ignoreEvents;
public slots:
    /* flora tab */

    void onTreeObjectComboBoxActivated(int);

    void onDeleteTreeObjectButtonClicked();
    void onAddTreeObjectButtonClicked();
    void onTreeObjectLSystemComboBoxActivated(int);

    void onIterationsCountSpinBoxValueChanged(int);
    void onSeedSpinBoxValueChanged(int);

    void onTreeObjectXSpinBoxValueChanged(double);
    void onTreeObjectYSpinBoxValueChanged(double);
    void onTreeObjectAngleSpinBoxValueChanged(double);

    void onInterpretButtonClicked();

    void onShowCommandsCheckBoxClicked(bool);

    void onLSystemComboBoxActivated(int);

    void onDeleteLSystemButtonClicked();
    void onAddLSystemButtonClicked();
    void onLoadLSystemButtonClicked();

    void onSaveLSystemButtonClicked();
    void onBuildLSystemButtonClicked();

    /* rendering tab */

    void onPerspectiveProjectionCheckBoxClicked(bool);

    void onFovSliderValueChanged(int);

    void onTexturingEnabledCheckBoxClicked(bool);

    void onBilinearFilteringEnabledCheckBoxClicked(bool);

    void onIlluminationCheckBoxClicked(bool);
    void onShadowsEnabledCheckBoxClicked(bool);

    void onLightSourceYRotSliderValueChanged(int);
    void onLightSourceZRotSliderValueChanged(int);

    void onShadowTextureSizeSpinBoxValueChanged(int);

    void onShowShadowMapCheckBoxClicked(bool);

    void onCullingBackRadioButtonClicked();
    void onCullingFrontRadioButtonClicked();
    void onCullingOffRadioButtonClicked();

    void onClippingNearRadioButtonClicked();
    void onClippingNearFarRadioButtonClicked();
    void onClippingAllRadioButtonClicked();

    void onAutomaticallyCaclulateFarNearCheckBoxClicked(bool);
    void onNearPlaneSpinBoxValueChanged(double);
    void onFarPlaneSpinBoxValueChanged(double);

    void onZBufferEnabledCheckBoxClicked(bool);
    void onShowZBufferCheckBoxClicked(bool);

    void onDrawOriginCheckBoxClicked(bool);
    void onDrawAxisCameraOriginCheckBoxClicked(bool);
    void onAxisCameraCenterCastsShadowCheckBoxClicked(bool);

    void onWireframeEnabledCheckBoxClicked(bool);

    /* misc tab */

    void onSaveCurrentFrameButtonClicked();

    /* render button area */

    void onAutomaticRecalculationCheckBoxClicked(bool);
    void onRenderButtonClicked();

    /* renderarea */

    /*!
     * Need to be activated when a new frame is finished to update
     * information interface elements.
     */
    void onRenderAreaFrameFinished();
};

#endif
