#ifndef FLORAINC_FLORA_VIEWER_SRC_UTIL_H
#define FLORAINC_FLORA_VIEWER_SRC_UTIL_H

#include <algorithm>

/*!
 * Clamp value between low and high (using inclusive range);
 * @tparam T Passed type.
 * @param value Value to be clamped.
 * @param low Start of inclusive range.
 * @param high End of inclusive range.
 * @return Clamped value.
 */
template<typename T>
T clampValue(T value, T low, T high)
{
    return std::min(std::max(value, low), high);
}

#endif
