#ifndef FLORAINC_FLORA_VIEWER_SRC_FRAME_GENERATOR_SETTINGS_H
#define FLORAINC_FLORA_VIEWER_SRC_FRAME_GENERATOR_SETTINGS_H

#include <flora_renderer.h>

class RenderAreaFrameGenerator;

/*!
 * A set of settings which determine how rendering of object should be
 * performed.
 */
struct FrameGeneratorSettings
{
    /*! Direction of global light vector (sun). */
    frVector4 globalLightVector;
    /*! projectionView matrix used for current rendering pass. */
    frMatrix4x4* projectionView;
    /*! projectionView matrix used when rendering shadow map. */
    frMatrix4x4* shadowProjectionView;

    /*! Shadow map for the scene. */
    frBuffer2d* shadowDepthBuffer;

    /*! Pointer to RenderAreaFrameGenerator for querying additional info. */
    RenderAreaFrameGenerator* generator;

    /* Near and far plane in current rendering pass. */
    double nearPlane, farPlane;

    /*! Determines whenether illumination calculation is requested. */
    bool illuminationEnabled;
    bool shadowsEnabled; /*!< Determines whenether shadows are requested. */
    bool texturingEnabled; /*!< Determines whenether texturing is requested. */
    /*! Determines if bilinear filtering is requested. */
    bool bilinearFilteringEnabled;
};

#endif
