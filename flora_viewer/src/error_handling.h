#ifndef FLORAINC_FLORA_VIEWER_SRC_ERROR_HANDLING_H
#define FLORAINC_FLORA_VIEWER_SRC_ERROR_HANDLING_H

/*!
 * Print formatted message to stderr and exit program.
 */
[[noreturn]] void fatalError(const char* format, ...);

#endif
