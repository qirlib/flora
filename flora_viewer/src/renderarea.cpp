#include "renderarea.h"

#include <QPainter>

RenderArea::RenderArea(QWidget *parent)
    : QWidget(parent),
      generator(this),
      automaticallyRedrawFrame(true),
      haveValidFrame(false)
{
    this->setMouseTracking(true);
    this->setFocusPolicy(Qt::ClickFocus);
}

void RenderArea::setAutomaticRedraw(bool value)
{
    automaticallyRedrawFrame = value;
}

bool RenderArea::isAutomaticallyRedrawn()
{
    return automaticallyRedrawFrame;
}

RenderAreaFrameGenerator& RenderArea::getGenerator()
{
    return generator;
}

void RenderArea::forceRegeneration()
{
    haveValidFrame = generator.generateNewFrame(width(), height());
}

bool RenderArea::hasValidFrame()
{
    return haveValidFrame;
}

const QImage& RenderArea::getCurrentFrame()
{
    return currentFrame;
}

void RenderArea::resizeEvent(QResizeEvent *)
{
    this->update();
}

void RenderArea::mouseMoveEvent(QMouseEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processMouseMoveEvent(event))
    {
        this->update();
    }
}

void RenderArea::mousePressEvent(QMouseEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processMousePressEvent(event))
    {
        this->update();
    }
}

void RenderArea::mouseReleaseEvent(QMouseEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processMouseReleaseEvent(event))
    {
        this->update();
    }
}

void RenderArea::mouseDoubleClickEvent(QMouseEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processMouseDoubleClickEvent(event))
    {
        this->update();
    }
}

void RenderArea::wheelEvent(QWheelEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processWheelEvent(event))
    {
        this->update();
    }
}

void RenderArea::keyPressEvent(QKeyEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processKeyPressEvent(event))
    {
        this->update();
    }
}

void RenderArea::keyReleaseEvent(QKeyEvent* event)
{
    if(automaticallyRedrawFrame &&
       generator.getCamera()->processKeyReleaseEvent(event))
    {
        this->update();
    }
}

void RenderArea::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    auto width = (size_t)this->width(), height = (size_t)this->height();

    if(width == 0 || height == 0)
        return;

    if(automaticallyRedrawFrame)
    {
        haveValidFrame = generator.generateNewFrame(width, height);
    }

    if(haveValidFrame)
    {
        painter.fillRect(0, 0,
                         static_cast<int>(width), static_cast<int>(height),
                         QColor::fromRgb(0, 0, 127));

        currentFrame =
            QImage(generator.getCurrentFrame(),
                   static_cast<int>(generator.getCurrentFrameWidth()),
                   static_cast<int>(generator.getCurrentFrameHeight()),
                   QImage::Format_ARGB32);

        painter.drawImage(0, 0, currentFrame);

        emit onFrameFinished();
    }
    else
    {
        painter.fillRect(0, 0,
                         (int)width, (int)height,
                         QColor::fromRgb(255, 0, 0));
    }
}
