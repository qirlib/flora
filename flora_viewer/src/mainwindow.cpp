#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <cmath>

#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

#include "util.h"

static const QString STARTUP_MESSAGE =
"Программа может загрузить стандартную демонстрационную сцену, "
"состоящую из нескольких объектов растительности. "
"Если у вас имеются проблемы с производительностью, "
"вы можете отказаться от её загрузки и настраивать сцену самостоятельно. "
"Если вы всё ещё испытываете проблемы "
"с производительностью, воспользуйтесь режимом каркасного отображения "
"(вкладка \"Отрисовка\").\n"
"Загрузить стандартную сцену?";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    changingNearFar(false),
    oldIndex(-1),
    ignoreEvents(false)
{
    ui->setupUi(this);

    int result =
        QMessageBox::question(nullptr,
                              "Предупреждение о производительности",
                              STARTUP_MESSAGE,
                              QMessageBox::Yes, QMessageBox::No);

    renderArea = new RenderArea(this);

    ui->mainHL->insertWidget(0, renderArea);

    RenderAreaFrameGenerator& gen = renderArea->getGenerator();

    /* flora tab */

    connect(ui->treeObjectComboBox,
            SIGNAL(activated(int)),
            this,
            SLOT(onTreeObjectComboBoxActivated(int)));

    connect(ui->deleteTreeObjectButton,
            SIGNAL(clicked()),
            this,
            SLOT(onDeleteTreeObjectButtonClicked()));

    connect(ui->addTreeObjectButton,
            SIGNAL(clicked()),
            this,
            SLOT(onAddTreeObjectButtonClicked()));

    connect(ui->treeObjectLSystemComboBox,
            SIGNAL(activated(int)),
            this,
            SLOT(onTreeObjectLSystemComboBoxActivated(int)));

    connect(ui->iterationsCountSpinBox,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onIterationsCountSpinBoxValueChanged(int)));

    connect(ui->seedSpinBox,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onSeedSpinBoxValueChanged(int)));

    Landscape* landscape = gen.getLandscape();
    ui->treeObjectXSpinBox->setMinimum(landscape->getMinX());
    ui->treeObjectXSpinBox->setMaximum(landscape->getMaxX());
    connect(ui->treeObjectXSpinBox,
            SIGNAL(valueChanged(double)),
            this,
            SLOT(onTreeObjectXSpinBoxValueChanged(double)));

    ui->treeObjectYSpinBox->setMinimum(landscape->getMinZ());
    ui->treeObjectYSpinBox->setMaximum(landscape->getMaxZ());
    connect(ui->treeObjectYSpinBox,
            SIGNAL(valueChanged(double)),
            this,
            SLOT(onTreeObjectYSpinBoxValueChanged(double)));

    connect(ui->treeObjectAngleSpinBox,
            SIGNAL(valueChanged(double)),
            this,
            SLOT(onTreeObjectAngleSpinBoxValueChanged(double)));

    connect(ui->interpretButton,
            SIGNAL(clicked()),
            this,
            SLOT(onInterpretButtonClicked()));

    connect(ui->showCommandsCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onShowCommandsCheckBoxClicked(bool)));

    connect(ui->lSystemComboBox,
            SIGNAL(activated(int)),
            this,
            SLOT(onLSystemComboBoxActivated(int)));

    connect(ui->deleteLSystemButton,
            SIGNAL(clicked()),
            this,
            SLOT(onDeleteLSystemButtonClicked()));

    connect(ui->addLSystemButton,
            SIGNAL(clicked()),
            this,
            SLOT(onAddLSystemButtonClicked()));

    connect(ui->loadLSystemButton,
            SIGNAL(clicked()),
            this,
            SLOT(onLoadLSystemButtonClicked()));

    connect(ui->saveLSystemButton,
            SIGNAL(clicked()),
            this,
            SLOT(onSaveLSystemButtonClicked()));

    connect(ui->buildLSystemButton,
            SIGNAL(clicked()),
            this,
            SLOT(onBuildLSystemButtonClicked()));

    /* rendering tab */

    ui->perspectiveProjectionCheckBox->setChecked(
        gen.getProjectionMatrixType());
    connect(ui->perspectiveProjectionCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onPerspectiveProjectionCheckBoxClicked(bool)));

    ui->fovSlider->setValue(gen.getFov());
    connect(ui->fovSlider,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onFovSliderValueChanged(int)));

    ui->texturingEnabledCheckBox->setChecked(gen.getTexturingEnabled());
    connect(ui->texturingEnabledCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onTexturingEnabledCheckBoxClicked(bool)));

    ui->bilinearFilteringEnabledCheckBox->setChecked(
        gen.getBilinearFilteringEnabled());
    connect(ui->bilinearFilteringEnabledCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onBilinearFilteringEnabledCheckBoxClicked(bool)));

    ui->illuminationCheckBox->setChecked(gen.getIlluminationEnabled());
    connect(ui->illuminationCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onIlluminationCheckBoxClicked(bool)));

    ui->shadowsEnabledCheckBox->setChecked(gen.getShadowsEnabled());
    connect(ui->shadowsEnabledCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onShadowsEnabledCheckBoxClicked(bool)));

    ui->lightSourceYRotSlider->setValue(gen.getLightSourceYRot() * 180 / FR_PI);
    connect(ui->lightSourceYRotSlider,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onLightSourceYRotSliderValueChanged(int)));

    ui->lightSourceZRotSlider->setValue(gen.getLightSourceZRot() * 180 / FR_PI);
    connect(ui->lightSourceZRotSlider,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onLightSourceZRotSliderValueChanged(int)));

    ui->shadowsTextureSizeSpinBox->setValue(gen.getShadowsTextureSize());
    connect(ui->shadowsTextureSizeSpinBox,
            SIGNAL(valueChanged(int)),
            this,
            SLOT(onShadowTextureSizeSpinBoxValueChanged(int)));

    ui->showShadowMapCheckBox->setChecked(
        gen.getOutputFrameType() == OutputFrameType::ShadowMap);
    ui->showShadowMapCheckBox->setEnabled(gen.getShadowsEnabled());
    connect(ui->showShadowMapCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onShowShadowMapCheckBoxClicked(bool)));

    switch(gen.getCullingMode())
    {
    case frCullingModeBack:
        ui->cullingBackRadioButton->setChecked(true);
        break;
    case frCullingModeFront:
        ui->cullingFrontRadioButton->setChecked(true);
        break;
    case frCullingModeNone:
        ui->cullingOffRadioButton->setChecked(true);
        break;
    }

    connect(ui->cullingBackRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onCullingBackRadioButtonClicked()));

    connect(ui->cullingFrontRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onCullingFrontRadioButtonClicked()));

    connect(ui->cullingOffRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onCullingOffRadioButtonClicked()));

    switch(gen.getClippingMode())
    {
    case frClippingModeNear:
        ui->clippingNearRadioButton->setChecked(true);
        break;
    case frClippingModeNearFar:
        ui->clippingNearFarRadioButton->setChecked(true);
        break;
    case frClippingModeAll:
        ui->clippingAllRadioButton->setChecked(true);
        break;
    }

    connect(ui->clippingNearRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onClippingNearRadioButtonClicked()));

    connect(ui->clippingNearFarRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onClippingNearFarRadioButtonClicked()));

    connect(ui->clippingAllRadioButton,
            SIGNAL(clicked()),
            this,
            SLOT(onClippingAllRadioButtonClicked()));

    ui->automaticallyCalculateFarNearCheckBox->setChecked(
        gen.getAutomaticNearFarPlanes());
    connect(ui->automaticallyCalculateFarNearCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onAutomaticallyCaclulateFarNearCheckBoxClicked(bool)));

    if(!gen.getAutomaticNearFarPlanes())
        ui->nearPlaneSpinBox->setValue(gen.getNearPlane());
    connect(ui->nearPlaneSpinBox,
            SIGNAL(valueChanged(double)),
            this,
            SLOT(onNearPlaneSpinBoxValueChanged(double)));

    if(!gen.getAutomaticNearFarPlanes())
        ui->farPlaneSpinBox->setValue(gen.getFarPlane());
    connect(ui->farPlaneSpinBox,
            SIGNAL(valueChanged(double)),
            this,
            SLOT(onFarPlaneSpinBoxValueChanged(double)));

    ui->zBufferEnabledCheckBox->setChecked(gen.getZBufferEnabled());
    connect(ui->zBufferEnabledCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onZBufferEnabledCheckBoxClicked(bool)));

    ui->showZBufferCheckBox->setChecked(
        gen.getOutputFrameType() == OutputFrameType::ZBuffer);
    connect(ui->showZBufferCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onShowZBufferCheckBoxClicked(bool)));

    ui->drawOriginCheckBox->setChecked(gen.getDrawOrigin());
    connect(ui->drawOriginCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onDrawOriginCheckBoxClicked(bool)));

    ui->drawAxisCameraOriginCheckBox->setChecked(gen.getDrawAxisCameraCenter());
    connect(ui->drawAxisCameraOriginCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onDrawAxisCameraOriginCheckBoxClicked(bool)));

    ui->axisCameraCenterCastsShadowCheckBox->setChecked(
        gen.getAxisCameraCenterCastsShadow());
    connect(ui->axisCameraCenterCastsShadowCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onAxisCameraCenterCastsShadowCheckBoxClicked(bool)));

    ui->wireframeEnabledCheckBox->setChecked(gen.getWireframeEnabled());
    connect(ui->wireframeEnabledCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onWireframeEnabledCheckBoxClicked(bool)));

    /* misc tab */

    connect(ui->saveCurrentFrameButton,
            SIGNAL(clicked()),
            this,
            SLOT(onSaveCurrentFrameButtonClicked()));

    /* render button area */

    connect(ui->automaticRecalculationCheckBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onAutomaticRecalculationCheckBoxClicked(bool)));

    connect(ui->renderButton,
            SIGNAL(clicked()),
            this,
            SLOT(onRenderButtonClicked()));

    /* renderarea */

    connect(renderArea,
            SIGNAL(onFrameFinished()),
            this,
            SLOT(onRenderAreaFrameFinished()));

    renderArea->getGenerator().setTreeObjectsReference(&treeObjects);

    bool loadScene = result == QMessageBox::Yes;

    if(loadLSystemFromFile(QString(LSYSTEMS_DIRECTORY_PATH) +
                               "/monopodial.lsy") && loadScene)
    {
        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setX(32);
        getCurrentTreeObject().setZ(34);
        getCurrentTreeObject().setIterations(10);

        regenerateTreeObject();

        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setX(43);
        getCurrentTreeObject().setZ(9);
        getCurrentTreeObject().setIterations(11);
        getCurrentTreeObject().setSeed(4);

        regenerateTreeObject();
    }

    if(loadLSystemFromFile(QString(LSYSTEMS_DIRECTORY_PATH) +
                              "/monopodial_v2.lsy") && loadScene)
    {
        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("monopodial_v2");
        getCurrentTreeObject().setX(-17);
        getCurrentTreeObject().setZ(-1);
        getCurrentTreeObject().setIterations(10);

        regenerateTreeObject();

        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("monopodial_v2");
        getCurrentTreeObject().setX(-7);
        getCurrentTreeObject().setZ(-7);
        getCurrentTreeObject().setAngleY(0.73);
        getCurrentTreeObject().setIterations(9);
        getCurrentTreeObject().setSeed(14);

        regenerateTreeObject();
    }

    loadLSystemFromFile(QString(LSYSTEMS_DIRECTORY_PATH) +
                            "/monopodial_symmetrical.lsy");
    if(loadLSystemFromFile(QString(LSYSTEMS_DIRECTORY_PATH) +
                              "/monopodial_leafless.lsy") && loadScene)
    {
        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("monopodial_leafless");
        getCurrentTreeObject().setX(28);
        getCurrentTreeObject().setZ(-40);
        getCurrentTreeObject().setAngleY(48);
        getCurrentTreeObject().setIterations(6);

        regenerateTreeObject();
    }

    if(loadLSystemFromFile(QString(LSYSTEMS_DIRECTORY_PATH) +
                              "/bush_centralized.lsy") && loadScene)
    {
        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("bush_centralized");
        getCurrentTreeObject().setX(44);
        getCurrentTreeObject().setZ(9.40);
        getCurrentTreeObject().setAngleY(0.73);
        getCurrentTreeObject().setIterations(3);
        getCurrentTreeObject().setSeed(14);

        regenerateTreeObject();

        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("bush_centralized");
        getCurrentTreeObject().setIterations(3);
        getCurrentTreeObject().setSeed(1);

        regenerateTreeObject();

        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("bush_centralized");
        getCurrentTreeObject().setX(-2);
        getCurrentTreeObject().setZ(-2);
        getCurrentTreeObject().setIterations(4);

        regenerateTreeObject();

        onAddTreeObjectButtonClicked();

        getCurrentTreeObject().setLSystem("bush_centralized");
        getCurrentTreeObject().setX(-1);
        getCurrentTreeObject().setZ(-2);
        getCurrentTreeObject().setIterations(3);
        getCurrentTreeObject().setSeed(3);

        regenerateTreeObject();
    }

    if(!lsystems.empty())
        switchToLSystem(0);

    if(!treeObjects.empty())
        switchToTreeObject(0);

    this->showMaximized();
    renderArea->update();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* utility */

/*!
 * Set up interface so that work with index-th tree object can be performed.
 * @param index Index of treeObject requested (index in treeObjects). If
 * set to -1 then it is assumed that there are no tree objects and
 * tree object interaction interface
 * needs to be disabled.
 */
void MainWindow::switchToTreeObject(int index)
{
    ui->treeObjectComboBox->setEnabled(index != -1);
    ui->deleteTreeObjectButton->setEnabled(index != -1);
    ui->treeObjectLSystemComboBox->setEnabled(index != -1);
    ui->iterationsCountSpinBox->setEnabled(index != -1);
    ui->seedSpinBox->setEnabled(index != -1);
    ui->treeObjectXSpinBox->setEnabled(index != -1);
    ui->treeObjectYSpinBox->setEnabled(index != -1);
    ui->treeObjectAngleSpinBox->setEnabled(index != -1);
    ui->showCommandsCheckBox->setEnabled(index != -1);
    ui->interpretButton->setEnabled(index != -1);

    if(index != -1)
    {
        ignoreEvents = true;

        ui->treeObjectComboBox->setCurrentIndex(index);

        ui->treeObjectLSystemComboBox->setCurrentIndex(
            ui->treeObjectLSystemComboBox->findText(
                QString::fromStdString(treeObjects[index].getLSystem())));

        ui->iterationsCountSpinBox->setMaximum(
                getLSystem(ui->treeObjectLSystemComboBox->currentText())
                        .getMaximumIterations());

        ui->iterationsCountSpinBox->setValue(
                treeObjects[index].getIterartions());

        ui->seedSpinBox->setValue(treeObjects[index].getSeed());
        ui->treeObjectXSpinBox->setValue(treeObjects[index].getX());
        ui->treeObjectYSpinBox->setValue(treeObjects[index].getZ());
        ui->treeObjectAngleSpinBox->setValue(treeObjects[index].getAngleY());

        ui->interpretationStatusLabel->setText(
            QString::fromStdString(treeObjects[index].getLog()));

        ignoreEvents = false;
    }
    else
    {
        ui->interpretationStatusLabel->setText("...");
    }

    updateInterpretationString();
    updateTreeObjectPolygonCountString();
}

/*!
 * Set up interface so that work with index-th l-system can be performed.
 * @param index Index of lsystem requested (index in lsystems). If
 * set to -1 then it is assumed that there are no lsystems and
 * lsystem interaction interface
 * needs to be disabled.
 */
void MainWindow::switchToLSystem(int index)
{
    ui->lSystemComboBox->setEnabled(index != -1);
    ui->deleteLSystemButton->setEnabled(index != -1);
    ui->sourceDataTextEdit->setEnabled(index != -1);
    ui->saveLSystemButton->setEnabled(index != -1);
    ui->buildLSystemButton->setEnabled(index != -1);
    ui->addTreeObjectButton->setEnabled(index != -1);

    if(oldIndex != -1)
    {
        lsystems[oldIndex]
            .setSource(ui->sourceDataTextEdit->toPlainText().toStdString());
    }

    oldIndex = index;

    if(index != -1)
    {
        ui->lSystemComboBox->setCurrentIndex(index);
        ui->sourceDataTextEdit->setText(
            QString::fromStdString(lsystems[index].getSource()));
        ui->compileStatusLabel->setText(
            QString::fromStdString(lsystems[index].getLog()));
    }
}

/*! Attempt to regenerate rendering data of currently selected tree object
 * and set up inteface elements in accordance with the results */
void MainWindow::regenerateTreeObject()
{
    getCurrentTreeObject().regenerate(
        getLSystem(getCurrentTreeObject().getLSystem()));
    ui->interpretationStatusLabel->setText(
        QString::fromStdString(getCurrentTreeObject().getLog()));

    updateInterpretationString();
    updateTreeObjectPolygonCountString();
}

/*! Set up interface elements so that current
 * sequence of tokens used for 3d model generation is represented, */
void MainWindow::updateInterpretationString()
{
    if(ui->treeObjectComboBox->currentIndex() == -1)
    {
        ui->chainLabel->setText("...");
        return;
    }

    std::vector<FG::InterpretationToken>& tokens =
        getCurrentTreeObject().getTokens();

    QString text = "...";
    if(ui->showCommandsCheckBox->isChecked() &&
       getCurrentTreeObject().isValid())
    {
        text = "";

        for(FG::InterpretationToken& token : tokens)
        {
            text += QString::fromStdString(token.name);

            if(token.parameters.size() != 0)
            {
                text += "(";

                for(size_t i = 0; i < token.parameters.size(); i++)
                {
                    text += QString::number(token.parameters[i]);

                    if(i != token.parameters.size() - 1)
                        text += ", ";
                }

                text += ")";
            }

            text += " ";
        }
    }

    ui->chainLabel->setText(text);
}

/*!
 * Set up interface elements so that information about
 * current tree object's polygon count is represented.
 */
void MainWindow::updateTreeObjectPolygonCountString()
{
    if(!treeObjects.empty() && getCurrentTreeObject().isValid())
    {
        ui->treeObjectPolygonCountLabel->setText(
                "Кол-во полигонов: " +
                QString::number(getCurrentTreeObject().getPolygonCount()));
    }
    else
        ui->treeObjectPolygonCountLabel->setText("Кол-во полигонов: ...");
}

/* utility */

/*! Return currently selected tree object */
TreeObject& MainWindow::getCurrentTreeObject()
{
    return treeObjects[ui->treeObjectComboBox->currentIndex()];
}

/*!
 * Get LSystem used by index-th treeObject.
 * @param index Index of treeObject in quesion.
 * @return LSystem used by index-th treeObject.
 */
FG::LSystem& MainWindow::getTreeObjectLSystem(int index)
{
    return lsystems[ui->lSystemComboBox->findText(
        QString::fromStdString(treeObjects[index].getLSystem()))];
}

/*!
 * Get LSystem with the given name.
 * If such LSystem could not be found, the behavior is undefined.
 * @param name Name of queried LSystem.
 * @return LSystem queried.
 */
FG::LSystem& MainWindow::getLSystem(const std::string& name)
{
    return getLSystem(QString::fromStdString(name));
}

/*!
 * Get LSystem with the given name.
 * If such LSystem could not be found, the behavior is undefined.
 * @param other Name of queried LSystem.
 * @return LSystem queried.
 */
FG::LSystem& MainWindow::getLSystem(const QString& name)
{
    return lsystems[ui->lSystemComboBox->findText(name)];
}

/* flora tab */

/*!
 * Add item of form "Prefix (Number)" to items stored in comboBox.
 * If there is no "Prefix" item, then an item "Prefix" is added.
 * Otherwise, a minimum number is chosen (starting with 2) so
 * that added item's name is unique.
 * @param baseName Prefix used to generate item.
 * @param comboBox ComboBox in question.
 */
static void addItemToComboBox(QString baseName, QComboBox*& comboBox)
{
    QString name = baseName;

    int number = 2;
    bool duplicate;

    do
    {
        duplicate = false;

        for(int i = 0; i < comboBox->count(); i++)
        {
            if(name == comboBox->itemText(i))
            {
                duplicate = true;

                name = baseName + " (" + QString::number(number++) + ")";

                break;
            }
        }
    } while(duplicate);

    comboBox->addItem(name);
}

void MainWindow::onTreeObjectComboBoxActivated(int value)
{
    switchToTreeObject(value);

    renderArea->update();
}

void MainWindow::onDeleteTreeObjectButtonClicked()
{
    int index = ui->treeObjectComboBox->currentIndex();

    ui->treeObjectComboBox->removeItem(index);
    treeObjects.erase(treeObjects.begin() + index,
                      treeObjects.begin() + index + 1);

    if(treeObjects.size() != 0)
        switchToTreeObject(std::max(index - 1, 0));
    else
        switchToTreeObject(-1);

    renderArea->update();
}

void MainWindow::onAddTreeObjectButtonClicked()
{
    ui->treeObjectComboBox->setEnabled(true);
    addItemToComboBox("Растительность ", ui->treeObjectComboBox);
    treeObjects.emplace_back(lsystems,
                             ui->treeObjectLSystemComboBox
                                 ->currentText().toStdString());

    TreeObject& object = treeObjects[ui->treeObjectComboBox->count() - 1];
    object.setIterations(
        static_cast<unsigned int>(
                getTreeObjectLSystem(ui->treeObjectComboBox->count() - 1)
                        .getPreferredIterations()));

    switchToTreeObject(ui->treeObjectComboBox->count() - 1);
    onInterpretButtonClicked();
    renderArea->update();
}

void MainWindow::onTreeObjectLSystemComboBoxActivated(int)
{
    if(ignoreEvents) return;

    getCurrentTreeObject()
        .setLSystem(ui->treeObjectLSystemComboBox->currentText().toStdString());

    ui->iterationsCountSpinBox->setMaximum(
        getLSystem(ui->treeObjectLSystemComboBox->currentText())
            .getMaximumIterations());

    regenerateTreeObject();
    renderArea->update();
}

void MainWindow::onIterationsCountSpinBoxValueChanged(int value)
{
    if(ignoreEvents) return;

    getCurrentTreeObject().setIterations(value);
    regenerateTreeObject();
    renderArea->update();
}

void MainWindow::onSeedSpinBoxValueChanged(int value)
{
    if(ignoreEvents) return;

    getCurrentTreeObject().setSeed(value);
    regenerateTreeObject();
    renderArea->update();
}

void MainWindow::onTreeObjectXSpinBoxValueChanged(double value)
{
    if(ignoreEvents) return;

    getCurrentTreeObject().setX(value);
    renderArea->update();
}

void MainWindow::onTreeObjectYSpinBoxValueChanged(double value)
{
    if(ignoreEvents) return;

    getCurrentTreeObject().setZ(value);
    renderArea->update();
}

void MainWindow::onTreeObjectAngleSpinBoxValueChanged(double value)
{
    if(ignoreEvents) return;

    getCurrentTreeObject().setAngleY(FR_TO_RADIANS(value));
    renderArea->update();
}

void MainWindow::onInterpretButtonClicked()
{
    regenerateTreeObject();
    renderArea->update();
}

void MainWindow::onShowCommandsCheckBoxClicked(bool)
{
    updateInterpretationString();
}

void MainWindow::onLSystemComboBoxActivated(int index)
{
    switchToLSystem(index);
}

void MainWindow::onDeleteLSystemButtonClicked()
{
    int index = ui->lSystemComboBox->currentIndex();

    std::string lSystemName = ui->lSystemComboBox->itemText(index)
                                                       .toStdString();

    for(TreeObject& object : treeObjects)
        if(object.getLSystem() == lSystemName)
        {
            QMessageBox::critical(this,
                                  "Ошибка",
                                  "Данная L-система всё ещё используется "
                                  "как минимум одним объектом. Удалите "
                                  "подобные объекты предварительно.");
            return;
        }

    ui->lSystemComboBox->removeItem(index);
    ui->treeObjectLSystemComboBox->removeItem(index);
    lsystems.erase(lsystems.begin() + index, lsystems.begin() + index + 1);

    oldIndex = -1;

    if(lsystems.size() != 0)
        switchToLSystem(std::max(index - 1, 0));
    else
        switchToLSystem(-1);
}

void MainWindow::onAddLSystemButtonClicked()
{
    ui->lSystemComboBox->setEnabled(true);
    addItemToComboBox("L-система ", ui->lSystemComboBox);
    ui->treeObjectLSystemComboBox->addItem(
        ui->lSystemComboBox->itemText(ui->lSystemComboBox->count() - 1));
    lsystems.push_back(FG::LSystem());

    switchToLSystem(ui->lSystemComboBox->count() - 1);
}

bool MainWindow::loadLSystemFromFile(const QString& fileName)
{
    if(fileName.isEmpty())
        return false;

    QFile file(fileName);

    if(file.open(QFile::ReadOnly))
    {
        QString data = QTextStream(&file).readAll();

        addItemToComboBox(QFileInfo(file).baseName(), ui->lSystemComboBox);
        ui->treeObjectLSystemComboBox->addItem(
            ui->lSystemComboBox->itemText(ui->lSystemComboBox->count() - 1));

        FG::LSystem lsystem;
        lsystem.setSource(data.toStdString());
        lsystems.push_back(std::move(lsystem));

        switchToLSystem(lsystems.size() - 1);

        onBuildLSystemButtonClicked();

        return true;
    }
    else
    {
        QMessageBox::critical(this,
                              "Ошибка открытия файла L-системы",
                              "Ошибка: не удалось открыть файл '" +
                              fileName +
                              "'.");

        return false;
    }
}

void MainWindow::onLoadLSystemButtonClicked()
{
    QString fileName =
        QFileDialog::getOpenFileName(this,
                                     "Выберите файл для загрузки L-системы...",
                                     QString(LSYSTEMS_DIRECTORY_PATH),
                                     "Файлы L-систем (*.lsy)");

    loadLSystemFromFile(fileName);
}

void MainWindow::onSaveLSystemButtonClicked()
{
    QString fileName =
        QFileDialog::getSaveFileName(this,
                                     "Выберите файл для сохранения L-системы...",
                                     QString(LSYSTEMS_DIRECTORY_PATH),
                                     "Файлы L-систем (*.lsy)");

    if(fileName.isEmpty())
        return;

    QFile file(fileName);

    if(file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream ostream(&file);

        ostream << ui->sourceDataTextEdit->toPlainText();;
    }
    else
    {
        QMessageBox::critical(this,
                              "Ошибка",
                              "Ошибка: не удалось открыть файл.");
    }
}

void MainWindow::onBuildLSystemButtonClicked()
{    
    FG::LSystem& lsystem = lsystems[ui->lSystemComboBox->currentIndex()];

    lsystem.setSource(ui->sourceDataTextEdit->toPlainText().toStdString());
    lsystem.verify();
    ui->compileStatusLabel->setText(QString::fromStdString(lsystem.getLog()));
}

/* rendering tab */

void MainWindow::onPerspectiveProjectionCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setProjectionMatrixType(value);
    renderArea->update();
}

void MainWindow::onFovSliderValueChanged(int value)
{
    ui->fovLabel->setText("Поле зрения (FOV, град.): " +
                          QString::number(value));

    renderArea->getGenerator().setFov(value);
    renderArea->update();
}

void MainWindow::onTexturingEnabledCheckBoxClicked(bool value)
{
    ui->bilinearFilteringEnabledCheckBox->setEnabled(value);

    renderArea->getGenerator().setTexturingEnabled(value);
    renderArea->update();
}

void MainWindow::onBilinearFilteringEnabledCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setBilinearFilteringEnabled(value);
    renderArea->update();
}

void MainWindow::onIlluminationCheckBoxClicked(bool value)
{
    bool slidersEnabled = value || ui->shadowsEnabledCheckBox->isChecked();

    ui->lightSourceYRotSlider->setEnabled(slidersEnabled);
    ui->lightSourceZRotSlider->setEnabled(slidersEnabled);

    renderArea->getGenerator().setIlluminationEnabled(value);
    renderArea->update();
}

void MainWindow::onShadowsEnabledCheckBoxClicked(bool value)
{
    bool slidersEnabled = value || ui->illuminationCheckBox->isChecked();

    ui->lightSourceYRotSlider->setEnabled(slidersEnabled);
    ui->lightSourceZRotSlider->setEnabled(slidersEnabled);

    ui->shadowsTextureSizeSpinBox->setEnabled(value);

    if(!value &&
       renderArea->getGenerator().getOutputFrameType()
           == OutputFrameType::ShadowMap)
    {
        renderArea->getGenerator().setOutputFrameType(OutputFrameType::Frame);
        ui->showShadowMapCheckBox->setChecked(false);
    }

    ui->showShadowMapCheckBox->setEnabled(value);

    renderArea->getGenerator().setShadowsEnabled(value);
    renderArea->update();
}

void MainWindow::onLightSourceYRotSliderValueChanged(int value)
{
    renderArea->getGenerator().setLightSourceYRot(value * FR_PI / 180);
    renderArea->update();
}

void MainWindow::onLightSourceZRotSliderValueChanged(int value)
{
    renderArea->getGenerator().setLightSourceZRot(value * FR_PI / 180);
    renderArea->update();
}

void MainWindow::onShadowTextureSizeSpinBoxValueChanged(int value)
{
    renderArea->getGenerator().setShadowsTextureSize(value);
    renderArea->update();
}

void MainWindow::onShowShadowMapCheckBoxClicked(bool value)
{
    renderArea->getGenerator()
        .setOutputFrameType(value ? OutputFrameType::ShadowMap :
                                    OutputFrameType::Frame);

    ui->showZBufferCheckBox->setChecked(false);

    renderArea->update();
}

void MainWindow::onCullingBackRadioButtonClicked()
{
    renderArea->getGenerator().setCullingMode(frCullingModeBack);
    renderArea->update();
}

void MainWindow::onCullingFrontRadioButtonClicked()
{
    renderArea->getGenerator().setCullingMode(frCullingModeFront);
    renderArea->update();
}

void MainWindow::onCullingOffRadioButtonClicked()
{
    renderArea->getGenerator().setCullingMode(frCullingModeNone);
    renderArea->update();
}

void MainWindow::onClippingNearRadioButtonClicked()
{
    renderArea->getGenerator().setClippingMode(frClippingModeNear);
    renderArea->update();
}

void MainWindow::onClippingNearFarRadioButtonClicked()
{
    renderArea->getGenerator().setClippingMode(frClippingModeNearFar);
    renderArea->update();
}

void MainWindow::onClippingAllRadioButtonClicked()
{
    renderArea->getGenerator().setClippingMode(frClippingModeAll);
    renderArea->update();
}

void MainWindow::onAutomaticallyCaclulateFarNearCheckBoxClicked(bool value)
{
    ui->nearPlaneSpinBox->setEnabled(!value);
    ui->farPlaneSpinBox->setEnabled(!value);

    if(!value)
    {
        renderArea->getGenerator()
            .setForcedNearPlane(ui->nearPlaneSpinBox->value());
        renderArea->getGenerator()
            .setForcedFarPlane(ui->farPlaneSpinBox->value());
    }

    renderArea->getGenerator().setAutomaticNearFarPlanes(value);
    renderArea->update();
}

void MainWindow::onNearPlaneSpinBoxValueChanged(double value)
{
    if(!ui->nearPlaneSpinBox->isEnabled() || changingNearFar)
        return;

    changingNearFar = true;

    double near = value;
    double far = ui->farPlaneSpinBox->value();

    near = clampValue(near,
                      RenderAreaFrameGenerator::MIN_NEAR,
                      far - RenderAreaFrameGenerator::MIN_NEAR_FAR_DISTANCE);
 
    ui->nearPlaneSpinBox->setValue(near);

    changingNearFar = false;

    renderArea->getGenerator().setForcedNearPlane(near);
    renderArea->getGenerator().setForcedFarPlane(far);

    renderArea->update();
}

void MainWindow::onFarPlaneSpinBoxValueChanged(double value)
{
    if(!ui->farPlaneSpinBox->isEnabled() || changingNearFar)
        return;

    changingNearFar = true;

    double near = ui->nearPlaneSpinBox->value();
    double far = value;

    far = clampValue(far,
                     near + RenderAreaFrameGenerator::MIN_NEAR_FAR_DISTANCE,
                     RenderAreaFrameGenerator::MAX_FAR);

    ui->farPlaneSpinBox->setValue(far);

    changingNearFar = false;

    renderArea->getGenerator().setForcedNearPlane(near);
    renderArea->getGenerator().setForcedFarPlane(far);

    renderArea->update();
}

void MainWindow::onZBufferEnabledCheckBoxClicked(bool value)
{
    ui->showZBufferCheckBox->setEnabled(value);

    renderArea->getGenerator().setZBufferEnabled(value);
    renderArea->update();
}

void MainWindow::onShowZBufferCheckBoxClicked(bool value)
{
    renderArea->getGenerator()
        .setOutputFrameType(value ? OutputFrameType::ZBuffer :
                                    OutputFrameType::Frame);

    ui->showShadowMapCheckBox->setChecked(false);

    renderArea->update();
}

void MainWindow::onDrawOriginCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setDrawOrigin(value);
    renderArea->update();
}

void MainWindow::onDrawAxisCameraOriginCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setDrawAxisCameraCenter(value);
    renderArea->update();
}

void MainWindow::onAxisCameraCenterCastsShadowCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setAxisCameraCenterCastsShadow(value);
    renderArea->update();
}

void MainWindow::onWireframeEnabledCheckBoxClicked(bool value)
{
    renderArea->getGenerator().setWireframeEnabled(value);
    renderArea->update();
}

/* misc tab */

void MainWindow::onSaveCurrentFrameButtonClicked()
{
    if(!renderArea->hasValidFrame())
    {
        QMessageBox::critical(this,
                              "Ошибка сохранения",
                              "Отсутствует корректно сгенерированный кадр.");
        return;
    }

    QImage currentFrame = renderArea->getCurrentFrame();

    QString fileName =
        QFileDialog::getSaveFileName(this,
                                     "Выберите файл для сохранения...",
                                     "",
                                     "Изображения PNG (*.png)");

    if(fileName == "")
        return;

    if(QFileInfo(fileName).completeSuffix() != "png")
        fileName += ".png";

    currentFrame.save(fileName, "png", /* quality */ 100);
}

/* render button area */

void MainWindow::onAutomaticRecalculationCheckBoxClicked(bool value)
{
    renderArea->setAutomaticRedraw(value);
}

void MainWindow::onRenderButtonClicked()
{
    renderArea->forceRegeneration();
    renderArea->update();
}

/* renderarea */

void MainWindow::onRenderAreaFrameFinished()
{
    ui->nearPlaneSpinBox->setValue(renderArea->getGenerator().getNearPlane());
    ui->farPlaneSpinBox->setValue(renderArea->getGenerator().getFarPlane());

    ui->scenePolygonCountLabel->setText(
        "Кол-во полигонов в сцене: " +
        QString::number(renderArea->getGenerator().getPolygonCount()));
}
